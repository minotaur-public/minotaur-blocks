

# Check if using VCS
if {[llength [namespace which get_app_var]]} {
    set is_vcs [string equal [get_app_var synopsys_program_name] "vcs"]
} else {
    set is_vcs 0
}

if {[llength [namespace which get_app_var]]} {
    set is_fm [string equal [get_app_var synopsys_program_name] "fm_shell"]
} else {
    set is_fm 0
}
upf_version 2.1

################################################################################
# Load UPF for blocks
################################################################################
if {$is_vcs} {
    # RRAM
    for {set i 0} {$i < 12} {incr i} {
        puts "Loading UPF for rram_bank_${i}"
        load_upf RRAMBankUserModeController.upf -scope system/tlrram/rram_bank_${i}
    }

    # SRAM
    for {set i 0} {$i < 8} {incr i} {
        puts "Loading UPF for sram_${i}"
        load_upf TLSRAM.upf -scope system/tlsramWrapper/sram_${i}
    }

    # Accelerator
    load_upf TLMatrixUnit.upf -scope system/accel/matrixUnit
    load_upf TLVectorUnit.upf -scope system/accel/vectorUnit

    # Tile
    load_upf RocketTile.upf -scope system/tile_prci_domain/tile_reset_domain/tile

    # Instruction RRAM
    load_upf RRAM8KUserModeController.upf -scope system/tlrram/instRRAM
}

if {$is_fm} {
    # RRAM
    for {set i 0} {$i < 12} {incr i} {
        puts "Loading UPF for rram_bank_${i}"
        load_upf inputs/blocks/RRAMBankUserModeController.upf.rtl -scope system/tlrram/rram_bank_${i}
    }

    # SRAM
    for {set i 0} {$i < 8} {incr i} {
        puts "Loading UPF for sram_${i}"
        load_upf inputs/blocks/TLSRAM.upf.rtl -scope system/tlsramWrapper/sram_${i}
    }

    # Accelerator
    load_upf inputs/blocks/TLMatrixUnit.upf.rtl -scope system/accel/matrixUnit
    load_upf inputs/blocks/TLVectorUnit.upf.rtl -scope system/accel/vectorUnit

    # Tile
    load_upf inputs/blocks/RocketTile.upf.rtl -scope system/tile_prci_domain/tile_reset_domain/tile

    # Instruction RRAM
    load_upf inputs/blocks/RRAM8KUserModeController.upf.rtl -scope system/tlrram/instRRAM
}
################################################################################
# Create and logically connect power ports and nets
################################################################################
create_supply_port VDD
create_supply_net VDD
connect_supply_net VDD -ports VDD

create_supply_port VDD_RRAM
create_supply_net VDD_RRAM
connect_supply_net VDD_RRAM -ports VDD_RRAM

create_supply_port VDD_RMACRO
create_supply_net VDD_RMACRO
connect_supply_net VDD_RMACRO -ports VDD_RMACRO

create_supply_port VDIO_RMACRO
create_supply_net VDIO_RMACRO
connect_supply_net VDIO_RMACRO -ports VDIO_RMACRO

create_supply_port VDUH_RMACRO
create_supply_net VDUH_RMACRO
connect_supply_net VDUH_RMACRO -ports VDUH_RMACRO

create_supply_port VDD_SMACRO
create_supply_net VDD_SMACRO
connect_supply_net VDD_SMACRO -ports VDD_SMACRO

create_supply_port VDD_IO
create_supply_net VDD_IO
connect_supply_net VDD_IO -ports VDD_IO

create_supply_port POC
create_supply_net POC
connect_supply_net POC -ports POC

create_supply_port VDIO_CORE
create_supply_net VDIO_CORE
connect_supply_net VDIO_CORE -ports VDIO_CORE

create_supply_port VSS
create_supply_net VSS
connect_supply_net VSS -ports VSS

create_supply_port VSS_IO
create_supply_net VSS_IO
connect_supply_net VSS_IO -ports VSS_IO

create_supply_net VDD_SW -resolve {parallel}

################################################################################
# Create Supply Set
################################################################################
create_supply_set VDD_IO_SS \
    -function {power VDD_IO} \
    -function {ground VSS_IO}

create_supply_set POC_SS \
    -function {power POC} \
    -function {ground VSS_IO}

create_supply_set VDIO_CORE_SS \
    -function {power VDIO_CORE} \
    -function {ground VSS}

create_supply_set VDD_SS \
    -function {power VDD} \
    -function {ground VSS}

create_supply_set VDD_SW_SS \
    -function {power VDD_SW} \
    -function {ground VSS}

create_supply_set VDD_RRAM_SS \
    -function {power VDD_RRAM} \
    -function {ground VSS}

create_supply_set VDD_RMACRO_SS \
    -function {power VDD_RMACRO} \
    -function {ground VSS}

create_supply_set VDIO_RMACRO_SS \
    -function {power VDIO_RMACRO} \
    -function {ground VSS}

create_supply_set VDD_SMACRO_SS       \
    -function {power VDD_SMACRO}    \
    -function {ground VSS}

set_port_attributes \
    -ports {jtag_TCK jtag_TMS jtag_TDI jtag_TDO c2c* bootControl_boot \
            rram_bist_enable rram_tck rram_tms rram_tdi rram_tdo \
            rram_jtag_sel rram_POC_H clock clockOut reset_wire_reset }\
    -driver_supply {VDD_IO_SS} \
    -receiver_supply {VDD_IO_SS}

################################################################################
# Create power domains
################################################################################

create_power_domain TOP_PD \
    -include_scope \
    -supply {primary VDD_SS} \
    -supply {extra_1 VDIO_CORE_SS}

create_power_domain CORE_PD \
    -elements { system/bootControl system/bootROMDomainWrapper system/clint \
    system/debug_1 system/domain system/dtm system/ibus \
    system/intsink* system/intsource* system/plicDomainWrapper \
    system/subsystem_cbus system/subsystem_membus system/subsystem_sbus \
    system/subsystem_pbus system/tileHartIdNexusNode \
    system/accel system/tlsramWrapper system/tile_prci_domain system/xbar* system/xbar* system/tlrram system/c2c_adapters*/dma } \
    -supply {primary VDD_SW_SS} \
    -supply {extra_0 VDD_SS} \
    -supply {extra_1 VDD_RRAM_SS} \
    -supply {extra_2 VDD_RMACRO_SS} \
    -supply {extra_3 VDIO_RMACRO_SS} \
    -supply {extra_4 VDD_SMACRO_SS}
    
create_power_domain IO_PD \
    -elements { io } \
    -supply {primary VDIO_CORE_SS} \
    -supply {extra_0 VDD_SS} \
    -supply {extra_1 VDD_IO_SS} 

create_power_domain PT_PD \
    -elements { system/pmu/passthroughPowerEnable_1p1v system/EmptyBlackBox } \
    -supply {primary VDD_SMACRO_SS} \
    -supply {extra_0 VDD_SS}

################################################################################
# Connect IPs to power domains
################################################################################
# RRAM
for {set i 0} {$i < 12} {incr i} {
    connect_supply_net VDD_RRAM -ports system/tlrram/rram_bank_${i}/VDD_RRAM
    connect_supply_net VDD_SW -ports system/tlrram/rram_bank_${i}/VDD_SW
    connect_supply_net VDD -ports system/tlrram/rram_bank_${i}/VDD
    connect_supply_net VDD_RMACRO -ports system/tlrram/rram_bank_${i}/VDD_RMACRO
    connect_supply_net VDIO_RMACRO -ports system/tlrram/rram_bank_${i}/VDIO_RMACRO
    connect_supply_net VDUH_RMACRO -ports system/tlrram/rram_bank_${i}/VDUH_RMACRO
    connect_supply_net VSS -ports system/tlrram/rram_bank_${i}/VSS
}

# SRAM
for {set i 0} {$i < 8} {incr i} {
    connect_supply_net VDD -ports system/tlsramWrapper/sram_${i}/VDD
    connect_supply_net VDD_SW -ports system/tlsramWrapper/sram_${i}/VDD_CORE_SW
    connect_supply_net VDD_SMACRO -ports system/tlsramWrapper/sram_${i}/VDD_SMACRO
    connect_supply_net VSS -ports system/tlsramWrapper/sram_${i}/VSS
}

# Accelerator
connect_supply_net VDD -ports system/accel/matrixUnit/VDD
connect_supply_net VDD_SW -ports system/accel/matrixUnit/VDD_CORE_SW
connect_supply_net VDD_SMACRO -ports system/accel/matrixUnit/VDD_SMACRO
connect_supply_net VSS -ports system/accel/matrixUnit/VSS

connect_supply_net VDD -ports system/accel/vectorUnit/VDD
connect_supply_net VDD_SW -ports system/accel/vectorUnit/VDD_CORE_SW
connect_supply_net VSS -ports system/accel/vectorUnit/VSS

# Tile
connect_supply_net VDD -ports system/tile_prci_domain/tile_reset_domain/tile/VDD
connect_supply_net VDD_SW -ports system/tile_prci_domain/tile_reset_domain/tile/VDD_CORE_SW
connect_supply_net VDD_SMACRO -ports system/tile_prci_domain/tile_reset_domain/tile/VDD_SMACRO
connect_supply_net VSS -ports system/tile_prci_domain/tile_reset_domain/tile/VSS

# IRRAM
connect_supply_net VDD_RRAM -ports system/tlrram/instRRAM/VDD_RRAM
connect_supply_net VDD_SW -ports system/tlrram/instRRAM/VDD_CORE_SW
connect_supply_net VDD -ports system/tlrram/instRRAM/VDD_CORE
connect_supply_net VDD_RMACRO -ports system/tlrram/instRRAM/VDD_RMACRO
connect_supply_net VDIO_RMACRO -ports system/tlrram/instRRAM/VDIO_RMACRO
connect_supply_net VDUH_RMACRO -ports system/tlrram/instRRAM/VDUH_RMACRO
connect_supply_net VSS -ports system/tlrram/instRRAM/VSS

set io_paths [find_objects . -pattern {io/*iocell*} -object_type inst]
foreach io $io_paths {
    connect_supply_net VDD_IO -ports ${io}/iocell/VDDPST
    connect_supply_net VSS_IO -ports ${io}/iocell/VSSPST
    connect_supply_net VDIO_CORE -ports ${io}/iocell/VDD
    connect_supply_net VSS -ports ${io}/iocell/VSS
    connect_supply_net POC -ports ${io}/iocell/POC
}

if {$is_vcs} {
    create_logic_net corePowerEnable
    connect_logic_net corePowerEnable -ports {system/pmu/auto_io_out_globalPowerEnable system/tile_prci_domain/tile_reset_domain/tile/powerEnable}
    
    create_logic_net corePowerEnable_1p1v
    connect_logic_net corePowerEnable_1p1v -ports {system/pmu/passthroughPowerEnable_1p1v/out system/tile_prci_domain/tile_reset_domain/tile/powerEnable1p1}
} elseif {$is_fm} {
    # do nothing, it's handled by the formality script
} else {
    connect_logic_net system/pmu/io_pmu_globalPowerEnable_1p1v -ports {system/tile_prci_domain/tile_reset_domain/tile/powerEnable1p1}
    connect_logic_net system/pmu/auto_io_out_globalPowerEnable -ports {system/tile_prci_domain/tile_reset_domain/tile/powerEnable}
}
################################################################################
# Create Power Switches
################################################################################
create_power_switch VDD_PS \
    -input_supply_port	{TVDD CORE_PD.extra_0.power} \
    -output_supply_port	{VDD VDD_SW} \
    -control_port		{NSLEEPIN system/pmu/auto_io_out_globalPowerEnable} \
    -on_state		{vdd_on TVDD NSLEEPIN} \
    -off_state		{vdd_off !NSLEEPIN} \
    -domain			CORE_PD

map_power_switch VDD_PS -domain CORE_PD -lib_cells {HDRSID0BWP40 HDRSID1BWP40 HDRSID2BWP40 }

################################################################################
# Set Isolation
################################################################################
set_isolation chiptop_isolation \
    -domain CORE_PD \
    -clamp_value 0 \
    -isolation_supply_set TOP_PD.primary \
    -isolation_signal system/pmu/auto_io_out_globalIsolationEnable \
    -isolation_sense high \
    -location parent \
    -applies_to outputs \
    -source VDD_SW_SS \
    -sink VDD_SS

set_isolation chiptop_isolation_io \
    -domain CORE_PD \
    -clamp_value 0 \
    -isolation_supply_set TOP_PD.primary \
    -isolation_signal system/pmu/auto_io_out_globalIsolationEnable \
    -isolation_sense high \
    -location parent \
    -applies_to outputs \
    -source VDD_SW_SS \
    -sink VDIO_CORE_SS

################################################################################
# Set Retention
################################################################################

# None in this design

################################################################################
# Set Level Shifters
################################################################################

set_level_shifter lvl_CORE2IO \
    -domain IO_PD \
    -exclude_elements {io/*/iocell/PAD}\
    -applies_to inputs \
    -location self \
    -sink VDIO_CORE_SS \
    -input_supply VDD_SS \
    -rule low_to_high \
        -force_shift \
    -name_prefix LVL_CORE2IO

set_level_shifter lvl_IO2CORE \
    -domain IO_PD \
    -applies_to outputs \
    -exclude_elements {io/*/iocell/PAD}\
    -location parent \
    -source VDIO_CORE_SS \
    -output_supply VDD_SS \
    -rule high_to_low \
        -force_shift \
    -name_prefix LVL_IO2CORE

set_level_shifter lvl_SRAM \
    -domain PT_PD \
    -elements { system/pmu/passthroughPowerEnable_1p1v } \
    -applies_to inputs \
    -location self \
    -force_shift \
    -output_supply VDD_SMACRO_SS\
    -rule low_to_high \
    -name_prefix LVL_SRAM

################################################################################
# Define port states
################################################################################

add_port_state VDIO_CORE \
    -state {VDIO_CORE_ON 1.1}

add_port_state VDD_IO \
    -state {VDD_IO_ON 2.5}

add_port_state POC \
    -state {POC_ON 2.5} \
    -state {POC_OFF off}

add_port_state VDD_RRAM \
    -state {VDD_RRAM_ON 0.9}

add_port_state VDD_RMACRO \
    -state {VDD_RMACRO_ON 1.1}

add_port_state VDIO_RMACRO \
    -state {VDIO_RMACRO_ON 2.5}

add_port_state VDUH_RMACRO \
    -state {VDUH_RMACRO_ON 2.5}

add_port_state VDD_SMACRO \
    -state {VDD_SMACRO_ON 1.1}

add_port_state VDD \
    -state {VDD_ON 0.9}

add_port_state VDD_PS/VDD \
    -state {VDD_SW_ON 0.9} \
    -state {VDD_SW_OFF off}

add_port_state VSS -state {VSS 0}
add_port_state VSS_IO -state {VSS_IO_ON 0}

################################################################################
# Define power state table
################################################################################
create_pst TOP_PST -supplies                {VDD    VDD_IO      POC     VDIO_CORE    VDD_SW     VDD_RRAM    VDD_RMACRO      VDIO_RMACRO     VDUH_RMACRO     VDD_SMACRO      VSS     VSS_IO}
add_pst_state CHIP_ON -pst TOP_PST -state   {VDD_ON VDD_IO_ON   POC_OFF  VDIO_CORE_ON VDD_SW_ON  VDD_RRAM_ON VDD_RMACRO_ON   VDIO_RMACRO_ON  VDUH_RMACRO_ON  VDD_SMACRO_ON   VSS    VSS_IO_ON}
add_pst_state CHIP_OFF -pst TOP_PST -state  {VDD_ON VDD_IO_ON   POC_OFF  VDIO_CORE_ON VDD_SW_OFF VDD_RRAM_ON VDD_RMACRO_ON   VDIO_RMACRO_ON  VDUH_RMACRO_ON  VDD_SMACRO_ON   VSS    VSS_IO_ON}
add_pst_state EXTERNAL_SHUTDOWN -pst TOP_PST -state  {VDD_ON VDD_IO_ON   POC_ON  VDIO_CORE_ON VDD_SW_OFF VDD_RRAM_ON VDD_RMACRO_ON   VDIO_RMACRO_ON  VDUH_RMACRO_ON  VDD_SMACRO_ON   VSS   VSS_IO_ON}

################################################################################
# Apply specific options
################################################################################
if {$is_vcs} {
    set_design_attributes -elements CORE_PD -attribute SNPS_reinit true
    set_design_attributes -elements {POC} -attribute SNPS_default_supply_net_state POC_OFF
}
