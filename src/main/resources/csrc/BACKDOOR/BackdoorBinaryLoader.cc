#include <fstream>
#include <vector>

#include "ElfLoader.h"
#include "SoCMemoryLoader.h"

extern "C" void load_binary(char *binary, int numChips) {
  std::cout << "numChips: " << numChips << std::endl;
  std::string file(binary);
  std::vector<std::string> binaryFiles;

  // if ELF file is provided, then use that same ELF for every chip
  ElfLoader elfLoader(file);
  if (elfLoader.isElf()) {
    for (int i = 0; i < numChips; i++) {
      binaryFiles.push_back(file);
    }
  } else {
    // if not ELF file, then it must be a file with a path to a binary on every
    // line
    std::string line;
    std::ifstream fileIn(file);
    while (std::getline(fileIn, line)) {
      binaryFiles.push_back(line);
    }
  }

  // load binary into each chip
  for (int i = 0; i < numChips; i++) {
    std::string path;
    if (i == 0) {
      path = "TestDriver.testHarness.testHarness.chiptop";
    } else {
      path = "TestDriver.testHarness.testHarness_" + std::to_string(i) +
             ".chiptop";
    }

    SocMemoryLoader memLoader(path);
    ElfLoader elfLoader(binaryFiles.at(i), &memLoader);
    elfLoader.load();
  }
}
