#pragma once

#include <string>

#include "MemoryLoader.h"

class BackdoorMemoryLoader {
 public:
  BackdoorMemoryLoader(char *binary, int numChips);

  void run();

 private:
  std::string binary_file;
  int numChips;
};