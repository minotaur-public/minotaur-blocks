#pragma once
#include <string>

#include "MemoryLoader.h"
#include "RRAMMemoryModel.h"
#include "SRAMMemoryModel.h"

class SocMemoryLoader : public MemoryLoader {
 public:
  SocMemoryLoader(const std::string& path);

  void writeMemory(uint64_t dst_addr, size_t len, const void* src);

 private:
  SRAMMemoryModel sram;
  RRAMMemoryModel rram;
};