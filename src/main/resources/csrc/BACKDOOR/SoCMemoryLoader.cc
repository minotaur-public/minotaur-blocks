#include "SoCMemoryLoader.h"

SocMemoryLoader::SocMemoryLoader(const std::string &path) : sram(path) {}

void SocMemoryLoader::writeMemory(uint64_t dst_addr, size_t len,
                                  const void *src) {
  std::cout << "Backdoor: Writing " << len << " bytes to 0x" << std::hex
            << dst_addr << std::dec << std::endl;

  // TODO: implement RRAM loading
  if ((dst_addr & 0xF000000) == 0x3000000) {
    for (size_t byte = 0; byte < len; byte++) {
      uint64_t address = dst_addr - 0x3000000 + byte;
      uint8_t *data = reinterpret_cast<uint8_t *>((char *)src + byte);
      sram.writeToMemory(address, *data);
    }
  }
}
