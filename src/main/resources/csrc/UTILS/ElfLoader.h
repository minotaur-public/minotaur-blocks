#pragma once

#include <string>

#include "MemoryLoader.h"

class ElfLoader {
 public:
  ElfLoader(const std::string &path);
  ElfLoader(const std::string &path, MemoryLoader *memLoader);

  void setMemLoader(MemoryLoader *memLoader);
  bool isElf();
  void load();

 private:
  std::string binary_file;
  MemoryLoader *memLoader;

  int fd;
  char *buf;
  size_t elf_size;

  void openFile();
  void closeFile();

  template <typename ehdr_t, typename phdr_t, typename shdr_t, typename sym_t>
  void decode();
};