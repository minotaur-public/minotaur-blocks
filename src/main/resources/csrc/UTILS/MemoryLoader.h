#pragma once

#include <stdint.h>

#include <cstdlib>

class MemoryLoader {
 public:
  MemoryLoader() {}

  virtual void writeMemory(uint64_t dst_addr, size_t len, const void *src) = 0;
};