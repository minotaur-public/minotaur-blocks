#include "ElfLoader.h"

#include <assert.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include <chrono>
#include <cstring>
#include <fstream>
#include <iostream>
#include <map>
#include <mutex>
#include <stdexcept>
#include <string>
#include <thread>
#include <vector>

#include "fesvr/elf.h"

ElfLoader::ElfLoader(const std::string &path) : binary_file(path) {}

ElfLoader::ElfLoader(const std::string &path, MemoryLoader *memLoader)
    : binary_file(path), memLoader(memLoader) {}

void ElfLoader::setMemLoader(MemoryLoader *memLoader) {
  this->memLoader = memLoader;
}

void ElfLoader::openFile() {
  // Open File
  if (access(binary_file.c_str(), R_OK) != 0)
    throw std::runtime_error(
        std::string("Error: Could not find the ELF file: ") + binary_file +
        ".\n");

  fd = open(binary_file.c_str(), O_RDONLY);
  struct stat s;

  if (fd < 0 || fstat(fd, &s) < 0)
    throw std::runtime_error(
        std::string("Error: Could not open the ELF file: ") + binary_file +
        ".\n");

  // Read file
  elf_size = s.st_size;
  buf =
      static_cast<char *>(mmap(NULL, elf_size, PROT_READ, MAP_PRIVATE, fd, 0));

  if (buf == MAP_FAILED)
    throw std::runtime_error("Error: Failed to mmap ELF file.");

  if (elf_size < sizeof(Elf64_Ehdr))
    throw std::runtime_error("Error: Bad ELF file size.");
}

void ElfLoader::closeFile() {
  // Clean up
  if (munmap(buf, elf_size) != 0 || close(fd) != 0)
    throw std::runtime_error("Error: Failed to free ELF file.");
}

bool ElfLoader::isElf() {
  openFile();

  // Decode ELF file
  const Elf64_Ehdr *eh64 = reinterpret_cast<const Elf64_Ehdr *>(buf);

  bool isElfFile = IS_ELF32(*eh64) || IS_ELF64(*eh64);

  closeFile();

  return isElfFile;
}

void ElfLoader::load() {
  openFile();

  // Decode ELF file
  const Elf64_Ehdr *eh64 = reinterpret_cast<const Elf64_Ehdr *>(buf);

  if (IS_ELF32(*eh64)) {
    decode<Elf32_Ehdr, Elf32_Phdr, Elf32_Shdr, Elf32_Sym>();
  } else if (IS_ELF64(*eh64)) {
    decode<Elf64_Ehdr, Elf64_Phdr, Elf64_Shdr, Elf64_Sym>();
  } else {
    throw std::runtime_error("Error: Bad ELF file.");
  }

  closeFile();
}

template <typename ehdr_t, typename phdr_t, typename shdr_t, typename sym_t>
void ElfLoader::decode() {
  std::vector<uint8_t> zeros;

  ehdr_t *eh = (ehdr_t *)buf;
  phdr_t *ph = (phdr_t *)(buf + eh->e_phoff);

  assert(elf_size >= eh->e_phoff + eh->e_phnum * sizeof(*ph));

  for (unsigned int i = 0; i < eh->e_phnum; i++) {
    if (ph[i].p_type == PT_LOAD && ph[i].p_memsz) {
      if (ph[i].p_filesz) {
        assert(elf_size >= ph[i].p_offset + ph[i].p_filesz);
        memLoader->writeMemory(ph[i].p_paddr, ph[i].p_filesz,
                               (uint8_t *)buf + ph[i].p_offset);
      }

      zeros.resize(ph[i].p_memsz - ph[i].p_filesz);
      memLoader->writeMemory(ph[i].p_paddr + ph[i].p_filesz,
                             ph[i].p_memsz - ph[i].p_filesz, &zeros[0]);
    }
  }

  shdr_t *sh = (shdr_t *)(buf + eh->e_shoff);
  assert(elf_size >= eh->e_shoff + eh->e_shnum * sizeof(*sh));
  assert(eh->e_shstrndx < eh->e_shnum);
  assert(elf_size >= sh[eh->e_shstrndx].sh_offset + sh[eh->e_shstrndx].sh_size);

  char *shstrtab = buf + sh[eh->e_shstrndx].sh_offset;
  unsigned int strtabidx = 0, symtabidx = 0;

  for (unsigned int i = 0; i < eh->e_shnum; i++) {
    unsigned int max_len = sh[eh->e_shstrndx].sh_size - sh[i].sh_name;

    assert(sh[i].sh_name < sh[eh->e_shstrndx].sh_size);
    assert(strnlen(shstrtab + sh[i].sh_name, max_len) < max_len);

    if (sh[i].sh_type & SHT_NOBITS) continue;
    assert(elf_size >= sh[i].sh_offset + sh[i].sh_size);

    if (strcmp(shstrtab + sh[i].sh_name, ".strtab") == 0) strtabidx = i;

    if (strcmp(shstrtab + sh[i].sh_name, ".symtab") == 0) symtabidx = i;
  }

  if (strtabidx && symtabidx) {
    char *strtab = buf + sh[strtabidx].sh_offset;
    sym_t *sym = (sym_t *)(buf + sh[symtabidx].sh_offset);

    for (unsigned i = 0; i < sh[symtabidx].sh_size / sizeof(sym_t); i++) {
      unsigned max_len = sh[strtabidx].sh_size - sym[i].st_name;
      assert(sym[i].st_name < sh[strtabidx].sh_size);
      assert(strnlen(strtab + sym[i].st_name, max_len) < max_len);
    }
  }
}