#include "RRAMMemoryModel.h"

#include <bitset>
#include <cstring>
#include <fstream>
#include <iostream>
#include <random>
#include <sstream>
#include <string>

#include "spdlog/spdlog.h"

RRAMMemoryModel::RRAMMemoryModel() : bwMode(QUAD) {
#ifdef EMU
  for (int i = 0; i < 12; i++) {
    for (int j = 0; j < 4; j++) {
      marg_handles[i][j] = marg_new(45, 64 * 1024);
    }
  }
#endif
}

std::bitset<12> RRAMMemoryModel::calculate_parity(std::bitset<32> data) {
  std::bitset<12> parity;
  parity[0] = data[0] ^ data[3] ^ data[4] ^ data[5] ^ data[6] ^ data[7] ^
              data[8] ^ data[10] ^ data[11] ^ data[15] ^ data[16] ^ data[20] ^
              data[22] ^ data[23] ^ data[26] ^ data[27] ^ data[28] ^ data[29];
  parity[1] = data[1] ^ data[4] ^ data[5] ^ data[6] ^ data[7] ^ data[8] ^
              data[9] ^ data[11] ^ data[12] ^ data[16] ^ data[17] ^ data[21] ^
              data[23] ^ data[24] ^ data[27] ^ data[28] ^ data[29] ^ data[30];
  parity[2] = data[2] ^ data[5] ^ data[6] ^ data[7] ^ data[8] ^ data[9] ^
              data[10] ^ data[12] ^ data[13] ^ data[17] ^ data[18] ^ data[22] ^
              data[24] ^ data[25] ^ data[28] ^ data[29] ^ data[30] ^ data[31];
  parity[3] = data[4] ^ data[5] ^ data[9] ^ data[13] ^ data[14] ^ data[15] ^
              data[16] ^ data[18] ^ data[19] ^ data[20] ^ data[22] ^ data[25] ^
              data[27] ^ data[28] ^ data[30] ^ data[31];
  parity[4] = data[0] ^ data[3] ^ data[4] ^ data[7] ^ data[8] ^ data[11] ^
              data[14] ^ data[17] ^ data[19] ^ data[21] ^ data[22] ^ data[27] ^
              data[31];
  parity[5] = data[0] ^ data[1] ^ data[3] ^ data[6] ^ data[7] ^ data[9] ^
              data[10] ^ data[11] ^ data[12] ^ data[16] ^ data[18] ^ data[26] ^
              data[27] ^ data[29];
  parity[6] = data[1] ^ data[2] ^ data[4] ^ data[7] ^ data[8] ^ data[10] ^
              data[11] ^ data[12] ^ data[13] ^ data[17] ^ data[19] ^ data[27] ^
              data[28] ^ data[30];
  parity[7] = data[2] ^ data[3] ^ data[5] ^ data[8] ^ data[9] ^ data[11] ^
              data[12] ^ data[13] ^ data[14] ^ data[18] ^ data[20] ^ data[28] ^
              data[29] ^ data[31];
  parity[8] = data[0] ^ data[5] ^ data[7] ^ data[8] ^ data[9] ^ data[11] ^
              data[12] ^ data[13] ^ data[14] ^ data[16] ^ data[19] ^ data[20] ^
              data[21] ^ data[22] ^ data[23] ^ data[26] ^ data[27] ^ data[28] ^
              data[30];
  parity[9] = data[0] ^ data[1] ^ data[6] ^ data[8] ^ data[9] ^ data[10] ^
              data[12] ^ data[13] ^ data[14] ^ data[15] ^ data[17] ^ data[20] ^
              data[21] ^ data[22] ^ data[23] ^ data[24] ^ data[27] ^ data[28] ^
              data[29] ^ data[31];
  parity[10] = data[1] ^ data[2] ^ data[3] ^ data[4] ^ data[5] ^ data[6] ^
               data[8] ^ data[9] ^ data[13] ^ data[14] ^ data[18] ^ data[20] ^
               data[21] ^ data[24] ^ data[25] ^ data[26] ^ data[27] ^ data[30];
  parity[11] = data[2] ^ data[3] ^ data[4] ^ data[5] ^ data[6] ^ data[7] ^
               data[9] ^ data[10] ^ data[14] ^ data[15] ^ data[19] ^ data[21] ^
               data[22] ^ data[25] ^ data[26] ^ data[27] ^ data[28] ^ data[31];

  return parity;
}

handle RRAMMemoryModel::get_rram_handle(int bank, int macro) {
  std::stringstream fmt;

  fmt << "TestDriver.testHarness.testHarness.chiptop.system.tlrram.rram_bank_"
      << bank << ".rram_bank_controller.rram_block_" << macro
      << ".rram_ip.u_rram.main_mem";

  return acc_handle_object(strdup(fmt.str().c_str()), NULL);
}

std::string RRAMMemoryModel::toBinaryString(uint8_t data) {
  std::stringstream ss;
  ss << std::bitset<8>(data);
  return ss.str();
}

// Note: somehow vpi doesn't work, but acc works?? Opposite of SRAM
void RRAMMemoryModel::writeToMemory(int address, int data) {
  spdlog::debug("[RRAM] Writing {} to {}", data, address);
  int byte_select, macro_select, word_address, bank_select;
  decodeAddress(address, byte_select, macro_select, word_address, bank_select);

  std::string bitstring = toBinaryString(data);

  spdlog::debug("Writing to bank {} macro {} address {} byte {} data {} ",
                bank_select, macro_select, word_address, byte_select,
                bitstring);

#ifdef EMU
  unsigned buf[2];
  marg_read(marg_handles[bank_select][macro_select], word_address, buf);
  uint64_t existingValue = (static_cast<uint64_t>(buf[1]) << 32) | buf[0];
  std::string strVal =
      std::bitset<45>(existingValue).to_string();  // string conversion
#else
  char buf[64];
  handle rram_handle = get_rram_handle(bank_select, macro_select);
  acc_getmem_bitstr(rram_handle, buf, word_address, 0, 45);
  std::string strVal(buf);
#endif
  // bytes go from right to left, while string goes from left to right
  // therefore, we need to do insert at 45 - (byte_select + 1)*8
  strVal.replace(45 - (byte_select + 1) * 8, 8, bitstring);

  // update parity bits (data starts at pos 12+1)
  std::bitset<12> parity =
      calculate_parity(std::bitset<32>(strVal.substr(12 + 1)));

  std::stringstream ss;
  ss << parity;
  // parity starts at pos 1 (ignore upper io bit)
  strVal.replace(1, 12, ss.str());
#ifdef EMU
  // convert to bitset
  uint64_t newValue = std::bitset<45>(strVal).to_ullong();
  buf[0] = newValue;
  buf[1] = newValue >> 32;
  marg_write(marg_handles[bank_select][macro_select], word_address, buf);
#else
  acc_setmem_bitstr(rram_handle, const_cast<char *>(strVal.c_str()),
                    word_address, 0);
#endif
}

void RRAMMemoryModel::setBandwidthMode(BandwidthMode mode) { bwMode = mode; }

void RRAMMemoryModel::decodeAddress(int address, int &byte_select,
                                    int &macro_select, int &word_address,
                                    int &bank_select) {
  // [45-1:0] main_mem[64k-1:0]
  // {io,ecc,data}
  // {1, 12, 32}
  // ignore the upper io bit

  // byte_select and macro_select is common to all BW modes
  // byte_select = address[1:0]
  byte_select = address & ((1 << 2) - 1);
  // macro_select = address[3:2]
  macro_select = (address >> 2) & ((1 << 2) - 1);

  address = address >> 4;
  if (bwMode == QUAD) {
    // word_address = address[17:2]
    word_address = (address >> 2) & ((1 << 16) - 1);
    // bank_select = {address[19:18],address[1:0]}
    bank_select =
        (((address >> 18) & ((1 << 2) - 1)) << 2) | (address & ((1 << 2) - 1));
  } else if (bwMode == DOUBLE) {
    // word_address = address[16:1]
    word_address = (address >> 1) & ((1 << 16) - 1);
    // bank_select = {address[19:17],address[0]}
    bank_select = (((address >> 17) & ((1 << 3) - 1)) << 1) | (address & 1);
  } else if (bwMode == SINGLE) {
    // word_address = address[15:0]
    word_address = (address) & ((1 << 16) - 1);
    // bank_select = {address[19:16]}
    bank_select = (((address >> 16) & ((1 << 4) - 1)));
  } else {
    std::cerr << "Invalid bandwidth mode: " << bwMode << std::endl;
    exit(1);
  }
}

void RRAMMemoryModel::flushWrites() {
#ifdef EMU
  for (int bank = 0; bank < 12; bank++) {
    for (int macro = 0; macro < 4; macro++) {
      std::stringstream fmt;

      fmt << "TestDriver.testHarness.testHarness.chiptop.system.tlrram.rram_"
             "bank_"
          << bank << ".rram_bank_controller.rram_block_" << macro
          << ".rram_ip.u_rram.main_mem";

      int vm_handle = marg_vmem_handle(strdup(fmt.str().c_str()));
      marg_put(marg_handles[bank][macro], 0, 64 * 1024, vm_handle, 0);
    }
  }
#endif
}
