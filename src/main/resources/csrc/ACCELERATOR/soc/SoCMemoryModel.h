#pragma once

// clang-format off
#include "src/DataTypes.h"
// clang-format on
#include "MemoryModel.h"
#include "RRAMMemoryModel.h"
#include "SRAMMemoryModel.h"
#include "src/ArchitectureParams.h"

class SoCMemoryModel : public MemoryModel {
 public:
  SoCMemoryModel() : MemoryModel(true) {
    rram = new RRAMMemoryModel();
    sram = new SRAMMemoryModel("TestDriver.testHarness.chiptop0.system.sbus");
  }

  // uint8_t readFromSRAM(int address) { return sram->readFromMemory(address); }
  void setBandwidthMode(BandwidthMode mode) { rram->setBandwidthMode(mode); }

  void flushWrites() {
    rram->flushWrites();
    sram->flushWrites();
  }

  void prepareReads() { sram->prepareReads(); }

  RRAMMemoryModel *rram;
  SRAMMemoryModel *sram;

 private:
  void writeToReference(int address, double val, bool doublePrecision) {};

  void writeToMemory(int address, double val, const MemorySource &mem,
                     bool doublePrecision) {
    // birch only has SRAM
    RTLMemoryModel *memory = sram;

    if (!doublePrecision) {
      INPUT_DATATYPE p8 = val;
      memory->writeToMemory(address, p8.bits_rep());
    } else {
      ACCUM_DATATYPE p16 = val;
      int bits = p16.bits_rep();
      memory->writeToMemory(address, bits & 0xFF);
      memory->writeToMemory(address + 1, (bits >> 8) & 0xFF);
    }
  }
};
