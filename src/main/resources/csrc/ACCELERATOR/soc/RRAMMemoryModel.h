#pragma once

#include <acc_user.h>
#include <svdpi.h>
#ifdef EMU
#include <marg_user.h>
#else
#include <vcs_acc_user.h>
#include <vcsuser.h>
#endif
#include <vpi_user.h>

#include <bitset>

#include "RTLMemoryModel.h"
#include "test/common/VerificationTypes.h"

class RRAMMemoryModel : public RTLMemoryModel {
 public:
  RRAMMemoryModel();
  void setBandwidthMode(BandwidthMode mode);
  void flushWrites();

 private:
  BandwidthMode bwMode;
  // used for emulation
  int marg_handles[12][4];

  void decodeAddress(int address, int &byte_select, int &macro_select,
                     int &word_address, int &bank_select);

  void writeToMemory(int address, int data) override;

  std::bitset<12> calculate_parity(std::bitset<32> data);
  handle get_rram_handle(int bank, int macro);

  std::string toBinaryString(uint8_t data);
};