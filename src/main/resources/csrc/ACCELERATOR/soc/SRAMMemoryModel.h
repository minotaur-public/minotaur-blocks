#pragma once

#include <acc_user.h>
#include <svdpi.h>
#ifndef EMU
#include <vcs_acc_user.h>
#include <vcsuser.h>
#endif
#include <vpi_user.h>

#include <bitset>

#include "RTLMemoryModel.h"

class SRAMMemoryModel : public RTLMemoryModel {
 public:
  SRAMMemoryModel(const std::string &chiptopPath);

  void writeToMemory(int address, int data) override;
  uint8_t readFromMemory(int address);
  void flushWrites();
  void prepareReads();

 private:
  std::string chiptopPath;

  // used for emulation
  int marg_handles[12][4];

  vpiHandle get_sram_handle(int block, int bank, int macro);
  std::string toBinaryString(uint8_t data);
  uint8_t fromBinaryString(const std::string &data);
};
