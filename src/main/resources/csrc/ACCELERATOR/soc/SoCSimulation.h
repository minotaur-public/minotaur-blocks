#pragma once

#include <svdpi.h>

#include "SoCMemoryModel.h"
#include "test/common/Simulation.h"

class SoCSimulation : public Simulation {
 public:
  SoCSimulation();

  void loadMemory();
  int checkOutput();
  void checkSingleOutput(svLogicVecVal *, svLogicVecVal *);

 private:
  int workloadCount;
  SoCMemoryModel *socMemory;
};