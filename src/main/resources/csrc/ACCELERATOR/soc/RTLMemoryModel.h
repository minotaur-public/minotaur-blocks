#pragma once

class RTLMemoryModel {
 public:
  RTLMemoryModel() {}

  virtual void writeToMemory(int address, int data) = 0;
};