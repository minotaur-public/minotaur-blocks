#include "SRAMMemoryModel.h"

#ifdef EMU
#include <marg_user.h>
#endif

#include <bitset>
#include <cstring>
#include <fstream>
#include <iostream>
#include <random>
#include <sstream>
#include <string>

#include "spdlog/spdlog.h"

SRAMMemoryModel::SRAMMemoryModel(const std::string& chiptopPath)
    : chiptopPath(chiptopPath) {
#ifdef EMU
  for (int bank = 0; bank < 8; bank++) {
    for (int macro = 0; macro < 2; macro++) {
      marg_handles[bank][macro] = marg_new(64, 16384);
    }
  }
  prepareReads();
#endif
}

vpiHandle SRAMMemoryModel::get_sram_handle(int block, int bank, int macro) {
  std::stringstream fmt;
  std::stringstream block_str;
  if (block == 0) block_str << "";
  else block_str << "_" << block;

#ifdef GENERIC_SOC  
  fmt << chiptopPath << ".sram" << block_str.str()
      << ".sram.mem_" << bank << "_" << macro
      << ".mem";
#else
  fmt << chiptopPath << ".sram" << block_str.str()
      << ".sram.mem_" << bank << "_" << macro
      << ".ip224uhdlp1p11rf_4096x64m4b2c1s1_t0r0p0d0a1m1h_bmod"
      << ".ip224uhdlp1p11rf_4096x64m4b2c1s1_t0r0p0d0a1m1h_array.DATA_ARRAY";
#endif

  spdlog::debug(fmt.str().c_str());

  return vpi_handle_by_name(strdup(fmt.str().c_str()), NULL);
}

std::string SRAMMemoryModel::toBinaryString(uint8_t data) {
  std::stringstream ss;
  ss << std::bitset<8>(data);
  return ss.str();
}

// Note: somehow acc doesn't work, but vpi works?? Opposite of RRAM
void SRAMMemoryModel::writeToMemory(int address, int data) {
  spdlog::debug("[SRAM] Writing {} to {}", data, address);
//  // SRAM is composed of banks of 16384 words of 16 bytes each
//  int bank_select = address / (16384 * 16);
//  int memory_index = address % (16384 * 16);
//
//  // 2 banks hold 64b of data each
//  // [0-7]     [8-15]
//  // [16-23]   [24-32]
//  int macro_select = memory_index % 16 / 8;
//  int macro_address = memory_index / 16;
//  int byte_select = (memory_index % 16 % 8);
//
//  // reg[64-1:0] memory[2048-1:0][8-1:0]
//  // {row,col} = macro_address
//  int address_mda[2];
//  address_mda[0] = macro_address / 8;
//  address_mda[1] = macro_address % 8;
//
//  std::string bitstring = toBinaryString(data);
//
//  spdlog::debug("Writing to bank {} macro {} address {} {} byte {} data {}",
//                bank_select, macro_select, address_mda[0], address_mda[1],
//                byte_select, bitstring);

  // SRAM is composed of 512 KB blocks
  // Address already in bytes
  int block_select = address / (512 * 1024);
  int block_address = address % (512 * 1024);

  // Each block is composed of macros with 4096 words
  int bank_select = (block_address) / (4096 * 16);
  int bank_address = (block_address) % (4096 * 16);

  // Now choose which macro, since each macro holds 64-bit words
  // 16 bytes = 128 bits; 8 bytes = 64 bits
  int macro_select = (bank_address % 16) / 8;
  int macro_address = bank_address / 16;
  int byte_select = (bank_address % 16 % 8);
  
  int address_mda[1];
  address_mda[0] = macro_address;

  std::string bitstring = toBinaryString(data);

  spdlog::debug("Writing to block {} bank {} macro {} address {} byte {} data {}",
                block_select, bank_select, macro_select, address_mda[0],
                byte_select, bitstring);

#ifdef EMU
  unsigned buf[2];
  marg_read(marg_handles[bank_select][macro_select], macro_address, buf);
  uint64_t existingValue = (static_cast<uint64_t>(buf[1]) << 32) | buf[0];
  std::string strVal =
      std::bitset<64>(existingValue).to_string();  // string conversion
#else
  vpiHandle sram_handle = get_sram_handle(block_select, bank_select, macro_select);
//  vpiHandle wordHandle = vpi_handle_by_multi_index(sram_handle, 2, address_mda);
  vpiHandle wordHandle = vpi_handle_by_multi_index(sram_handle, 1, address_mda);

  s_vpi_value v;
  v.format = vpiBinStrVal;
  vpi_get_value(wordHandle, &v);
  std::string strVal(v.value.str);
#endif
  // bytes go from right to left, while string goes from left to right
  // therefore, we need to do insert at 64 - (byte_select + 1)*8
  //std::cout << strVal << std::endl;
  //std::cout << bitstring << std::endl;
  strVal.replace(64 - (byte_select + 1) * 8, 8, bitstring);

  spdlog::debug(strVal);

#ifdef EMU
  // convert to bitset
  uint64_t newValue = std::bitset<64>(strVal).to_ullong();
  buf[0] = newValue;
  buf[1] = newValue >> 32;
  marg_write(marg_handles[bank_select][macro_select], macro_address, buf);
#else
  v.value.str = const_cast<char*>(strVal.c_str());
  vpi_put_value(wordHandle, &v, NULL, vpiNoDelay);
#endif
}

uint8_t SRAMMemoryModel::fromBinaryString(const std::string& data) {
  unsigned long long result = std::bitset<8>(data).to_ullong();
  return (uint8_t)result;
}

uint8_t SRAMMemoryModel::readFromMemory(int address) {
  //// SRAM is composed of banks of 16384 words of 16 bytes each
  //int bank_select = address / (16384 * 16);
  //int memory_index = address % (16384 * 16);

  //// 2 banks hold 64b of data each
  //// [0-7]     [8-15]
  //// [16-23]   [24-32]
  //int macro_select = memory_index % 16 / 8;
  //int macro_address = memory_index / 16;
  //int byte_select = (memory_index % 16 % 8);

  //// reg[64-1:0] memory[2048-1:0][8-1:0]
  //// {row,col} = macro_address
  //int address_mda[2];
  //address_mda[0] = macro_address / 8;
  //address_mda[1] = macro_address % 8;

  //spdlog::debug("Reading from bank {} macro {} address {} {} byte {}",
  //              bank_select, macro_select, address_mda[0], address_mda[1],
  //              byte_select);

  // SRAM is composed of 1 MB blocks
  // Address already in bytes
  int block_select = address / (512 * 1024);
  int block_address = address % (512 * 1024);

  // Each block is composed of macros with 4096 words
  int bank_select = (block_address) / (4096 * 16);
  int bank_address = (block_address) % (4096 * 16);

  // Now choose which macro, since each macro holds 64-bit words
  // 16 bytes = 128 bits; 8 bytes = 64 bits
  int macro_select = (bank_address % 16) / 8;
  int macro_address = bank_address / 16;
  int byte_select = (bank_address % 16 % 8);
  
  int address_mda[1];
  address_mda[0] = macro_address;

  spdlog::debug("Reading from block {} bank {} macro {} address {} byte {}",
                block_select, bank_select, macro_select, address_mda[0],
                byte_select);

#ifdef EMU
  unsigned buf[2];
  marg_read(marg_handles[bank_select][macro_select], macro_address, buf);
  uint64_t existingValue = (static_cast<uint64_t>(buf[1]) << 32) | buf[0];
  std::string strVal =
      std::bitset<64>(existingValue).to_string();  // string conversion
#else
  vpiHandle sram_handle = get_sram_handle(block_select, bank_select, macro_select);
//  vpiHandle wordHandle = vpi_handle_by_multi_index(sram_handle, 2, address_mda);
  vpiHandle wordHandle = vpi_handle_by_multi_index(sram_handle, 1, address_mda);

  s_vpi_value v;
  v.format = vpiBinStrVal;
  vpi_get_value(wordHandle, &v);

  std::string strVal(v.value.str);
#endif
  // bytes go from right to left, while string goes from left to right
  // therefore, we need to do insert at 64 - (byte_select + 1)*8
  std::string byte = strVal.substr(64 - (byte_select + 1) * 8, 8);

  // Detect read of uninitialized memory
  if (byte.find('x') != std::string::npos) {
    spdlog::error(
        "readFromMemory() from uninitialized memory at address {}: {}", address,
        byte);

    return 0;
    //std::exit(1);
  }

  return fromBinaryString(byte);
}

void SRAMMemoryModel::flushWrites() {
#ifdef EMU
  for (int bank = 0; bank < 8; bank++) {
    for (int macro = 0; macro < 2; macro++) {
      std::stringstream fmt;

      fmt << "TestDriver.testHarness.testHarness.chiptop.system.tlsramWrapper."
             "sram_"
          << bank << ".sramBankWrapper.sramBank.sram_block_" << macro
          << ".MEMORY";

      int vm_handle = marg_vmem_handle(strdup(fmt.str().c_str()));
      marg_put(marg_handles[bank][macro], 0, 16384, vm_handle, 0);
    }
  }
#endif
}

void SRAMMemoryModel::prepareReads() {
#ifdef EMU
  for (int bank = 0; bank < 8; bank++) {
    for (int macro = 0; macro < 2; macro++) {
      std::stringstream fmt;

      fmt << "TestDriver.testHarness.testHarness.chiptop.system.tlsramWrapper."
             "sram_"
          << bank << ".sramBankWrapper.sramBank.sram_block_" << macro
          << ".MEMORY";

      int vm_handle = marg_vmem_handle(strdup(fmt.str().c_str()));
      marg_get(marg_handles[bank][macro], 0, 16384, vm_handle, 0);
    }
  }
#endif
}
