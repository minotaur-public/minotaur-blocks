#include "SoCSimulation.h"

#include <sys/stat.h>
#include <sys/types.h>

#include <fstream>
#include <iostream>
#include <string>

#include "spdlog/spdlog.h"
#include "test/common/GoldModel.h"

void run_op(std::vector<SimplifiedParams> params_list,
            INPUT_DATATYPE *sramMemory, INPUT_DATATYPE *rramMemory,
            std::vector<MemoryMap> memoryMap) {
  // intentionally empty
  // this function would be called in the accelerator environment to run the
  // SystemC model. in this environment, the operation is run on the SoC using
  // the CPU, not through the verification environment
}

SoCSimulation::SoCSimulation() : Simulation() {
  sims.clear();
  sims.push_back("customposit");
  workloadCount = 0; // Pointer to current workload to run
}

void SoCSimulation::loadMemory() {
  std::vector<MemoryModel *> memories;

  socMemory = new SoCMemoryModel();
  positMemory = new SimpleMemoryModel<INPUT_DATATYPE>(false);

  memories.push_back(socMemory);
  memories.push_back(positMemory);

  // Load first tests input
  for (MemoryModel *memModel : memories) {
    memModel->loadModelActivations(workloads.front().params,
                                   workloads.front().files,
                                   workloads.front().memoryMap, true);
  }

  // Load weights, biases for all layers
  for (const Workload &workload : workloads) {
    socMemory->setBandwidthMode(workload.params.bandwidth_mode);
    for (MemoryModel *memModel : memories) {
      memModel->loadModelParams(workload.params, workload.files,
                                workload.memoryMap, true);
    }
  }

  // load last layer reference outputs
  for (MemoryModel *memModel : memories) {
    memModel->loadReferenceOutput(workloads.back().params,
                                  workloads.back().files,
                                  workloads.back().params.ACC_T_OUTPUT);
  }

  // for emulation, we need to flush all writes to the memory after we've
  // finished all data loading
  socMemory->flushWrites();
}

int SoCSimulation::checkOutput() {
  // We run the Posit Gold Model as reference
  if (workloadCount < workloads.size()) {
    SimplifiedParams currentParams = workloads.at(workloadCount).params;
    MemoryMap memoryMap = workloads.at(workloadCount).memoryMap;

    int X, Y, C, K, FX, FY, STRIDE;
    X = currentParams.loops[0][currentParams.inputXLoopIndex[0]] *
        currentParams.loops[1][currentParams.inputXLoopIndex[1]];
    Y = currentParams.loops[0][currentParams.inputYLoopIndex[0]] *
        currentParams.loops[1][currentParams.inputYLoopIndex[1]];
    C = currentParams.loops[1][currentParams.reductionLoopIndex[1]] *
        IC_DIMENSION; // WARN: this may be wrong
    K = currentParams.loops[0][currentParams.weightLoopIndex[0]] *
        currentParams.loops[1][currentParams.weightLoopIndex[1]] *
        OC_DIMENSION; // WARN: this may be wrong
    FX = currentParams.loops[1][currentParams.fxIndex];
    FY = currentParams.loops[1][currentParams.fyIndex];
    STRIDE = currentParams.STRIDE;

    if (currentParams.REPLICATION) {
      FX = 7;
      C = 3;
    }

    if (currentParams.MAXPOOL) {
      X /= 2;
      Y /= 2;
    }

    if (currentParams.AVGPOOL) {
      X = 1;
      Y = 1;
    }

    size_t size = X * Y * K;
    if (currentParams.SOFTMAX || currentParams.SOFTMAX_GRAD) {
      size = X * Y;
    } else if (currentParams.CROSS_ENTROPY_GRAD) {
      size = X;
    } else if (currentParams.NO_NORM_GRAD) {
      size = C;
    } else if (currentParams.WEIGHT_UPDATE) {
      size = X * C;
    }

    std::cout << "Performing " + workloads.at(workloadCount).name + ":"
              << std::endl;
    std::cout << "(" << X << "x" << Y << "x" << C << ")" << " * " << "(" << FX
              << "x" << FY << "x" << C << "x" << K << ")" << " -> (" << X << "x"
              << Y << "x" << K << ") [size=" << size << "]" << std::endl;

    run_gold_model(
        currentParams, positMemory->sram + currentParams.INPUT_OFFSET,
        (memoryMap.weights ? positMemory->rram : positMemory->sram) +
            +currentParams.WEIGHT_OFFSET,
        positMemory->sram + currentParams.OUTPUT_OFFSET,
        (currentParams.ATTENTION_MASK ? positMemory->sram : positMemory->rram) +
            currentParams.BIAS_OFFSET,
        positMemory->sram + currentParams.RESIDUAL_OFFSET,
        positMemory->sram + currentParams.WEIGHT_RESIDUAL_OFFSET);


    workloadCount++;
    return 0;
  }

  // When we are done with all workloads, we check the final output
  SimplifiedParams currentParams = workloads.at(workloadCount - 1).params;
  std::cout << "Checking final output... "
            << workloads.at(workloadCount - 1).name << std::endl;

  // for emulation, before we can read memory, we need to refresh the internal
  // memory buffers
  socMemory->prepareReads();

  int X, Y, C, K, FX, FY, STRIDE;
  X = currentParams.loops[0][currentParams.inputXLoopIndex[0]] *
      currentParams.loops[1][currentParams.inputXLoopIndex[1]];
  Y = currentParams.loops[0][currentParams.inputYLoopIndex[0]] *
      currentParams.loops[1][currentParams.inputYLoopIndex[1]];
  C = currentParams.loops[1][currentParams.reductionLoopIndex[1]] *
      IC_DIMENSION;
  K = currentParams.loops[0][currentParams.weightLoopIndex[0]] *
      currentParams.loops[1][currentParams.weightLoopIndex[1]] * OC_DIMENSION;
  FX = currentParams.loops[1][currentParams.fxIndex];
  FY = currentParams.loops[1][currentParams.fyIndex];
  STRIDE = currentParams.STRIDE;

  size_t size = X * Y * K;
  if (currentParams.SOFTMAX || currentParams.SOFTMAX_GRAD) {
    size = X * Y;
  } else if (currentParams.CROSS_ENTROPY_GRAD) {
    size = X;
  } else if (currentParams.NO_NORM_GRAD) {
    size = C;
  } else if (currentParams.WEIGHT_UPDATE) {
    size = X * C;
  }

  // Records absolute differences
  int abs_diff_buckets[5] = {0, 0, 0, 0, 0};
  // Records relative differences
  int rel_diff_buckets[5] = {0, 0, 0, 0, 0};

  double always_zero = 0.0;

  // Ensure output directory exists
  std::string output_dir = "output";
  struct stat st;
  if (stat(output_dir.c_str(), &st) == -1) {
    mkdir(output_dir.c_str(), 0700);
  }

  // Open output file for comparison
  std::string filename(output_dir + "/" + this->modelName + "_" + this->tests +
                       "_soc_vs_hlsgold_outputs.txt");
  std::cout << "Writing comparison of " << this->modelName << " -> "
            << this->tests << " (size: " << size << ") to: " << filename
            << std::endl;
  std::ofstream diffFile(filename);
  diffFile << this->modelName << " -> " << this->tests << std::endl;

  for (int index = 0; index < size; index++) {
    float a, b;
    if (currentParams.ACC_T_OUTPUT) {
      int addr = currentParams.OUTPUT_OFFSET + 2 * index;
      uint8_t lower = socMemory->sram->readFromMemory(addr);
      uint8_t upper = socMemory->sram->readFromMemory(addr + 1);

      Posit<16, 1> p16;
      p16.setbits(upper << 8 + lower);
      a = static_cast<float>(p16);

      lower = positMemory->sram[addr].bits_rep();
      upper = positMemory->sram[addr + 1].bits_rep();
      p16.setbits(upper << 8 + lower);
      b = static_cast<float>(p16);
    } else {
      int addr = currentParams.OUTPUT_OFFSET + index;
      uint8_t sramValue = socMemory->sram->readFromMemory(addr);

      INPUT_DATATYPE p8;
      p8.setbits(sramValue);

      a = static_cast<float>(p8);
      b = static_cast<float>(positMemory->sram[addr]);
    }

    always_zero += abs(a) + abs(b);
    float abs_diff = abs(a - b);

    // Write the two values + error scale indicator to file
    diffFile << a << " vs. " << b << " ";
    for (float i = 0.001; i < abs_diff; i *= 10.0) {
      diffFile << "*";
    }
    diffFile << std::endl;

    if (abs_diff < 0.001) {
      abs_diff_buckets[0]++;
    }
    if (abs_diff < 0.01) {
      abs_diff_buckets[1]++;
    }
    if (abs_diff < 0.1) {
      abs_diff_buckets[2]++;
    }
    if (abs_diff < 1) {
      abs_diff_buckets[3]++;
    } else {
      abs_diff_buckets[4]++;
    }

    // Does not fully protect against overflow, but lets not over engineer
    if (a == 0 && b == 0) {
      rel_diff_buckets[0]++;
      rel_diff_buckets[1]++;
      rel_diff_buckets[2]++;
      rel_diff_buckets[3]++;
      continue;
    } else {
      // See https://en.wikipedia.org/wiki/Relative_change_and_difference
      float rel_diff = abs_diff / ((abs(a) + abs(b)) / 2);
      if (rel_diff < 0.001) {
        rel_diff_buckets[0]++;
      }
      if (rel_diff < 0.01) {
        rel_diff_buckets[1]++;
      }
      if (rel_diff < 0.1) {
        rel_diff_buckets[2]++;
      }
      if (rel_diff < 1) {
        rel_diff_buckets[3]++;
      } else {
        rel_diff_buckets[4]++;
      }
    }
  }

  std::cout << "Difference Count:" << std::endl;
  std::cout << "< 0.001: " << abs_diff_buckets[0] << "("
            << (float)abs_diff_buckets[0] / size * 100.0 << "%)" << std::endl;
  std::cout << "< 0.01: " << abs_diff_buckets[1] << "("
            << (float)abs_diff_buckets[1] / size * 100.0 << "%)" << std::endl;
  std::cout << "< 0.1: " << abs_diff_buckets[2] << "("
            << (float)abs_diff_buckets[2] / size * 100.0 << "%)" << std::endl;
  std::cout << "< 1: " << abs_diff_buckets[3] << "("
            << (float)abs_diff_buckets[3] / size * 100.0 << "%)" << std::endl;
  std::cout << "> 1: " << abs_diff_buckets[4] << "("
            << (float)abs_diff_buckets[4] / size * 100.0 << "%)" << std::endl;

  std::cout << "Percent Difference Count:" << std::endl;
  std::cout << "< 0.001: " << rel_diff_buckets[0] << "("
            << (float)rel_diff_buckets[0] / size * 100.0 << "%)" << std::endl;
  std::cout << "< 0.01: " << rel_diff_buckets[1] << "("
            << (float)rel_diff_buckets[1] / size * 100.0 << "%)" << std::endl;
  std::cout << "< 0.1: " << rel_diff_buckets[2] << "("
            << (float)rel_diff_buckets[2] / size * 100.0 << "%)" << std::endl;
  std::cout << "< 1: " << rel_diff_buckets[3] << "("
            << (float)rel_diff_buckets[3] / size * 100.0 << "%)" << std::endl;
  std::cout << "> 1: " << rel_diff_buckets[4] << "("
            << (float)rel_diff_buckets[4] / size * 100.0 << "%)" << std::endl;
  std::cout << std::endl;

  if (always_zero == 0.0) {
    std::cout << "WARNING: All compared values are zero!" << std::endl;
  }

  // Ideally, these buckets should be non-overlapping...
  // TODO(fpedd): Subtract the next smaller bucket to make them
  // non-overlapping
  float rel_err = (1 - (float)rel_diff_buckets[1] / size) * 0.001 +
                  (1 - (float)rel_diff_buckets[2] / size) * 0.01 +
                  (1 - (float)rel_diff_buckets[3] / size) * 0.1 +
                  (float)rel_diff_buckets[4] / size;

  std::cout << "Relative error: " << rel_err
            << " (tolerance: " << this->tolerance << ")" << std::endl;
  if (rel_err > this->tolerance) {
    std::cout << "Test failed!" << std::endl;
  } else {
    std::cout << "Test passed!" << std::endl;
  }

  return 0;
}

inline uint8_t get_byte_at_index(int32_t val, int index) {
  uint32_t mask = ((1 << 8) - 1) << (index * 8);
  return (val & mask) >> (index * 8);
}

void SoCSimulation::checkSingleOutput(svLogicVecVal *data,
                                      svLogicVecVal *address) {
  static int outputCount = 0;

  spdlog::info("[{} / {}]: {}", workloadCount, workloads.size(), outputCount);

  int index = address->aval;

  for (int k_i = 0; k_i < OC_DIMENSION; k_i++) {
    uint8_t result = get_byte_at_index(data[k_i / 4].aval, k_i % 4);
    INPUT_DATATYPE positResult;
    positResult.setbits(result);

    INPUT_DATATYPE positExpected = positMemory->sram[index + k_i];

    if (positExpected.bits_rep() != positResult.bits_rep()) {
      std::cout << "[" << index + k_i << "] "
                << "Expected: " << static_cast<float>(positExpected) << "( "
                << positExpected.bits_rep() << " ) "
                << "Received: " << static_cast<float>(positResult) << "( "
                << positResult.bits_rep() << " ) " << std::endl;
    }

    outputCount++;
  }
}
