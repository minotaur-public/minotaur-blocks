#include <assert.h>

#include "SoCSimulation.h"
#include "spdlog/cfg/env.h"

extern bool reset_triggered;

SoCSimulation* sim = NULL;

bool running_a_network() {
  /* If the TESTS and NETWORK environment variables are set, then we are
   * running a network. Otherwise, we are running a simple testbench. */
  const char* tests = std::getenv("TESTS");
  const char* network = std::getenv("NETWORK");
  return tests && network;
}

extern "C" void load_memory() {
  if (running_a_network()) {
    spdlog::cfg::load_env_levels();  // load log levels from env variable
                                     // SPDLOG_LEVEL

    assert(sim == NULL && "load_memory() called twice");
    sim = new SoCSimulation();
    printf("Loading memory\n"); 
    sim->loadMemory();
    sim->checkOutput();  // Run the gold model once to compare immediately
  }
}

extern "C" void check_all_outputs() {
  // Compare a complete tensor
  if (running_a_network()) {
    assert(sim != NULL && "check_all_outputs() called before load_memory()");
    sim->checkOutput();
  }
}

extern "C" void check_single_output(svLogicVecVal* data,
                                    svLogicVecVal* address) {
  // Compare a single element of a tensor coming directly from the vector unit
  // on the fly to make failing tests fail earlier
  if (running_a_network()) {
    assert(sim != NULL && "check_single_output() called before load_memory()");
    sim->checkSingleOutput(data, address);
  }
}
