#ifdef SOC_COSIM
#include <acc_user.h>
#include <svdpi.h>
#include <sysc/datatypes/bit/sc_lv.h>
#include <vcs_acc_user.h>
#include <vcsuser.h>
#include <vpi_user.h>

#include <bitset>
#include <cstring>
#include <deque>
#include <fstream>
#include <iostream>
#include <random>
#include <sstream>
#include <string>

#include "networks/PositTypes.h"
#include "networks/accelerator_types.h"

#define DIMENSION 16

extern Posit<8, 1>* sramModel;
extern Posit<8, 1>* rramModel;

extern Posit<8, 1>* outputs;
extern int outputOffset;
extern int outputsSize;

std::deque<int>* inputQueue;
std::deque<int>* weightQueue;
std::deque<int>* biasQueue;
std::deque<int>* residualQueue;

// SimplifiedParams params;
// MemoryMap memMap;

std::deque<sc_dt::sc_lv<32> >* sysc_serialMatrixParamsIn;
std::deque<sc_dt::sc_lv<32> >* sysc_serialVectorParamsIn;
std::deque<sc_dt::sc_lv<64> >* sysc_inputAddressRequest;
std::deque<sc_dt::sc_lv<128> >* sysc_inputAddressResponse;
std::deque<sc_dt::sc_lv<64> >* sysc_weightAddressRequest;
std::deque<sc_dt::sc_lv<128> >* sysc_weightAddressResponse;
std::deque<sc_dt::sc_lv<64> >* sysc_vectorFetch0AddressRequest;
std::deque<sc_dt::sc_lv<128> >* sysc_vectorFetch0AddressResponse;
std::deque<sc_dt::sc_lv<64> >* sysc_vectorFetch1AddressRequest;
std::deque<sc_dt::sc_lv<128> >* sysc_vectorFetch1AddressResponse;
std::deque<sc_dt::sc_lv<64> >* sysc_vectorFetch2AddressRequest;
std::deque<sc_dt::sc_lv<128> >* sysc_vectorFetch2AddressResponse;
std::deque<sc_dt::sc_lv<128> >* sysc_vectorOutput;
std::deque<sc_dt::sc_lv<32> >* sysc_vectorOutputAddress;

void register_interface(
    std::deque<sc_dt::sc_lv<32> >* serialMatrixParamsIn,
    std::deque<sc_dt::sc_lv<32> >* serialVectorParamsIn,
    std::deque<sc_dt::sc_lv<64> >* inputAddressRequest,
    std::deque<sc_dt::sc_lv<128> >* inputAddressResponse,
    std::deque<sc_dt::sc_lv<64> >* weightAddressRequest,
    std::deque<sc_dt::sc_lv<128> >* weightAddressResponse,
    std::deque<sc_dt::sc_lv<64> >* vectorFetch0AddressRequest,
    std::deque<sc_dt::sc_lv<128> >* vectorFetch0AddressResponse,
    std::deque<sc_dt::sc_lv<64> >* vectorFetch1AddressRequest,
    std::deque<sc_dt::sc_lv<128> >* vectorFetch1AddressResponse,
    std::deque<sc_dt::sc_lv<64> >* vectorFetch2AddressRequest,
    std::deque<sc_dt::sc_lv<128> >* vectorFetch2AddressResponse,
    std::deque<sc_dt::sc_lv<128> >* vectorOutput,
    std::deque<sc_dt::sc_lv<32> >* vectorOutputAddress) {
  sysc_serialMatrixParamsIn = serialMatrixParamsIn;
  sysc_serialVectorParamsIn = serialVectorParamsIn;
  sysc_inputAddressRequest = inputAddressRequest;
  sysc_inputAddressResponse = inputAddressResponse;
  sysc_weightAddressRequest = weightAddressRequest;
  sysc_weightAddressResponse = weightAddressResponse;
  sysc_vectorFetch0AddressRequest = vectorFetch0AddressRequest;
  sysc_vectorFetch0AddressResponse = vectorFetch0AddressResponse;
  sysc_vectorFetch1AddressRequest = vectorFetch1AddressRequest;
  sysc_vectorFetch1AddressResponse = vectorFetch1AddressResponse;
  sysc_vectorFetch2AddressRequest = vectorFetch2AddressRequest;
  sysc_vectorFetch2AddressResponse = vectorFetch2AddressResponse;
  sysc_vectorOutput = vectorOutput;
  sysc_vectorOutputAddress = vectorOutputAddress;
}

void init_checkers() {
  inputQueue = new std::deque<int>;
  weightQueue = new std::deque<int>;
  biasQueue = new std::deque<int>;
  residualQueue = new std::deque<int>;

  // params = layerParams;
  // memMap = layerMmap;

  // sramModel = sram;
  // rramModel = rram;
  // outputs = outputArray;
  // outputOffset = outputArrayOffset;
  // outputsSize = outputArraySize;
}

inline uint8_t get_byte_at_index(int32_t val, int index) {
  uint32_t mask = ((1 << 8) - 1) << (index * 8);
  return (val & mask) >> (index * 8);
}

void log_request(svLogicVecVal* memRequest, std::deque<int>* queue) {
  // memrequest (assume access of 16bytes)
  int32_t address = memRequest[0].aval;
  int32_t size = memRequest[1].aval;
  for (int i = 0; i < size; i++) {
    queue->push_back(address + i);
  }
}

template <int width>
void check_val(svLogicVecVal* vec, sc_dt::sc_lv<width> data,
               const std::string& interface) {
  // compare chunks of 32bits
  for (int i = 0; i < width / 32; i++) {
    std::bitset<32> rtlbits(vec[i].aval);
    std::bitset<32> syscbits(data.range((i + 1) * 32 - 1, i * 32).to_string());

    if (rtlbits != syscbits) {
      std::cout << i << " mismatch on interface " << interface << ": "
                << std::endl;
      std::cout << "RTL: " << rtlbits << " SYSC: " << syscbits << std::endl;
    }
  }
}

template <int width>
void check_interface(svLogicVecVal* vec,
                     std::deque<sc_dt::sc_lv<width> >* syscQueue,
                     const std::string& interface) {
  assert(syscQueue->size() > 0);
  sc_dt::sc_lv<width> syscVal = syscQueue->front();
  syscQueue->pop_front();

  check_val<width>(vec, syscVal, interface);
}

extern "C" void check_matrix_params(svLogicVecVal* vec) {
  check_interface<32>(vec, sysc_serialMatrixParamsIn, "serialMatrixParamsIn");
}

extern "C" void check_vector_params(svLogicVecVal* vec) {
  check_interface<32>(vec, sysc_serialVectorParamsIn, "serialVectorParamsIn");
}

extern "C" void log_input_request(svLogicVecVal* memRequest) {
  check_interface<64>(memRequest, sysc_inputAddressRequest,
                      "inputAddressRequest");
  log_request(memRequest, inputQueue);
}
extern "C" void log_weight_request(svLogicVecVal* memRequest) {
  check_interface<64>(memRequest, sysc_weightAddressRequest,
                      "weightAddressRequest");
  log_request(memRequest, weightQueue);
}
extern "C" void log_bias_request(svLogicVecVal* memRequest) {
  check_interface<64>(memRequest, sysc_vectorFetch2AddressRequest,
                      "vectorFetch2AddressRequest");
  log_request(memRequest, biasQueue);
}
extern "C" void log_residual_request(svLogicVecVal* memRequest) {
  check_interface<64>(memRequest, sysc_vectorFetch1AddressRequest,
                      "vectorFetch1AddressRequest");
  log_request(memRequest, residualQueue);
}

// void check_response(svLogicVecVal* data, std::deque<int>* queue,
//                     MemorySource memSource, std::string interface) {
//   for (int i = 0; i < DIMENSION; i++) {
//     int address = queue->front();
//     queue->pop_front();

//     Posit<8, 1> expected;
//     if (memSource == SRAM) {
//       expected = sramModel[address];
//     } else {
//       expected = rramModel[address];
//     }

//     Posit<8, 1> result;
//     result.bits = get_byte_at_index(data[i / 4].aval, i % 4);

//     if (expected.bits != result.bits) {
//       std::cout << "Unexpected memory response for " << interface << " at "
//                 << address << std::endl
//                 << "expected: " << expected.bits
//                 << ", received: " << result.bits << std::endl;
//     }
//   }
// }

extern "C" void check_input_response(svLogicVecVal* data) {
  check_interface<128>(data, sysc_inputAddressResponse, "inputAddressResponse");
  // check_response(data, inputQueue, memMap.inputs, "inputs");
}
extern "C" void check_weight_response(svLogicVecVal* data) {
  check_interface<128>(data, sysc_weightAddressResponse,
                       "weightAddressResponse");
  // check_response(data, weightQueue, memMap.weights, "weights");
}
extern "C" void check_bias_response(svLogicVecVal* data) {
  check_interface<128>(data, sysc_vectorFetch2AddressResponse,
                       "vectorFetch2AddressResponse");
  // check_response(data, biasQueue, memMap.bias, "bias");
}
extern "C" void check_residual_response(svLogicVecVal* data) {
  check_interface<128>(data, sysc_vectorFetch1AddressResponse,
                       "vectorFetch1AddressResponse");
  // check_response(data, residualQueue, memMap.residual, "residual");
}

extern "C" void check_output_data(svLogicVecVal* data) {
  static int outputCount = 0;
  static int outputSize = sysc_vectorOutput->size();

  check_interface<128>(data, sysc_vectorOutput, "vectorOutput");
}

extern "C" void check_output_address(svLogicVecVal* data) {
  check_interface<32>(data, sysc_vectorOutputAddress, "vectorOutputAddress");
}
#endif