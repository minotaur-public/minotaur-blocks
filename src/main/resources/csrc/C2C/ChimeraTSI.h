#ifndef CHIMERA_TSI_H
#define CHIMERA_TSI_H

#include <cstdint>
#include <deque>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include "MemoryLoader.h"

class ChimeraTSI : public MemoryLoader {
 public:
  ChimeraTSI(char *binary, char *stdout_file, bool loadBinary);
  std::deque<uint32_t> rx_queue;
  std::deque<uint32_t> tx_queue;
  std::mutex *io_mutex;

  bool finished;
  bool rst_req;
  void writeMemory(uint64_t, size_t, const void *);
  void run();

 private:
  std::string binary_file;
  std::string stdout_file;
  bool loadBinary;

  void write_chunk(uint64_t, size_t, const void *);
  void continue_execution();
  void power_on();
  uint32_t read_word();
  void service_syscalls();
  void spin_on();
  void spin_off();
  void dummy_memory_write();

  // Maximum size of a chunk that can be sent over C2C and its alignment
  // requirements.
  size_t CHUNK_MAX_SIZE = 1024;
  size_t CHUNK_ALIGN = 4;

  // The two C2C commands: Read and Write
  uint32_t SAI_CMD_WRITE = 1;
  uint32_t SAI_CMD_READ = 0;

  uint64_t MSIP_BASE = 0x2000000;
  uint64_t PMU_BASE = 0x13000000;
  uint64_t BOOT_ADDR_REG = 0x11000004;
};
#endif
