#include <svdpi.h>
#include <vpi_user.h>

#include <string>
#include <thread>
#include <vector>

#include "ChimeraTSI.h"

ChimeraTSI *chimera_tsi = NULL;

#ifdef SOC_COSIM
bool syscDone = false;
#else
bool syscDone = true;
#endif

uint64_t cycles = 0;
bool reset_triggered = false;

uint64_t get_cycles() { return cycles; }

extern "C" void set_trigger_reset();
void toggle_reset_wrapper() {
  // wait 100 cycles first
  uint64_t startCycles = get_cycles();
  while (get_cycles() - startCycles < 100)
    ;

  // wait for all outgoing messages to finish being sent
  while (!chimera_tsi->tx_queue.empty())
    ;

  // wait 100 cycles after this
  startCycles = get_cycles();
  while (get_cycles() - startCycles < 100)
    ;

  svSetScope(svGetScopeFromName("TestDriver"));
  set_trigger_reset();
  printf("C2C requested reset\n");
}

static svScope TestDriver_scope;
extern "C" void register_scope_vlog() {
  TestDriver_scope = svGetScope();
  printf("C2C TestDriver scope set \n");
}

extern "C" int c2c_tick(unsigned char out_valid, unsigned char *out_ready,
                        int out_bits,

                        unsigned char *in_valid, unsigned char in_ready,
                        int *in_bits,

                        char *binary, char *stdout_file,
                        unsigned char loadBinary) {
  *out_ready = 1;  // always ready in simulation

  cycles++;

  if (syscDone) {
    if (!chimera_tsi) {
      chimera_tsi = new ChimeraTSI(binary, stdout_file, loadBinary);
      std::thread t(&ChimeraTSI::run, chimera_tsi);  // start new thread
      t.detach();
    }

    if (chimera_tsi->rst_req) {
      svSetScope(TestDriver_scope);
      set_trigger_reset();
      chimera_tsi->rst_req = false;
      reset_triggered = true;
    }

    // Send contents of TX queue to chip
    if (chimera_tsi->tx_queue.empty() || in_ready == 0) {
      *in_valid = 0;
    } else {
      *in_valid = 1;
      chimera_tsi->io_mutex->lock();
      *in_bits = chimera_tsi->tx_queue.front();
      chimera_tsi->tx_queue.pop_front();
      chimera_tsi->io_mutex->unlock();
    }

    // Send chip outputs to RX queue
    if (out_valid) {
      chimera_tsi->io_mutex->lock();
      chimera_tsi->rx_queue.push_back(out_bits);
      chimera_tsi->io_mutex->unlock();
    }
    return chimera_tsi->finished ? 1 : 0;
  } else {
    return 0;
  }
}
