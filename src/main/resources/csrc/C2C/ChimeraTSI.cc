#include "ChimeraTSI.h"

#include <assert.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include <chrono>
#include <cstring>
#include <fstream>
#include <iostream>
#include <map>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include "ElfLoader.h"
#include "fesvr/elf.h"

uint64_t get_cycles();

void toggle_reset_wrapper();

ChimeraTSI::ChimeraTSI(char *binary, char *path_to_stdout_file, bool loadBinary)
    : binary_file(binary),
      stdout_file(path_to_stdout_file),
      loadBinary(loadBinary) {
  io_mutex = new std::mutex();
  finished = false;
  rst_req = false;
}

void ChimeraTSI::run() {
  spin_on();
  dummy_memory_write();

  if (loadBinary) {
    ElfLoader elfLoader(binary_file, this);
    elfLoader.load();
  }

  continue_execution();
  service_syscalls();
}

// Write some data into the Chimera's memory.
// Because all reads and writes have to be word-aligned, we
// need to take extra care.
void ChimeraTSI::writeMemory(uint64_t dst_addr, size_t len, const void *src) {
  size_t align = CHUNK_ALIGN;

  // First, if the write starts in the middle of a word, we need to read
  // that whole word, modify its latter half, and write it back.
  if (len && (dst_addr & (align - 1))) {
    size_t this_len = std::min(len, align - size_t(dst_addr & (align - 1)));
    uint8_t chunk[align];

    // TODO - dirty
    //    read_chunk(dst_addr & ~(align-1), align, chunk);
    // memcpy(chunk + (dst_addr & (align-1)), src, this_len);
    // write_chunk(dst_addr & ~(align-1), align, chunk);

    src = (char *)src + this_len;
    dst_addr += this_len;
    len -= this_len;
  }

  // Next, if the write ends in the middle of a word, we need to read
  // that whole word, modify its first half, and write it back.
  if (len & (align - 1)) {
    size_t this_len = len & (align - 1);
    size_t start = len - this_len;
    uint8_t chunk[align];

    // TODO - dirty
    // read_chunk(dst_addr + start, align, chunk);
    // memcpy(chunk, (char*)src + start, this_len);
    // write_chunk(dst_addr + start, align, chunk);

    len -= this_len;
  }

  // Now, we are aligned and can write chunk-by-chunk.
  for (size_t pos = 0; pos < len; pos += CHUNK_MAX_SIZE)
    write_chunk(dst_addr + pos, std::min(CHUNK_MAX_SIZE, len - pos),
                (char *)src + pos);
}

// Write a chunk of memory
void ChimeraTSI::write_chunk(uint64_t taddr, size_t nbytes, const void *src) {
  const uint32_t *src_data = static_cast<const uint32_t *>(src);
  size_t len = nbytes / sizeof(uint32_t);

  // FIXME - TODO : comment TSI messaging
  std::cout << "TSI: Writing " << nbytes << " bytes to 0x" << std::hex << taddr
            << std::dec << std::endl;

  io_mutex->lock();

  // tx_queue.push_back(SAI_CMD_WRITE);
  for (int i = 0; i < 4; i++) {
    tx_queue.push_back(taddr & 0xFFFF);
    taddr = taddr >> 16;
  }

  size_t tx_len = 2 * len - 1;  // len is number of 16b words
  for (int i = 0; i < 4; i++) {
    tx_queue.push_back(tx_len & 0xFFFF);
    tx_len = tx_len >> 16;
  }

  // Push data
  for (int i = 0; i < len; i++) {
    for (int j = 0; j < 2; j++) {
      tx_queue.push_back((src_data[i] >> j * 16) & 0xFFFF);
    }
  }
  io_mutex->unlock();
}

void ChimeraTSI::continue_execution() {
  while (!tx_queue.empty()) {
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
  }

  // wait 100 cycles after this
  uint64_t startCycles = get_cycles();
  while (get_cycles() - startCycles < 100)
    ;

  rst_req = true;
}

// Prevent the CPU from executing code in SRAM by overwriting the boot address
// register with the bootrom address
void ChimeraTSI::spin_on() {
  uint64_t bootromAddress = 0x10000;
  write_chunk(BOOT_ADDR_REG, sizeof(uint64_t), &bootromAddress);
}

// Write SRAM address back to the boot address register
void ChimeraTSI::spin_off() {
  uint64_t sramAddress = 0x3000000;
  write_chunk(BOOT_ADDR_REG, sizeof(uint64_t), &sramAddress);
}

// The SRAM memory model has a strange bug where the first write is lost, due to
// X issues
void ChimeraTSI::dummy_memory_write() {
  // Write 1 word to each SRAM bank
  uint32_t one = 1;
  for (int bank = 0; bank < 8; bank++) {
    write_chunk(0x3000000 + 0x040000 * bank, sizeof(uint32_t), &one);
  }
}

void ChimeraTSI::power_on() {
  uint32_t one = 1;

  write_chunk(PMU_BASE, sizeof(uint32_t), &one);
}

uint32_t ChimeraTSI::read_word() {
  while (true) {
    if (!rx_queue.empty()) {
      io_mutex->lock();
      uint32_t word = rx_queue.front();
      rx_queue.pop_front();
      io_mutex->unlock();
      return word;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
  }
}

void ChimeraTSI::service_syscalls() {
  std::ofstream file(stdout_file);

  std::cout << "Waiting for syscalls..." << std::endl;
  while (!finished) {
    // uint32_t cmd = read_word();

    uint64_t addr = 0;
    for (int i = 0; i < 4; i++) {
      uint64_t word = read_word();
      addr = (word << 16 * i) | addr;
    }
    if (addr == 0xfffffffffffffffe) {  // exit
      printf("Execution finished.\n");
      finished = true;
      continue;
    }
    if (addr == 0xfffffffffffffffc) {
      printf("powering up chip in 1000 cycles\n");

      // flush rest of command
      for (int i = 0; i < 4; i++) {
        volatile uint64_t garbage = read_word();
      }

      uint64_t start = get_cycles();
      // wait for 1000 cycles
      while (get_cycles() - start < 10000) {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
      }
      printf("powering up chip\n");
      power_on();

      continue;
    }

    uint64_t len = 0;
    for (int i = 0; i < 4; i++) {
      uint64_t word = read_word();
      len = (word << 16 * i) | len;
    }
    len++;  // real length is 1 more than received value

    char *payload = (char *)malloc(2 * len + 1);
    for (int i = 0; i < len; i++) {
      uint32_t word = read_word();
      payload[i * 2] = word & 0xFF;
      payload[i * 2 + 1] = (word & 0xFF00) >> 8;
    }

    switch (addr) {
      case 0xfffffffffffffffd:  // printf
        printf("%s", payload);
        file << payload;
        break;
      default:
        printf("C2C transaction to addr %X of size %X\n", addr, len);
    }

    free(payload);
  }
}
