#pragma once

#include <fstream>
#include <string>

class RRAMJTAG {
 public:
  RRAMJTAG(char *file, char *stdoutFile);

  void run();

  void transfer_pin_values(unsigned char *jtag_tck, unsigned char *jtag_tms,
                           unsigned char *jtag_tdi, unsigned char jtag_tdo);

  char bist_enable;
  char rram_jtag_sel;
  bool finished;

 private:
  std::string directivesFile;
  std::ofstream logfile;

  char tck;
  char tms;
  char tdi;
  char tdo;

  volatile bool jtagReady;

  void perform_jtag_transaction(char jtag_tck, char jtag_tms, char jtag_tdi,
                                char *jtag_tdo);

  void cycle_pins(char, char, char *);

  void set_ir(uint8_t ir);
  uint64_t set_dr(uint64_t dr);
  uint64_t set_ir_dr(uint8_t ir, uint64_t dr);
  void rw_jtag_reg(uint8_t addr, uint64_t data, uint64_t expected_result);
  void bypass(uint32_t data);

  void wait_for_busy(char);
  void check_status();
};
