#include "RRAMJTAG.h"

#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>

RRAMJTAG::RRAMJTAG(char *file, char *stdout_file) {
  directivesFile = std::string(file);
  logfile = std::ofstream(std::string(stdout_file));

  bist_enable = 0;
  rram_jtag_sel = 0;
  finished = false;
  jtagReady = false;
}

// Interacts with the SoC, and transfers pin values
void RRAMJTAG::transfer_pin_values(unsigned char *jtag_tck,
                                   unsigned char *jtag_tms,
                                   unsigned char *jtag_tdi,
                                   unsigned char jtag_tdo) {
  if (jtagReady) {
    *jtag_tck = tck;
    *jtag_tms = tms;
    *jtag_tdi = tdi;
    tdo = jtag_tdo;

    jtagReady = false;
  }
}

// sets pin values, and blocks until pin values are transferred to the SoC
void RRAMJTAG::perform_jtag_transaction(char jtag_tck, char jtag_tms,
                                        char jtag_tdi, char *jtag_tdo) {
  tck = jtag_tck;
  tms = jtag_tms;
  tdi = jtag_tdi;
  if (jtag_tdo) {
    *jtag_tdo = tdo;
  }

  jtagReady = true;

  // block until transaction happens
  while (jtagReady)
    ;
}

void RRAMJTAG::cycle_pins(char jtag_tms, char jtag_tdi, char *jtag_tdo) {
  perform_jtag_transaction(0, jtag_tms, jtag_tdi, jtag_tdo);
  perform_jtag_transaction(1, jtag_tms, jtag_tdi, jtag_tdo);
  perform_jtag_transaction(0, jtag_tms, jtag_tdi, jtag_tdo);
}

/*
 * Sets the IR register
 * Starts and ends at the idle state
 */
void RRAMJTAG::set_ir(uint8_t ir) {
  const int ir_len = 8;

  /*
   * Set IR
   */
  cycle_pins(0, 0, 0);
  cycle_pins(1, 0, 0);
  cycle_pins(1, 0, 0);
  cycle_pins(0, 0, 0);
  cycle_pins(0, 0, 0);

  /*
   * Shift-IR state
   */
  for (int i = 0; i < ir_len; i++) {
    char loop_tms = i == ir_len - 1;
    char loop_tdi = (ir >> i) & 1;
    cycle_pins(loop_tms, loop_tdi, 0);
  }

  cycle_pins(1, 0, 0);
  cycle_pins(0, 0, 0);
}

/*
 * Sets the DR register
 * Starts and ends at the idle state
 */
uint64_t RRAMJTAG::set_dr(uint64_t dr) {
  const int dr_len = 40;
  uint64_t result = 0;
  cycle_pins(0, 0, 0);
  cycle_pins(1, 0, 0);
  cycle_pins(0, 0, 0);
  cycle_pins(0, 0, 0);

  for (int i = 0; i < dr_len; i++) {
    char loop_tms = i == dr_len - 1;
    char loop_tdi = (dr >> i) & 1;
    char loop_tdo;
    cycle_pins(loop_tms, loop_tdi, &loop_tdo);

    result = (((uint64_t)loop_tdo) << (dr_len - 1)) |
             (result >> 1);  // {tdo,result[dr_len-1:1]}
  }

  cycle_pins(1, 0, 0);
  cycle_pins(0, 0, 0);

  return result;
}

uint64_t RRAMJTAG::set_ir_dr(uint8_t ir, uint64_t dr) {
  set_ir(ir);
  uint64_t result = set_dr(dr);

  return result;
}

void RRAMJTAG::rw_jtag_reg(uint8_t addr, uint64_t data,
                           uint64_t expected_result) {
  uint8_t ir = (addr << 2) | (1 << 1);  // {6'haddr, 2'b10}
  uint64_t result = set_ir_dr(ir, data);
  std::cout << "Writing IR=" << std::hex << int{ir} << " DR=" << data
            << std::dec << "...";

  if (result != expected_result) {
    std::cout << "ERROR!!!!" << std::endl
              << "\texpected: " << std::hex << expected_result
              << " result: " << result << std::dec << std::endl
              << std::endl;
    logfile << ": FAILED" << std::endl;

  } else {
    std::cout << "SUCCESS!" << std::endl << std::endl;
    logfile << ": PASSED" << std::endl;
  }
}

/*
 * Bypass looks something like:
 * TDI -> [register] -> TDO
 */
void RRAMJTAG::bypass(uint32_t data) {
  // bypass instruction: {6'hXX, 2'b0X}
  uint8_t bypass_ir = 0;
  set_ir(bypass_ir);

  // move into SHIFT-DR state
  uint64_t result = 0;
  cycle_pins(0, 0, 0);
  cycle_pins(1, 0, 0);
  cycle_pins(0, 0, 0);
  cycle_pins(0, 0, 0);

  // extra iteration to capture the last bit
  for (int i = 0; i < 32 + 1; i++) {
    char loop_tms = i == (32 + 1) - 1;
    char loop_tdi = (data >> i) & 1;
    char loop_tdo;
    cycle_pins(loop_tms, loop_tdi, &loop_tdo);

    result = (((uint64_t)loop_tdo) << (32 - 1)) |
             (result >> 1);  // {tdo,result[dr_len-1:1]}
  }

  std::cout << "Sent: " << std::hex << data << "\tReceived: " << result
            << std::dec << std::endl;

  if (result == data) {
    logfile << ": PASSED" << std::endl;
  } else {
    logfile << ": FAILED" << std::endl;
  }

  // return to IDLE
  cycle_pins(1, 0, 0);
  cycle_pins(0, 0, 0);
}

void RRAMJTAG::wait_for_busy(char jtag_tms) {
  std::cout << "Waiting for busy to go down...";
  char c = 1;
  while (c) {
    cycle_pins(jtag_tms, 0, &c);
  }
  std::cout << " DONE" << std::endl;
}

void RRAMJTAG::check_status() {
  wait_for_busy(0);
  uint64_t result = set_dr(0);
}

void RRAMJTAG::run() {
  if (directivesFile.empty()) {
    std::cout << "No RRAM JTAG directives." << std::endl;
  } else {
    wait_for_busy(1);

    std::ifstream trace_file(directivesFile);

    std::string line;
    while (std::getline(trace_file, line)) {
      if (line.rfind("//", 0) == 0) {  // commment, so just print out
        std::cout << line << std::endl;
      } else if (line.rfind("set_bist_enable", 0) == 0) {
        std::istringstream stream(
            line.substr(std::string("set_bist_enable").length() + 1));
        uint16_t enable;
        stream >> std::setbase(0) >> enable;

        bist_enable = enable;
      } else if (line.rfind("set_rram_jtag_sel", 0) == 0) {
        std::istringstream stream(
            line.substr(std::string("set_rram_jtag_sel").length() + 1));
        uint16_t sel;
        stream >> std::setbase(0) >> sel;

        rram_jtag_sel = sel;
      } else if (line.rfind("rw_jtag_reg", 0) == 0) {
        std::istringstream stream(
            line.substr(std::string("rw_jtag_reg").length() + 1));
        std::cout << line.substr(std::string("rw_jtag_reg").length() + 1)
                  << std::endl;

        uint16_t addr;  // need to use uint16_t, bc if you use uint8_t, it
                        // treats it as a char, not a number
        uint64_t data;
        uint64_t expected_result;

        stream >> std::setbase(0) >> addr >> data >> expected_result;
        rw_jtag_reg(static_cast<uint8_t>(addr), data, expected_result);
      } else if (line.rfind("check_status", 0) == 0) {
        check_status();
      } else if (line.rfind("bypass", 0) == 0) {
        std::istringstream stream(
            line.substr(std::string("bypass").length() + 1));
        std::cout << line.substr(std::string("bypass").length() + 1)
                  << std::endl;
        logfile << "Testing bypass";

        uint32_t data;

        stream >> std::setbase(0) >> data;
        bypass(data);
      } else if (line.rfind("exit", 0) == 0) {
        break;
      }
    }

    finished = true;
  }
}
