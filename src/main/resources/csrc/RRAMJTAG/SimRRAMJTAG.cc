#include <thread>

#include "RRAMJTAG.h"

RRAMJTAG *rram_jtag = NULL;

extern bool reset_triggered;

extern "C" int rram_jtag_tick(unsigned char *jtag_rram_jtag_sel,
                              unsigned char *jtag_bist_enable,
                              unsigned char *jtag_TCK, unsigned char *jtag_TMS,
                              unsigned char *jtag_TDI, unsigned char jtag_TDO,
                              char *jtagFile, char *stdoutFile) {
  // wait until the second reset has been triggered
  if (!reset_triggered) {
    return 0;
  }

  if (!rram_jtag) {
    rram_jtag = new RRAMJTAG(jtagFile, stdoutFile);
    std::thread t(&RRAMJTAG::run, rram_jtag);
    t.detach();
  }

  *jtag_rram_jtag_sel = rram_jtag->rram_jtag_sel;
  *jtag_bist_enable = rram_jtag->bist_enable;
  rram_jtag->transfer_pin_values(jtag_TCK, jtag_TMS, jtag_TDI, jtag_TDO);

  return rram_jtag->finished ? 1 : 0;
}