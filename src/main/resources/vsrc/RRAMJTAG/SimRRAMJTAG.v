import "DPI-C" function int rram_jtag_tick
(
    output byte rram_jtag_sel,
    output bit bist_enable,
    
    output bit jtag_TCK,
    output bit jtag_TMS,
    output bit jtag_TDI,
    input bit jtag_TDO,
    input string rram_jtag_file,
    input string stdout_file
);


module SimRRAMJTAG
#(
    parameter TICK_DELAY = 50
)(
    input clock,
    input reset,

    output [5:0] rram_jtag_sel,
    output bist_enable,

    output tck,
    output tms,
    output tdi,
    input tdo,

    output finished
);

  string rram_jtag_file;
  initial begin
    $value$plusargs ("rram_jtag_file=%s", rram_jtag_file);
  end

      string stdout_file;
    initial begin
        $value$plusargs ("jtag_stdout_file=%s", stdout_file);
    end

  reg [31:0]                    tickCounterReg;
  wire [31:0]                   tickCounterNext;
  assign tickCounterNext = (tickCounterReg == 0) ? TICK_DELAY :  (tickCounterReg - 1);

  byte         __jtag_rram_jtag_sel;
  bit          __jtag_bist_enable;
  bit          __jtag_TCK;
  bit          __jtag_TMS;
  bit          __jtag_TDI;
  bit          __jtag_TDO;
  int          __exit;

      reg __exit_reg;


  assign #0.1 tck = __jtag_TCK;
  assign #0.1 tms = __jtag_TMS;
  assign #0.1 tdi = __jtag_TDI;
  assign #0.1 __jtag_TDO = tdo;
  assign #0.1 rram_jtag_sel = __jtag_rram_jtag_sel;
  assign #0.1 bist_enable = __jtag_bist_enable;
  assign finished = __exit_reg;

  always @(posedge clock) begin
      if(reset) begin
          tickCounterReg <= TICK_DELAY;
                      __exit_reg <= 0;

      end else begin
          tickCounterReg <= tickCounterNext;
          if (tickCounterReg == 0) begin
              __exit = rram_jtag_tick(
                  __jtag_rram_jtag_sel,
                  __jtag_bist_enable,
                  __jtag_TCK,
                  __jtag_TMS,
                  __jtag_TDI,
                  __jtag_TDO,
                  rram_jtag_file,
                  stdout_file
              );
          end

          __exit_reg <= __exit;
      end
  end

endmodule