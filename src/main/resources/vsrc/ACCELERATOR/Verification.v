`define TOP TestDriver.testHarness.chiptop0.system.sbus
`define DNN TestDriver.testHarness.chiptop0.system.sbus.dnn
module Verification (
    input clk
);

    import "DPI-C" context function void load_memory();
    import "DPI-C" function void check_all_outputs();

    // TODO: support different dnn configurations
    import "DPI-C" context function void check_single_output(logic [128-1:0] data, logic [32-1:0] address);
    // import "DPI-C" context function void check_interface(logic [128-1:0] data, string name);
    // import "DPI-C" context function void check_interfaces();

    `ifdef SOC_COSIM
    import "DPI-C" context function void check_matrix_params(logic[32-1:0] data);
    import "DPI-C" context function void check_vector_params(logic[32-1:0] data);

    import "DPI-C" context function void log_input_request(logic [64-1:0] memRequest);
    import "DPI-C" context function void log_weight_request(logic [64-1:0] memRequest);
    import "DPI-C" context function void log_bias_request(logic [64-1:0] memRequest);
    import "DPI-C" context function void log_residual_request(logic [64-1:0] memRequest);

    import "DPI-C" context function void check_input_response(logic [128-1:0] data);
    import "DPI-C" context function void check_weight_response(logic [128-1:0] data);
    import "DPI-C" context function void check_bias_response(logic [128-1:0] data);
    import "DPI-C" context function void check_residual_response(logic [128-1:0] data);

    import "DPI-C" context function void check_output_data(logic [128-1:0] data);
    import "DPI-C" context function void check_output_address(logic [32-1:0] data);
    import "DPI-C" context function void check_layer_memory();
    `endif

    initial begin
        @(negedge TestDriver.reset);
        load_memory();
    end

    always@(posedge `DNN.clock) begin
        if(`DNN.bb.vectorUnit.done_vld) begin
            #5000; // wait for all memory writes to finish
            check_all_outputs();
        end
    end

    always @(posedge `DNN.clock) begin
        if (`DNN.vectorOutputInterface.auto_out_a_ready & `DNN.vectorOutputInterface.auto_out_a_valid & !`DNN.reset) begin
            check_single_output(`DNN.vectorOutputInterface.dataQueue.io_deq_bits, `DNN.vectorOutputInterface.addrQueue.io_deq_bits);
        end
    end

    // always @(posedge `DNN.clock) begin
    //     check_interfaces();
    // end


    // always @(posedge `DNN.clock) begin
    //     if (`DNN.bb.inputDataResponse_rdy & `DNN.bb.inputDataResponse_vld) begin
    //         check_interface(`DNN.bb.inputDataResponse_dat[256-1:0], "inputDataResponse");
    //     end
    // end

    // always @(posedge `DNN.clock) begin
    //     if (`DNN.bb.weightDataResponse_rdy & `DNN.bb.weightDataResponse_vld) begin
    //         check_interface(`DNN.bb.weightDataResponse_dat[256-1:0], "weightDataResponse");
    //     end
    // end

    // always @(posedge `DNN.clock) begin
    //     if (`DNN.bb.vectorFetch0DataResponse_rdy & `DNN.bb.vectorFetch0DataResponse_vld) begin
    //         check_interface(`DNN.bb.vectorFetch0DataResponse_dat[256-1:0], "vectorFetch0DataResponse");
    //     end
    // end

    // always @(posedge `DNN.clock) begin
    //     if (`DNN.bb.vectorFetch1DataResponse_rdy & `DNN.bb.vectorFetch1DataResponse_vld) begin
    //         check_interface(`DNN.bb.vectorFetch1DataResponse_dat[256-1:0], "vectorFetch1DataResponse");
    //     end
    // end

    // always @(posedge `DNN.clock) begin
    //     if (`DNN.bb.vectorFetch2DataResponse_rdy & `DNN.bb.vectorFetch2DataResponse_vld) begin
    //         check_interface(`DNN.bb.vectorFetch2DataResponse_dat[256-1:0], "vectorFetch2DataResponse");
    //     end
    // end

    `ifdef SOC_COSIM
    always @(posedge `DNN.clk) begin
        if(`DNN.matrixUnit_1.serialMatrixParamsIn_vld & `DNN.matrixUnit_1.matrixUnit.serialMatrixParamsIn_rdy) begin
            check_matrix_params(`DNN.matrixUnit_1.serialMatrixParamsIn_dat[32-1:0]);
        end
    end

    always @(posedge `DNN.clk) begin
        if(`DNN.vectorUnit.serialParamsIn_vld & `DNN.vectorUnit.vectorUnit.serialParamsIn_rdy) begin
            check_vector_params(`DNN.vectorUnit.serialParamsIn_dat[32-1:0]);
        end
    end

    always @(posedge `DNN.clk) begin
        if(`DNN.matrixUnit_1.inputAddressRequest_vld & `DNN.matrixUnit_1.matrixUnit.inputAddressRequest_rdy) begin
            log_input_request(`DNN.matrixUnit_1.inputAddressRequest_dat[64-1:0]);
        end
    end

    always @(posedge `DNN.clk) begin
        if(`DNN.matrixUnit_1.inputDataResponse_vld & `DNN.matrixUnit_1.matrixUnit.inputDataResponse_rdy) begin
            check_input_response(`DNN.matrixUnit_1.inputDataResponse_dat[128-1:0]);
        end
    end

    always @(posedge `DNN.clk) begin
        if(`DNN.matrixUnit_1.weightAddressRequest_vld & `DNN.matrixUnit_1.matrixUnit.weightAddressRequest_rdy) begin
            log_weight_request(`DNN.matrixUnit_1.weightAddressRequest_dat[64-1:0]);
        end
    end

    always @(posedge `DNN.clk) begin
        if(`DNN.matrixUnit_1.weightDataResponse_vld & `DNN.matrixUnit_1.matrixUnit.weightDataResponse_rdy) begin
            check_weight_response(`DNN.matrixUnit_1.weightDataResponse_dat[128-1:0]);
        end
    end

    always @(posedge `DNN.clk) begin
        if(`DNN.vectorUnit.vectorFetch1AddressRequest_vld & `DNN.vectorUnit.vectorUnit.vectorFetch1AddressRequest_rdy) begin
            log_residual_request(`DNN.vectorUnit.vectorFetch1AddressRequest_dat[64-1:0]);
        end
    end

    always @(posedge `DNN.clk) begin
        if(`DNN.vectorUnit.vectorFetch1DataResponse_vld & `DNN.vectorUnit.vectorUnit.vectorFetch1DataResponse_rdy) begin
            check_residual_response(`DNN.vectorUnit.vectorFetch1DataResponse_dat[128-1:0]);
        end
    end

    always @(posedge `DNN.clk) begin
        if(`DNN.vectorUnit.vectorFetch2AddressRequest_vld & `DNN.vectorUnit.vectorUnit.vectorFetch2AddressRequest_rdy) begin
            log_bias_request(`DNN.vectorUnit.vectorFetch2AddressRequest_dat[64-1:0]);
        end
    end

    always @(posedge `DNN.clk) begin
        if(`DNN.vectorUnit.vectorFetch2DataResponse_vld & `DNN.vectorUnit.vectorUnit.vectorFetch2DataResponse_rdy) begin
            check_bias_response(`DNN.vectorUnit.vectorFetch2DataResponse_dat[128-1:0]);
        end
    end

    always @(posedge `DNN.clk) begin
        if(`DNN.vectorUnit.finalVectorOutput_vld & `DNN.vectorUnit.vectorUnit.finalVectorOutput_rdy) begin
            check_output_data(`DNN.vectorUnit.finalVectorOutput_dat[128-1:0]);
        end
    end

    always @(posedge `DNN.clk) begin
        if(`DNN.vectorUnit.vectorOutputAddress_vld & `DNN.vectorUnit.vectorUnit.vectorOutputAddress_rdy) begin
            check_output_address(`DNN.vectorUnit.vectorOutputAddress_dat[32-1:0]);
        end
    end
    `endif

endmodule
