/** 
 * Large SRAM consisting of 8 macros, with an overall
 **/
module sram_2p_8_4096_128(
   clk,
   addressA, 
   dataA,
   webA,
   cebA,
   qA,
   addressB,
   dataB,
   webB,
   cebB,
   qB,
   power_down,
   maskA,
   maskB
);

parameter width=128;
parameter num_inst = 8;

localparam memory_depth = 4096;
localparam depth = num_inst*memory_depth;
localparam address_bits = $clog2(depth);

// Port A
input [width-1:0] maskA;
input [address_bits-1:0] addressA;
input [width-1:0] dataA;
input webA;
input cebA;
output [width-1:0] qA;

// Port B 
input [width-1:0] maskB;
input [address_bits-1:0] addressB;
input [width-1:0] dataB;
input webB;
input cebB;
output [width-1:0] qB;
input clk;
input [num_inst-1:0] power_down;
wire [width-1:0] outputA[num_inst-1:0];
wire [width-1:0] outputB[num_inst-1:0];

wire [num_inst-1:0] ceb_arrayA;
wire [num_inst-1:0] ceb_arrayB;

wire [$clog2(num_inst)-1:0] mem_input_selectA;
reg [$clog2(num_inst)-1:0] mem_output_selectA;

wire [$clog2(num_inst)-1:0] mem_input_selectB;
reg [$clog2(num_inst)-1:0] mem_output_selectB;

assign ceb_arrayA = ~(8'b1 << mem_input_selectA);
assign ceb_arrayB = ~(8'b1 << mem_input_selectB);

assign mem_input_selectA = addressA[address_bits-1:address_bits-$clog2(num_inst)];
assign mem_input_selectB = addressB[address_bits-1:address_bits-$clog2(num_inst)];

assign qA = outputA[mem_output_selectA];
assign qB = outputB[mem_output_selectB];

always@(posedge clk) begin
   mem_output_selectA <= mem_input_selectA;
   mem_output_selectB <= mem_input_selectB;
end


for(genvar i = 0; i < num_inst; i=i+1) begin : bank
   wire [width-1:0] q_temp;
   wire [width-1:0] q_temp2;
   for(genvar j = 0; j < 2; j=j+1) begin : macro
      wire [(width/2-1):0] half_q_temp;
      wire [(width/2-1):0] half_q_temp2;
      
      TSDN40LPA4096X64M4M sram_array(
            .AA(addressA[12-1:0]),
            .AB(addressB[12-1:0]),
            .DA(dataA[(width*(j+1)/2-1):(width/2)*j]),
            .DB(dataB[(width*(j+1)/2-1):(width/2)*j]),
            .PD(power_down[i]),
            .CLKA(clk),
            .CLKB(clk),
            .CEBA(power_down[i] | (cebA | ceb_arrayA[i])),
            .CEBB(power_down[i] | (cebB | ceb_arrayB[i])),
            .WEBA(power_down[i] | (webA | ceb_arrayA[i])),
            .WEBB(power_down[i] | (webB | ceb_arrayB[i])),
            .BWEBA(maskA[(width*(j+1)/2-1):(width/2)*j]),
            .BWEBB(maskB[(width*(j+1)/2-1):(width/2)*j]),
            .QA(half_q_temp),
            .QB(half_q_temp2)
      );

      assign q_temp[(width*(j+1)/2-1):(width/2)*j] = half_q_temp;
      assign q_temp2[(width*(j+1)/2-1):(width/2)*j] = half_q_temp2;

   end

   assign outputA[i] = q_temp ;
   assign outputB[i] = q_temp2;

end

`ifndef SYNTHESIS
/* 
 *  Load a full 128-bit word into the sram memory
 */
function sram_load_word;

   input [address_bits-1:0] sram_address; 
   input [width-1:0] data;

   begin
      logic [$clog2(num_inst)-1:0] bank_select;
      logic [12-1:0] word_address;
      {bank_select, word_address} = sram_address;

      // Is there a better way to do this?? You can't do a hierarchical name lookup with a signal
      case(bank_select)
         3'h0: begin
            bank[0].macro[0].sram_array.MX.mem[word_address] = data[64-1:0];
            bank[0].macro[1].sram_array.MX.mem[word_address] = data[128-1:64];
         end
         3'h1: begin
            bank[1].macro[0].sram_array.MX.mem[word_address] = data[64-1:0];
            bank[1].macro[1].sram_array.MX.mem[word_address] = data[128-1:64];
         end
         3'h2: begin
            bank[2].macro[0].sram_array.MX.mem[word_address] = data[64-1:0];
            bank[2].macro[1].sram_array.MX.mem[word_address] = data[128-1:64];
         end
         3'h3: begin
            bank[3].macro[0].sram_array.MX.mem[word_address] = data[64-1:0];
            bank[3].macro[1].sram_array.MX.mem[word_address] = data[128-1:64];
         end
         3'h4: begin
            bank[4].macro[0].sram_array.MX.mem[word_address] = data[64-1:0];
            bank[4].macro[1].sram_array.MX.mem[word_address] = data[128-1:64];
         end
         3'h5: begin
            bank[5].macro[0].sram_array.MX.mem[word_address] = data[64-1:0];
            bank[5].macro[1].sram_array.MX.mem[word_address] = data[128-1:64];
         end
         3'h6: begin
            bank[6].macro[0].sram_array.MX.mem[word_address] = data[64-1:0];
            bank[6].macro[1].sram_array.MX.mem[word_address] = data[128-1:64];
         end
         3'h7: begin
            bank[7].macro[0].sram_array.MX.mem[word_address] = data[64-1:0];
            bank[7].macro[1].sram_array.MX.mem[word_address] = data[128-1:64];
         end
      endcase
   end

endfunction

/* 
 *  Load a byte into the sram memory
 */
function sram_load_byte;

   input [address_bits-1:0] sram_address;
   input [4-1:0] index; 
   input [8-1:0] data;

   begin
      logic [$clog2(num_inst)-1:0] bank_select;
      logic [12-1:0] word_address;
      logic [3-1:0] effective_index;

      {bank_select, word_address} = sram_address;
      effective_index = index % 8;

      // Is there a better way to do this?? You can't do a hierarchical name lookup with a signal
      case(bank_select)
         3'h0: begin
            if(index < 8)
               bank[0].macro[0].sram_array.MX.mem[word_address][effective_index*8 +: 8] = data[8-1:0];
            else 
               bank[0].macro[1].sram_array.MX.mem[word_address][effective_index*8 +: 8] = data[8-1:0];
         end
         3'h1: begin
            if(index < 8)
               bank[1].macro[0].sram_array.MX.mem[word_address][effective_index*8 +: 8] = data[8-1:0];
            else
               bank[1].macro[1].sram_array.MX.mem[word_address][effective_index*8 +: 8] = data[8-1:0];
         end
         3'h2: begin
            if(index < 8)
               bank[2].macro[0].sram_array.MX.mem[word_address][effective_index*8 +: 8] = data[8-1:0];
            else
               bank[2].macro[1].sram_array.MX.mem[word_address][effective_index*8 +: 8] = data[8-1:0];
         end
         3'h3: begin
            if(index < 8)
               bank[3].macro[0].sram_array.MX.mem[word_address][effective_index*8 +: 8] = data[8-1:0];
            else
               bank[3].macro[1].sram_array.MX.mem[word_address][effective_index*8 +: 8] = data[8-1:0];
         end
         3'h4: begin
            if(index < 8)
               bank[4].macro[0].sram_array.MX.mem[word_address][effective_index*8 +: 8] = data[8-1:0];
            else
               bank[4].macro[1].sram_array.MX.mem[word_address][effective_index*8 +: 8] = data[8-1:0];
         end
         3'h5: begin
            if(index < 8)
               bank[5].macro[0].sram_array.MX.mem[word_address][effective_index*8 +: 8] = data[8-1:0];
            else
               bank[5].macro[1].sram_array.MX.mem[word_address][effective_index*8 +: 8] = data[8-1:0];
         end
         3'h6: begin
            if(index < 8)
               bank[6].macro[0].sram_array.MX.mem[word_address][effective_index*8 +: 8] = data[8-1:0];
            else
               bank[6].macro[1].sram_array.MX.mem[word_address][effective_index*8 +: 8] = data[8-1:0];
         end
         3'h7: begin
            if(index < 8)
               bank[7].macro[0].sram_array.MX.mem[word_address][effective_index*8 +: 8] = data[8-1:0];
            else
               bank[7].macro[1].sram_array.MX.mem[word_address][effective_index*8 +: 8] = data[8-1:0];
         end
      endcase
   end

endfunction
`endif


endmodule
