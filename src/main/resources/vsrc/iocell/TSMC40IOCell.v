// See LICENSE for license details

`timescale 1ns/1ps

module TSMC40DigitalInIOCell(
    input pad,
    output i,
    input ie
);
    PDDW08DGZ_G iocell(
        .OEN(1'b1),
        .I(1'b0),
        .REN(1'b0),
        .PAD(pad),
        .C(i)
    );

endmodule

module TSMC40DigitalOutIOCell(
    output pad,
    input o,
    input oe
);

    PDDW08DGZ_G iocell(
        .OEN(1'b0),
        .I(o),
        .REN(1'b0),
        .PAD(pad),
        .C()
    );

endmodule
