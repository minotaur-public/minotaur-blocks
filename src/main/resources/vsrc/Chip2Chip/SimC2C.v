import "DPI-C" context function int c2c_tick(
  input  bit serial_out_valid,
  output bit serial_out_ready,
  input  int serial_out_bits,

  output bit serial_in_valid,
  input  bit serial_in_ready,
  output int serial_in_bits,

  input string binary,
  input string stdout_file,
  input bit load_binary
);

module SimC2C (
    input clock,
    input reset,

    input serial_out_valid,
    output serial_out_ready,
    input [31:0] serial_out_bits,

    output serial_in_valid,
    input serial_in_ready,
    output [31:0] serial_in_bits,

    output exit
);

  string binary;
  initial begin
    $value$plusargs("binary=%s", binary);
  end

  string stdout_file;
  initial begin
    $value$plusargs("stdout_file=%s", stdout_file);
  end

  bit load_binary;
  initial begin
    load_binary = !$test$plusargs("backdoor");
    if (load_binary) begin
      $display("Loading binary through C2C.\n");
    end
  end

  bit __in_valid;
  bit __out_ready;
  int __in_bits;
  int __exit;

  reg __in_valid_reg;
  reg __out_ready_reg;
  reg [31:0] __in_bits_reg;
  reg __exit_reg;

  //assign serial_in_valid  = __in_valid_reg;
  assign serial_in_valid = __in_valid;
  //assign serial_in_bits   = __in_bits_reg;
  assign serial_in_bits = __in_bits;
  //assign serial_out_ready = __out_ready_reg;
  assign serial_out_ready = __out_ready;
  assign exit = __exit_reg;

  // Evaluate the signals on the positive edge
  always @(negedge clock) begin
    if (reset) begin
      __in_valid = 0;
      __out_ready = 0;
      __exit = 0;

      __in_valid_reg <= 0;
      __in_bits_reg <= 0;
      __out_ready_reg <= 0;
      __exit_reg <= 0;
    end else begin
      __exit = c2c_tick(
        serial_out_valid,
        __out_ready,
        serial_out_bits,
        __in_valid,
        serial_in_ready,
        __in_bits,
        binary,
        stdout_file,
        load_binary
      );
      __out_ready_reg <= __out_ready;
      __in_valid_reg <= __in_valid;
      __in_bits_reg <= __in_bits;
      __exit_reg <= __exit[0];
    end
  end

endmodule
