module ClockDivider1024 (output reg clk_out, input clk_in);
   reg clk_div2;
   reg clk_div4;
   reg clk_div8;
   reg clk_div16;
   reg clk_div32;
   reg clk_div64;
   reg clk_div128;
   reg clk_div256;
   reg clk_div512;
   reg clk_div1024;

   initial 
   begin
   clk_out = 1'b0;
   clk_div2 = 1'b0;
   clk_div4 = 1'b0;
   clk_div8 = 1'b0;
   clk_div16 = 1'b0;
   clk_div32 = 1'b0;
   clk_div64 = 1'b0;
   clk_div128 = 1'b0;
   clk_div256 = 1'b0;
   clk_div512 = 1'b0;
   clk_div1024 = 1'b0;
   end

   always @(posedge clk_in) begin
      clk_div2 = ~clk_div2; // Must use =, NOT <=
   end

   always @(posedge clk_div2) begin
      clk_div4 = ~clk_div4; // Must use =, NOT <=
   end

   always @(posedge clk_div4) begin
      clk_div8 = ~clk_div8; // Must use =, NOT <=
   end

   always @(posedge clk_div8) begin
      clk_div16 = ~clk_div16; // Must use =, NOT <=
   end

   always @(posedge clk_div16) begin
      clk_div32 = ~clk_div32; // Must use =, NOT <=
   end

   always @(posedge clk_div32) begin
      clk_div64 = ~clk_div64; // Must use =, NOT <=
   end

   always @(posedge clk_div64) begin
      clk_div128 = ~clk_div128; // Must use =, NOT <=
   end

   always @(posedge clk_div128) begin
      clk_div256 = ~clk_div256; // Must use =, NOT <=
   end

   always @(posedge clk_div256) begin
      clk_div512 = ~clk_div512; // Must use =, NOT <=
   end

   always @(posedge clk_div512) begin
      clk_out = ~clk_out; // Must use =, NOT <=
   end

endmodule // ClockDivider1024