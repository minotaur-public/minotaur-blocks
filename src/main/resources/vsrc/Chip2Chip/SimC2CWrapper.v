module SimC2CWrapper (
        input clock,
    	input reset,
	    output [15:0] get_data_gvalue,
	    //output RDY_get_data_gvalue,
	    output get_data_gpresent,
	    //output RDY_get_data_gpresent,
	    input get_data_gcredit_1,
	    input EN_get_data_gcredit,
	    //output RDY_get_data_gcredit,

        input [15:0] put_data_pvalue_1,
        input EN_put_data_pvalue,
        //input RDY_put_data_pvalue,
        input put_data_ppresent_1,
        input EN_put_data_ppresent,
        //output RDY_put_data_ppresent,
        output put_data_pcredit,
        //output RDY_put_data_pcredit
        output exit, 
        output tx_clockout_gate_en
);
// AG - added the output port tx_clockout_gate_en to pull out the clock gate enable
reg        serial_out_valid;
reg        serial_out_ready;
reg [31:0] serial_out_bits;
reg        serial_in_valid;
reg        serial_in_ready;
reg [31:0] serial_in_bits;
  
wire RDY_get_data_gvalue;
wire RDY_get_data_gpresent;
wire RDY_get_data_gcredit;
wire [15:0] put_data_put;
wire EN_put_data_put;
wire RDY_put_data_put;

wire RDY_put_data_pvalue;
wire RDY_put_data_ppresent;
wire RDY_put_data_pcredit;
wire EN_get_data_get;
wire [15:0] get_data_get;
wire RDY_get_data_get;
// AG - connected tx_clockout_gate_en to pull out the clock gate enable
mkSendFIFOInst #(16) sf1(clock,
		    !reset,
		    get_data_gvalue,
		    RDY_get_data_gvalue,
		    get_data_gpresent,
		    RDY_get_data_gpresent,
		    get_data_gcredit_1,
		    EN_get_data_gcredit,
		    RDY_get_data_gcredit,
		    put_data_put,
		    EN_put_data_put,
		    RDY_put_data_put,
                    tx_clockout_gate_en);

mkReceiveFIFOInst #(16) rf1(clock, 
            !reset,
            put_data_pvalue_1,
            EN_put_data_pvalue,
            RDY_put_data_pvalue,
            put_data_ppresent_1,
            EN_put_data_ppresent,
            RDY_put_data_ppresent,
            put_data_pcredit,
            RDY_put_data_pcredit,
            EN_get_data_get,
            get_data_get,
            RDY_get_data_get);

SimC2C ss1(clock, 
                reset, 
                serial_out_valid, 
                serial_out_ready, 
                serial_out_bits, 
                serial_in_valid, 
                serial_in_ready, 
                serial_in_bits,
                exit);


parameter   cmd = 0, 
            addr0_0 = 1, 
            addr0_1 = 2, 
            addr1_0 = 3, 
            addr1_1 = 4, 
            len0_0 = 5, 
            len0_1 = 6, 
            len1_0 = 7,
            len1_1 = 8,
            data_read_0 = 9,
            data_read_1 = 10,
            data_write_0 = 11,
            data_write_1 = 12;

reg [4:0] state;

reg is_write;

reg [64:0] total_counter;

wire overflow;

wire [15:0] real_len;
assign {overflow, real_len} = 2*(serial_in_bits[15:0]+1);

reg fifo_write_en;
assign EN_put_data_put = fifo_write_en;

reg [15:0] fifo_in;
assign put_data_put = fifo_in;

reg fifo_read_en;
assign EN_get_data_get = fifo_read_en;

reg full;
assign full = !RDY_put_data_put;

reg empty_n;
assign empty_n = RDY_get_data_get;

reg [15:0] fifo_out;
assign fifo_out = get_data_get;

reg [15:0] prev_data;
reg [15:0] next_data;

assign serial_in_ready = !full;
assign fifo_write_en = serial_in_valid & serial_in_ready;
assign fifo_in = serial_in_bits[15:0];

assign serial_out_valid = empty_n;
assign serial_out_bits = {16'b0, fifo_out};
assign fifo_read_en = serial_out_ready & serial_out_valid;

// // combinational logic
// always @(*) begin
//     case (state)
//         cmd: begin
//             serial_in_ready = !full;
//             fifo_write_en = serial_in_valid & !full;
//             fifo_in = serial_in_bits[15:0];

//             serial_out_valid = empty_n;
//             serial_out_bits = {16'b0, fifo_out};
//             fifo_read_en = serial_out_ready & serial_out_valid;
//         end

//         addr0_0: begin
//             serial_in_ready = !full;
//             fifo_write_en = serial_in_valid & !full;
//             fifo_in = serial_in_bits[15:0];

//             serial_out_valid = 0;
//             fifo_read_en = 0;
//         end

//         addr0_1: begin
//             serial_in_ready = 0;
//             fifo_write_en = !full;
//             fifo_in = next_data;

//             serial_out_valid = 0;
//             fifo_read_en = 0;
//         end

//         addr1_0: begin
//             serial_in_ready = !full;
//             fifo_write_en = serial_in_valid & !full;
//             fifo_in = serial_in_bits[15:0];

//             serial_out_valid = 0;
//             fifo_read_en = 0;
//         end

//         addr1_1: begin
//             serial_in_ready = 0;
//             fifo_write_en = !full;
//             fifo_in = next_data;

//             serial_out_valid = 0;
//             fifo_read_en = 0;
//         end

//         len0_0: begin
//             serial_in_ready = !full;
//             fifo_write_en = serial_in_valid & !full;
//             fifo_in = serial_in_bits[15:0]*2+1;

//             serial_out_valid = 0;
//             fifo_read_en = 0;
//         end

//         len0_1: begin
//             serial_in_ready = 0;
//             fifo_write_en = !full;
//             fifo_in = next_data;

//             serial_out_valid = 0;
//             fifo_read_en = 0;
//         end

//         len1_0: begin
//             serial_in_ready = !full;
//             fifo_write_en = serial_in_valid & !full;
//             fifo_in = serial_in_bits[15:0]*2;

//             serial_out_valid = 0;
//             fifo_read_en = 0;
//         end

//         len1_1: begin
//             serial_in_ready = 0;
//             fifo_write_en = !full;
//             fifo_in = next_data;

//             serial_out_valid = 0;
//             fifo_read_en = 0;
//         end

//         data_write_0: begin
//             serial_in_ready = !full;
//             fifo_write_en = serial_in_valid & !full;
//             fifo_in = serial_in_bits[15:0];

//             serial_out_valid = 0;
//             fifo_read_en = 0;
//         end

//         data_write_1: begin
//             serial_in_ready = 0;
//             fifo_write_en = !full;
//             fifo_in = next_data;

//             serial_out_valid = 0;
//             fifo_read_en = 0;
//         end

//         data_read_0: begin
//             serial_in_ready = 0;
//             fifo_write_en = 0;

//             serial_out_valid = 0;
//             fifo_read_en = empty_n;
//         end

//         data_read_1: begin
//             serial_in_ready = 0;
//             fifo_write_en = 0;

//             serial_out_valid = 1;
//             fifo_read_en = empty_n;
//             serial_out_bits = {fifo_out, prev_data}; 
//         end

//     endcase
// end

// // sequential logic
// always @(posedge clock or posedge reset) begin
//     if(reset) begin
//         state <= cmd;
//     end
//     else begin
//         case (state)
//             cmd: begin
//                 total_counter <= 0;
//                 if(serial_in_ready & serial_in_valid) begin
//                     state <= addr0_0;
//                     is_write <= serial_in_bits[0] == 1'b1;
//                 end
//             end

//             addr0_0: begin
//                 if(serial_in_ready & serial_in_valid) begin
//                     state <= addr0_1;
//                     next_data <= serial_in_bits[31:16];
//                 end
//             end

//             addr0_1: begin
//                 if(!full) begin
//                     state <= addr1_0;
//                 end
//             end

//             addr1_0: begin
//                 if(serial_in_ready & serial_in_valid) begin
//                     state <= addr1_1;
//                     next_data <= serial_in_bits[31:16];
//                 end
//             end

//             addr1_1: begin
//                 if(!full) begin
//                     state <= len0_0;
//                 end
//             end
            
//             len0_0: begin
//                 if(serial_in_ready & serial_in_valid) begin
//                     state <= len0_1;
//                     next_data <= 2*serial_in_bits[31:16]+overflow;
//                     total_counter <= 2*(serial_in_bits+1);
//                 end
//             end

//             len0_1: begin
//                 if(!full) begin
//                     state <= len1_0;
//                 end
//             end

//             len1_0: begin
//                 if(serial_in_ready & serial_in_valid) begin
//                     state <= len1_1;
//                     next_data <= 2*serial_in_bits[31:16]+overflow;
//                     total_counter <= total_counter + 2*(serial_in_bits << 32);
//                 end
//             end

//             len1_1: begin
//                 if(!full) begin
//                     if(is_write) begin
//                         state <= data_write_0;
//                     end
//                     else begin
//                         state <= data_read_0;
//                     end
//                 end
//             end

//             data_write_0: begin
                
//                 if(!full & serial_in_valid) begin
//                     total_counter <= total_counter - 1;
//                     state <= data_write_1;
//                     next_data <= serial_in_bits[31:16];
//                 end
                
//             end

//             data_write_1: begin
//                 if(!full) begin
//                     if(total_counter == 65'b1) begin
//                         state <= cmd;
//                     end
//                     else begin
//                         total_counter <= total_counter - 1;    
//                         state <= data_write_0;
//                     end
//                 end
//             end

//             data_read_0: begin
//                 if(empty_n) begin
//                     total_counter <= total_counter - 1;
//                     state <= data_read_1;
//                     prev_data <= fifo_out;
//                 end
//             end

//             data_read_1: begin
//                 if(empty_n && serial_out_ready) begin
//                     if(total_counter == 65'b1) begin
//                         state <= cmd;
//                     end
//                     else begin
//                         total_counter <= total_counter - 1;
//                         state <= data_read_0;
//                     end
//                 end            
//             end

//         endcase
//     end
// end

endmodule



