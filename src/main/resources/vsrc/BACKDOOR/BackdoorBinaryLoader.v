module BackdoorBinaryLoader (
    input clock,
    input reset
);

  parameter NUM_CHIPS = 1;

  import "DPI-C" context function void load_binary(
    input string binary,
    input int num_chips
  );

  string binary;
  initial begin
    $value$plusargs("binary=%s", binary);
  end

  initial begin
    @(negedge reset);
    @(negedge reset);
    if ($test$plusargs("backdoor")) begin
      $display("Loading binary through backdoor.");
      load_binary(binary, NUM_CHIPS);
    end
  end

endmodule
