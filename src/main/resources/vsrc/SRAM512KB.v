`define DEPTH 4096

`define SRAM_4096_64(ROW, COL) \
reg  ren_``ROW``_``COL``; \
reg  wen_``ROW``_``COL``; \
reg  [63:0] din_``ROW``_``COL``; \
wire [63:0] q_``ROW``_``COL``; \
 \
ip224uhdlp1p11rf_4096x64m4b2c1s1_t0r0p0d0a1m1h mem_``ROW``_``COL`` ( \
   .clk(clk), \
   .ren(ren_``ROW``_``COL``), \
   .wen(wen_``ROW``_``COL``), \
   .adr(adr_``ROW``), \
   .mc(mc), \
   .mcen(mcen), \
   .clkbyp(clkbyp), \
   .din(din_``ROW``_``COL``), \
   .wbeb(wbeb_``COL``), \
   .wa(wa), \
   .wpulse(wpulse), \
   .wpulseen(wpulseen), \
   .fwen(fwen), \
   .q(q_``ROW``_``COL``) \
);

module SRAM512KB (
    input clk,         //Input Clock
    input rst,
    input ren,         //Read Enable
    input wen,         //Write Enable
    input [31:0] adr,  //Input Address
    input [127:0] din,  //Input Write Data 
    input [127:0] wbeb,
    output reg [127:0]    q
);

wire clkbyp = 1'b0;
wire mcen = 1'b0;
wire [2:0] mc = 3'b000;
wire wpulseen = 1'b0;
wire [1:0] wpulse = 2'b00;
wire [1:0] wa = 2'b00;
wire fwen = rst;

wire [63:0] wbeb_0;
wire [63:0] wbeb_1;
assign wbeb_0 = wbeb[63:0];
assign wbeb_1 = wbeb[127:64];

reg [11:0] adr_0;
reg [11:0] adr_1;
reg [11:0] adr_2;
reg [11:0] adr_3;
reg [11:0] adr_4;
reg [11:0] adr_5;
reg [11:0] adr_6;
reg [11:0] adr_7;

reg [31:0] pre_adr;

`SRAM_4096_64(0, 0)
`SRAM_4096_64(0, 1)
`SRAM_4096_64(1, 0)
`SRAM_4096_64(1, 1)
`SRAM_4096_64(2, 0)
`SRAM_4096_64(2, 1)
`SRAM_4096_64(3, 0)
`SRAM_4096_64(3, 1)
`SRAM_4096_64(4, 0)
`SRAM_4096_64(4, 1)
`SRAM_4096_64(5, 0)
`SRAM_4096_64(5, 1)
`SRAM_4096_64(6, 0)
`SRAM_4096_64(6, 1)
`SRAM_4096_64(7, 0)
`SRAM_4096_64(7, 1)

`define DEFAULT_SIGNALS(ROW, COL) \
    ren_``ROW``_``COL`` = 1'b0; \
    wen_``ROW``_``COL`` = 1'b0; \
    adr_``ROW`` = {12{1'b0}}; \
    din_``ROW``_``COL`` = {64{1'b0}};

`define SET_SIGNALS(ROW) \
    if (adr >= ROW * `DEPTH && adr < (ROW + 1) * `DEPTH) begin \
        ren_``ROW``_0 = ren; \
        ren_``ROW``_1 = ren; \
        wen_``ROW``_0 = wen; \
        wen_``ROW``_1 = wen; \
        adr_``ROW`` = {(adr - ROW * `DEPTH)}[11:0]; \
        din_``ROW``_0 = din[63:0]; \
        din_``ROW``_1 = din[127:64]; \
    end \
    if (pre_adr >= ROW * `DEPTH && pre_adr < (ROW + 1) * `DEPTH) begin \
        q[63:0] = q_``ROW``_0; \
        q[127:64] = q_``ROW``_1; \
    end

always @(posedge clk) begin
    if (rst) begin
        pre_adr <= {32{1'b0}};
    end
    else if (ren) begin
        pre_adr <= adr;
    end
end

always @(*) begin
    q = {128{1'b0}};

`DEFAULT_SIGNALS(0, 0)
`DEFAULT_SIGNALS(0, 1)
`DEFAULT_SIGNALS(1, 0)
`DEFAULT_SIGNALS(1, 1)
`DEFAULT_SIGNALS(2, 0)
`DEFAULT_SIGNALS(2, 1)
`DEFAULT_SIGNALS(3, 0)
`DEFAULT_SIGNALS(3, 1)
`DEFAULT_SIGNALS(4, 0)
`DEFAULT_SIGNALS(4, 1)
`DEFAULT_SIGNALS(5, 0)
`DEFAULT_SIGNALS(5, 1)
`DEFAULT_SIGNALS(6, 0)
`DEFAULT_SIGNALS(6, 1)
`DEFAULT_SIGNALS(7, 0)
`DEFAULT_SIGNALS(7, 1)

`SET_SIGNALS(0)
`SET_SIGNALS(1)
`SET_SIGNALS(2)
`SET_SIGNALS(3)
`SET_SIGNALS(4)
`SET_SIGNALS(5)
`SET_SIGNALS(6)
`SET_SIGNALS(7)
end

endmodule
