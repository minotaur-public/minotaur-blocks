/*
 * This is a generic SRAM model for an Intel16 4096x64 SRAM.
*/
module ip224uhdlp1p11rf_4096x64m4b2c1s1_t0r0p0d0a1m1h (
    input clk,
    input ren,
    input wen,
    input [11:0] adr,
    input [2:0] mc,
    input mcen,
    input clkbyp,
    input [63:0] din,
    input [63:0] wbeb,
    input [1:0] wa,
    input [1:0] wpulse,
    input wpulseen,
    input fwen,
    output reg [63:0] q
);

  reg [63:0] mem[4095:0];

  always @(posedge clk) begin
    if (ren) begin
      q <= mem[adr];
    end
    if (wen) begin
      mem[adr] <= (mem[adr] & wbeb) | (din & ~wbeb);
    end
  end

endmodule
