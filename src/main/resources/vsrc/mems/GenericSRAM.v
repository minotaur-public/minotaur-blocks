//
// Accelerator SRAM
//
module TS1N40LPB1024X128M4FWBA (
  input CLK,
  input PD,
  input [1:0] RTSEL,
  input [1:0] WTSEL,
  input BIST,
  input CEB,
  input CEBM,
  input WEB,
  input WEBM,
  input [9:0] A,
  input [9:0] AM,
  input [127:0] D,
  input [127:0] DM,
  output reg [127:0] Q
);

  reg [127:0] mem [1023:0];

  // Zero initailize the memory
  initial begin
    integer init_addr;
    for (init_addr = 0; init_addr < 1024; init_addr++) begin
      mem[init_addr] = 128'h0;
    end
  end

  always @(posedge CLK) begin
    if (!CEB) begin
      if (!WEB) begin
        mem[A] <= D;
      end
      else begin
        Q <= mem[A];
      end
    end
  end

endmodule

//
// Main system SRAM
//
module TS1N40LPB16384X64M8SW (
  input CLK,
  input PD,
  input CEB,
  input WEB,
  input [13:0] A,
  input [63:0] D,
  input [63:0] BWEB,
  input [1:0] RTSEL,
  input [1:0] WTSEL,
  output reg [63:0] Q
);

`ifdef EMU

  // This implementation is needed to use the MARG API
  // on Cadence servers

  reg [63:0] MEMORY [16384-1:0];

  // Zero initailize the memory
  initial begin
    integer init_addr;
    for (init_addr = 0; init_addr < 16384; init_addr++) begin
      MEMORY[init_addr] = 64'h0;
    end
  end

  always @(posedge CLK) begin
    if (!CEB) begin
      if (!WEB) begin
        MEMORY[A] <= (MEMORY[A] & BWEB) | (D & ~BWEB);
      end
      else begin
        Q <= MEMORY[A];
      end
    end
  end

`else

  // The following is the main implementation used in VCS and elsewhere
  // that has the expected hierarchical layout

  reg [63:0] MEMORY [2048-1:0][8-1:0];

  // Zero initailize the memory
  initial begin
    integer init_addr1;
    integer init_addr2;
    for (init_addr1 = 0; init_addr1 < 2048; init_addr1++) begin
      for (init_addr2 = 0; init_addr2 < 8; init_addr2++) begin
        MEMORY[init_addr1][init_addr2] = 64'h0;
      end
    end
  end

  wire [3-1:0] dim0;
  wire [11-1:0] dim1;

  assign {dim1, dim0} = A;

  always @(posedge CLK) begin
    if (!CEB) begin
      if (!WEB) begin
        MEMORY[dim1][dim0] <= (MEMORY[dim1][dim0] & BWEB) | (D & ~BWEB);
      end
      else begin
        Q <= MEMORY[dim1][dim0];
      end
    end
  end

`endif

endmodule

//
// Accelerator SRAM
//
module TSDN40LPA1024X67M4M (
  input CLKA,
  input CLKB,
  input PD,
  input CEBA,
  input CEBB,
  input WEBA,
  input WEBB,
  input [9:0] AA,
  input [9:0] AB,
  input [66:0] DA,
  input [66:0] DB,
  input [66:0] BWEBA,
  input [66:0] BWEBB,
  output reg [66:0] QA,
  output reg [66:0] QB
);

  reg [66:0] mem [1023:0];

  // Zero initailize the memory
  initial begin
    integer init_addr;
    for (init_addr = 0; init_addr < 1024; init_addr++) begin
      mem[init_addr] = 67'h0;
    end
  end

  always @(posedge CLKA) begin
    if (!CEBA) begin
      if (!WEBA) begin
        mem[AA] <= DA;
      end
      else begin
        QA <= mem[AA];
      end
    end
  end

  always @(posedge CLKB) begin
    if (!CEBB) begin
      if (!WEBB) begin
        mem[AB] <= DB;
      end
      else begin
        QB <= mem[AB];
      end
    end
  end

endmodule
