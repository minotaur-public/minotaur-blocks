//
// Components of RRAMBank
//
module RRN40ULP64KX44M64I4R4_DW33_SVT_C180807 (
    input clk,
    input rst_n,

    input [3:0] ip_user_cmd_i,
    input [9:0] ip_user_xadr_i,
    input [5:0] ip_user_yadr_i,
    input [31:0] ip_user_udin_i,

    output reg [44:0] trc_dout_o,
    output trc_busy_o
);

    reg [45-1:0] main_mem [65536-1:0];

    initial begin
        integer init_addr;
        for (init_addr = 0; init_addr < 65536; init_addr++) begin
            main_mem[init_addr] = 45'h0;
        end
    end

    reg [44:0] delay1;
    reg [44:0] delay2;
    reg [44:0] delay3;

    wire [15:0] addr = {ip_user_xadr_i, ip_user_yadr_i};
    
    always @(posedge clk) begin
        // RRAM_CONSTANTS.READ
        if (ip_user_cmd_i == 4'h1) begin
            delay1 <= main_mem[addr];
        end
        // RRAM_CONSTANTS.LOAD
        else if (ip_user_cmd_i == 4'h2) begin
            main_mem[addr] <= {13'b0, ip_user_udin_i};
        end

        delay2 <= delay1;
        delay3 <= delay2;
        trc_dout_o <= delay3;
    end

    reg [2:0] wait_count;
    always @(posedge clk) begin
        if (!rst_n) // Active low
            wait_count <= 3'h0;
        else if (ip_user_cmd_i == 4'h1)
            wait_count <= 3'h4;
        else if (wait_count > 3'h1)
            wait_count <= wait_count - 3'h1;
        else
            wait_count <= 3'h0;
    end

    assign trc_busy_o = wait_count > 3'h1;

endmodule

module RRAM_IP (
    input clk,
    input rst_n,

    input [3:0] ip_user_cmd_i,
    input [9:0] ip_user_xadr_i,
    input [5:0] ip_user_yadr_i,
    input [31:0] ip_user_udin_i,

    output [44:0] trc_dout_o,
    output trc_busy_o
);

    RRN40ULP64KX44M64I4R4_DW33_SVT_C180807 u_rram (
        .clk(clk),
        .rst_n(rst_n),
        .ip_user_cmd_i(ip_user_cmd_i),
        .ip_user_xadr_i(ip_user_xadr_i),
        .ip_user_yadr_i(ip_user_yadr_i),
        .ip_user_udin_i(ip_user_udin_i),
        .trc_dout_o(trc_dout_o),
        .trc_busy_o(trc_busy_o)
    );

endmodule

module trbcx1r64_40ulp64kx44m64i4r4dw33svt_wrapper (
    input clk,
    input rst_n,
    input inv_clk,
    input inv_tck,
    input rram_rst_i,

    // BIST
    input bist_enable,
    input bist_rst_n,
    input jtag_trst_n,
    input tck,
    input tms,
    input tdi,
    input tdo,

    // scan
    input scan_test,
    input scan_en,
    input scan_rst_n,

    input [3:0] ip_user_cmd_i,
    input ip_user_info_i,
    input ip_user_lven_i,
    input [9:0] ip_user_xadr_i,
    input [5:0] ip_user_yadr_i,
    input [31:0] ip_user_udin_i,
    input [12:0] ip_user_tdin_i,
    input ip_user_nap_i,
    input [63:0] ip_user_trc_data_i,
    output reg [44:0] trc_dout_o,
    output [63:0] trc_regif_dout_o,
    output trc_busy_o,
    output trc_err_o,

    input async_access_i,
    input async_ifren_i,
    input async_se_i,
    input [9:0] async_xadr_i,
    input [5:0] async_yadr_i,
    output async_rram_rdone_o,

    input POC_H
);

    RRAM_IP rram_ip (
        .clk(clk),
        .rst_n(rst_n),
        .ip_user_cmd_i(ip_user_cmd_i),
        .ip_user_xadr_i(ip_user_xadr_i),
        .ip_user_yadr_i(ip_user_yadr_i),
        .ip_user_udin_i(ip_user_udin_i),
        .trc_dout_o(trc_dout_o),
        .trc_busy_o(trc_busy_o)
    );

    /*
    assign tdo = 0;
    assign trc_regif_dout_o = 0;
    assign trc_err_o = 0;
    assign async_rram_rdone_o = 0;
    */

endmodule
