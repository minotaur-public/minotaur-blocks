module data_arrays_0_ext(
  input  [9:0]  RW0_addr,
  input         RW0_clk,
  input  [63:0] RW0_wdata,
  output [63:0] RW0_rdata,
  input         RW0_en,
  input         RW0_wmode,
  input  [7:0]  RW0_wmask
);

wire [63:0] bitmask;

genvar i;
generate
for(i = 0; i <= 63; i=i+1) begin: bitmask_gen
  assign bitmask[i] = RW0_wmask[i/8];
end
endgenerate

TS1N40LPB1024X64M4FW mem (
  .PD(1'b0),
  .CLK(RW0_clk),
  .CEB(~RW0_en),
  .A(RW0_addr),
  .D(RW0_wdata),
  .WEB(~RW0_wmode),
  .BWEB(~bitmask),
  .Q(RW0_rdata),
  .RTSEL(2'b01),
  .WTSEL(2'b01)
);

initial begin
  reg [10:0] w;
  reg [8-1:0] row;
  reg [2-1:0] col;

  for(w = 0; w < 2**10; w = w + 1 ) begin
    {row, col} = w;
    mem.MEMORY[row][col] = {64{1'b0}};
  end
end

endmodule

module tag_array_ext(
  input  [5:0]  RW0_addr,
  input         RW0_clk,
  input  [20:0] RW0_wdata,
  output [20:0] RW0_rdata,
  input         RW0_en,
  input         RW0_wmode,
  input         RW0_wmask
);

TS1N40LPB128X21M4FW mem (
  .PD(1'b0),
  .CLK(RW0_clk),
  .CEB(~RW0_en),
  .A({1'b0,RW0_addr}),
  .D(RW0_wdata),
  .WEB(~RW0_wmode),
  .BWEB({21{~RW0_wmask}}),
  .Q(RW0_rdata),
  .RTSEL(2'b01),
  .WTSEL(2'b01)
);

initial begin
  reg [7:0] w;
  reg [5-1:0] row;
  reg [2-1:0] col;

  for(w = 0; w < 2**7; w = w + 1 ) begin
    {row, col} = w;
    mem.MEMORY[row][col] = {21{1'b0}};
  end
end

endmodule

module data_arrays_0_0_ext(
  input  [7:0]  RW0_addr,
  input         RW0_clk,
  input  [31:0] RW0_wdata,
  output [31:0] RW0_rdata,
  input         RW0_en,
  input         RW0_wmode,
  input         RW0_wmask
);

TS1N40LPB256X32M4FW mem (
  .PD(1'b0),
  .CLK(RW0_clk),
  .CEB(~RW0_en),
  .A(RW0_addr),
  .D(RW0_wdata),
  .WEB(~RW0_wmode),
  .BWEB({32{~RW0_wmask}}),
  .Q(RW0_rdata),
  .RTSEL(2'b01),
  .WTSEL(2'b01)
);

initial begin
  reg [8:0] w;
  reg [6-1:0] row;
  reg [2-1:0] col;

  for(w = 0; w < 2**8; w = w + 1 ) begin
    {row, col} = w;
    mem.MEMORY[row][col] = {32{1'b0}};
  end
end

endmodule

module split_data_arrays_0_ext(
  input  [9:0] RW0_addr,
  input        RW0_clk,
  input  [7:0] RW0_wdata,
  output [7:0] RW0_rdata,
  input        RW0_en,
  input        RW0_wmode,
  input        RW0_wmask
);

TS1N40LPB1024X8M4FW mem (
  .PD(1'b0),
  .CLK(RW0_clk),
  .A(RW0_addr),
  .D(RW0_wdata),
  .CEB(~RW0_en),
  .WEB(~RW0_wmode),
  .BWEB({8{~RW0_wmask}}),
  .Q(RW0_rdata),
  .RTSEL(2'b01),
  .WTSEL(2'b01)
);

initial begin
  reg [10:0] w;
  reg [8-1:0] row;
  reg [2-1:0] col;

  for(w = 0; w < 2**10; w = w + 1 ) begin
    {row, col} = w;
    mem.MEMORY[row][col] = {8{1'b0}};
  end
end

endmodule

module split_tag_array_ext(
  input  [5:0]  RW0_addr,
  input         RW0_clk,
  input  [20:0] RW0_wdata,
  output [20:0] RW0_rdata,
  input         RW0_en,
  input         RW0_wmode,
  input         RW0_wmask
);

TS1N40LPB128X21M4FW mem (
  .PD(1'b0),
  .CLK(RW0_clk),
  .CEB(~RW0_en),
  .A({1'b0,RW0_addr}),
  .D(RW0_wdata),
  .WEB(~RW0_wmode),
  .BWEB({21{~RW0_wmask}}),
  .Q(RW0_rdata),
  .RTSEL(2'b01),
  .WTSEL(2'b01)
);

initial begin
  reg [7:0] w;
  reg [5-1:0] row;
  reg [2-1:0] col;

  for(w = 0; w < 2**7; w = w + 1 ) begin
    {row, col} = w;
    mem.MEMORY[row][col] = {21{1'b0}};
  end
end

endmodule

module split_data_arrays_0_0_ext(
  input  [7:0]  RW0_addr,
  input         RW0_clk,
  input  [31:0] RW0_wdata,
  output [31:0] RW0_rdata,
  input         RW0_en,
  input         RW0_wmode,
  input         RW0_wmask
);

TS1N40LPB256X32M4FW mem (
  .PD(1'b0),
  .CLK(RW0_clk),
  .CEB(~RW0_en),
  .A(RW0_addr),
  .D(RW0_wdata),
  .WEB(~RW0_wmode),
  .BWEB({32{~RW0_wmask}}),
  .Q(RW0_rdata),
  .RTSEL(2'b01),
  .WTSEL(2'b01)
);

initial begin
  reg [8:0] w;
  reg [6-1:0] row;
  reg [2-1:0] col;

  for(w = 0; w < 2**8; w = w + 1 ) begin
    {row, col} = w;
    mem.MEMORY[row][col] = {32{1'b0}};
  end
end

endmodule

module l2_tlb_ram_ext(
  input  [9:0]  RW0_addr,
  input         RW0_clk,
  input  [37:0] RW0_wdata,
  output [37:0] RW0_rdata,
  input         RW0_en,
  input         RW0_wmode
);
  
endmodule
