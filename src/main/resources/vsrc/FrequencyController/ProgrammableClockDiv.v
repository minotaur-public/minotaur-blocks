/*
 * USEFUL LINKS
 *
 * https://reference.digilentinc.com/learn/programmable-logic/tutorials/counter-and-clock-divider/start
 * http://www.referencedesigner.com/tutorials/verilogexamples/verilog_ex_07.php
 * https://asicdigitaldesign.wordpress.com/2009/08/26/real-world-examples-5-clock-divider-by-5/
 * https://pdfs.semanticscholar.org/cfdb/5e2c97971ef602c2bedf5d497a7c7bbb0a78.pdf
 * https://www.mikrocontroller.net/attachment/177197/xl33_30.pdf
 *
 */

module ProgrammableClockDiv (
	                   input       fclk, // Free running clock
		               input       reset_n, // Power-On reset
		               input [3:0] clksel, // Programmable clock divider selection
		               output      clkdiv); // Programmable clock divider divided clock

   wire                            poreset_sync_n;
   wire                            reset_n;

   wire                            tercnt_clk_divpg;
   wire [3:0]                      count_clk_divpg;

   wire                            en1_clk_divpg;
   wire                            en2_clk_divpg;
   wire                            clk1_clk_divpg;
   wire                            clk2_clk_divpg;
   reg                             q1_clk_divpg;
   reg                             q2_clk_divpg;

   wire                            fclk1_divpg;
   wire [3:0]                      count1_clk_divpg;
   reg [3:0]                       countto_clk_divpg;
   reg                             fclk2_divpg;
   reg [3:0]                       count2_clk_divpg;

   /*
    * Programmable clock divider [2-256]
    */
   // LFSR polynom: X^5 + X^2 + 1
   // LFSR sequence: 0-8-C-E-7-B-D-6-3-4-A-5-2-1
   //
   DW03_lfsr_dcnto #(.width(4)) i_CLKDIVPG_COUNTER(.data(4'h0),
						                           .count_to(countto_clk_divpg),
						                           .load(~tercnt_clk_divpg),
						                           .cen(1'b1),
						                           .clk(fclk),
						                           .reset(~reset_n),
						                           .count(count_clk_divpg[3:0]),
						                           .tercnt(tercnt_clk_divpg)
						                           );

   // LFSR terminal count generation
   //   always @(count_clk_divpg or clksel or fclk) begin
   always @(clksel or fclk) begin
      case (clksel)
	    4'h2: begin
	       countto_clk_divpg = 4'h8;
	       count2_clk_divpg =  4'h8;
	       fclk2_divpg = fclk;
	    end
	    4'h3: begin
	       countto_clk_divpg = 4'hc;
	       count2_clk_divpg =  4'hc;
	       fclk2_divpg = ~fclk;
	    end
	    4'h4: begin
	       countto_clk_divpg = 4'he;
	       count2_clk_divpg =  4'hc;
	       fclk2_divpg = fclk;
	    end
	    4'h5: begin
	       countto_clk_divpg = 4'h7;
	       count2_clk_divpg =  4'he;
	       fclk2_divpg = ~fclk;
	    end
	    4'h6: begin
	       countto_clk_divpg = 4'hb;
	       count2_clk_divpg =  4'he;
	       fclk2_divpg = fclk;
	    end
	    4'h8: begin
	       countto_clk_divpg = 4'h6;
	       count2_clk_divpg =  4'h7;
	       fclk2_divpg = fclk;
	    end
	    default: begin
	       countto_clk_divpg = 4'h1;
	       count2_clk_divpg =  4'h1;
	       fclk2_divpg = ~fclk;
	    end
      endcase // case (clksel)
   end

   assign fclk1_divpg = fclk;
   assign count1_clk_divpg = 4'h0;

   // T flip-flop enable generation
   assign en1_clk_divpg = count_clk_divpg == count1_clk_divpg ? 1'b1 : 1'b0;
   assign en2_clk_divpg = count_clk_divpg == count2_clk_divpg ? 1'b1 : 1'b0;

   EICG_wrapper U_cm0p_acg_clkdiv_tff1(
                                   .out(clk1_clk_divpg),
                                   .in(fclk1_divpg),
                                   .en(en1_clk_divpg),
                                   .test_en(1'b0));

   EICG_wrapper U_cm0p_acg_clkdiv_tff2(
                                   .out(clk2_clk_divpg),
                                   .in(fclk2_divpg),
                                   .en(en2_clk_divpg),
                                   .test_en(1'b0));

   // T flip-flop
   always @ ( posedge clk1_clk_divpg or negedge reset_n)
     if (~reset_n) begin
        q1_clk_divpg <= 1'b0;
     end else begin
        q1_clk_divpg <=  ~q1_clk_divpg;
     end

   always @ ( posedge clk2_clk_divpg or negedge reset_n)
     if (~reset_n) begin
        q2_clk_divpg <= 1'b0;
     end else begin
        q2_clk_divpg <=  ~q2_clk_divpg;
     end

   // // Clock generation
   assign clkdiv = q1_clk_divpg ^ q2_clk_divpg;
endmodule // cm0p_ik_clkgen
