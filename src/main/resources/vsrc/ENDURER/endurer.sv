	parameter WIDTH=128;                             // bits in a single bank
    parameter PARTS = WIDTH/32;                      // macros per bank
	parameter NUM_BANKS = 2;                         // number of banks (rows of macros)
													  // 8 banks
	parameter BITS_ADDRX = $clog2(NUM_BANKS) + 10;   // row address (10) bits + bit select
	parameter BITS_ADDRY = 6;
    parameter BITS_ADDR = BITS_ADDRX + BITS_ADDRY;
	
	parameter LD = 64;
    // TODO use BITS_ADDR/BITS_ADDRX after testing!!
    parameter MR = 1<<BITS_ADDR;
    parameter MRX = 1<<BITS_ADDRX;

	typedef logic [BITS_ADDR-1:0] e_address;
	typedef logic [BITS_ADDRX-1:0] e_address_x;             
	typedef logic [BITS_ADDRY-1:0] e_address_y;              // column address (6) bits
	typedef logic [WIDTH-1:0] e_data;               // data in width
	typedef logic [3:0] e_cmd;                        // command (load, write, idle, etc.)

    typedef enum {
        NOT_BUSY,
        BUSY
    } e_e_busy;

    typedef enum {
        IDLE,
        READ,
        LOAD,
        WRITE,
        CLR_LOAD,
        WRT_CONFIG,
        READ_CONFIG,
        RECALL,
        RST_REG,
        RUN_OS,
        CMD_REMAP = 10, // ENDURER Specifc!
        CACHE_WRITE = 11, // ENDURER Specific - cache not load
        ABORT = 13,
        SUSPEND = 14,
        RESUME = 15
    } e_e_cmd;

    typedef enum {
        DEFAULT,
        REMAPPING
    } e_mode;


module LFSR(
    input clock,
    input reset,
    input logic updateOffset,
    input logic forcePRNG,         // for MMIO to update the prng
    input e_address_x forcedPRNG_x,    // to rewrite prng state to desired value
    output e_address_x prng_x,       // current prng state, for shutdown
    output e_address nextPrng
);
    /*
     * Performs a random-shift remapping.
     * Updates relative_shift so that virtual-physical translation can work.
     */
    
    e_address_x next_prng_x;

    logic bit_; 

    assign bit_ = (prng_x[7] ^ prng_x[9]);
    assign next_prng_x = (prng_x >> 1) | (bit_ << (BITS_ADDR-1));
    assign nextPrng = {next_prng_x, 6'b0};

    always_ff @(posedge clock)
    begin
        if (reset) begin
            prng_x = 'h280; // TODO handle shutdown!
        end else if (forcePRNG) begin   // forcing takes precedence
            prng_x  = forcedPRNG_x;
        end else if (updateOffset) begin
            prng_x = next_prng_x;
        end
    end

endmodule

parameter width=128;
parameter num_inst = 1;

parameter memory_depth = 64;
parameter depth = num_inst*memory_depth;
parameter address_bits = $clog2(depth);


module sram_2p_64_128(
// Port A
input [width-1:0] maskA,
input [address_bits-1:0] addressA,
input [width-1:0] dataA,
input webA,
input cebA,
output [width-1:0] qA,

// Port B 
input [width-1:0] maskB,
input [address_bits-1:0] addressB,
input [width-1:0] dataB,
input webB,
input cebB,
output [width-1:0] qB,
input clk,
input power_down
);


genvar j;
generate
    logic [width-1:0] q_temp;
    logic [width-1:0] q_temp2;
     for(j = 0; j < 2; j=j+1) begin
      logic [(width/2-1):0] half_q_temp;
      logic [(width/2-1):0] half_q_temp2;
      TSDN40LPA64X64M4F sram_array(
          .AA(addressA[6-1:0]),
          .AB(addressB[6-1:0]),
          .DA(dataA[(width*(j+1)/2-1):(width/2)*j]),
          .DB(dataB[(width*(j+1)/2-1):(width/2)*j]),
          .PD(power_down),
          .CLKA(clk),
          .CLKB(clk),
          .CEBA(cebA),
          .CEBB(cebB),
          .WEBA(webA),
          .WEBB(webB),
          .BWEBA(maskA[(width*(j+1)/2-1):(width/2)*j]),
          .BWEBB(maskB[(width*(j+1)/2-1):(width/2)*j]),
          .QA(half_q_temp),
          .QB(half_q_temp2),
          .AWT(1'b0)
       );

       assign q_temp[(width*(j+1)/2-1):(width/2)*j] = half_q_temp;
       assign q_temp2[(width*(j+1)/2-1):(width/2)*j] = half_q_temp2;


     end

    assign qA = q_temp;
    assign qB = q_temp2;

endgenerate
endmodule

// TODO check why ours is 64x64m4f and not m4m

typedef enum { E_IDLE, INITIAL, OUTER_LOOP, FINAL_SAVE, FINAL } e_outer_state;
typedef enum { E_IDLE3, IN1_PREQUEL, IN1, IN1_STALL, IN1_STALL1, IN1_STALL2, IN1_STALL3, IN2, IN2_PRESTALL, IN2_STALL, IN_STALL, IN3, IN4, IN4_STALL, IN4_STALL2, IN5, IN6} e_inner_state;

module Endurer(
        input clock, 
        input reset,
        input e_data  writeDataIn,
        output e_data writeDataOut,
        input e_data  readDataIn,
        output e_data readDataOut,
        input e_address  addrIn,
        output e_address_x addrOutX,
		output e_address_y addrOutY,
        input e_cmd cmdIn,
        output logic [NUM_BANKS*PARTS*4-1:0] cmdOut, // e_cmd - > 4, 4 macros to a bank
 
        // for MMIO test/shutdown
        input logic useForcedRandomOffset,
        input e_address_x forcedRandomOffset_x,
        output e_address_x randomOffset_x,
        input logic useForcedPRNG,         
        input e_address_x forcedPRNG_x,  
        output e_address_x currentPRNG_x,
  
        input logic memoryBusyIn,
        output logic memoryBusyOut,

        // for shifting around what part endurer translates on
        // in case we try smaller remaps or shifted remaps in software
        input [31:0] memorySize,
        input [31:0] memoryOffset,
        output e_address addrTranslated,
        output logic endAddrOutOfBounds,
        
        // sram buffer
        input logic bufferValid);

    e_address randomOffset, nextRandomOffset, shift; //, addrTranslated;
	logic updateOffset, mode;

    e_address_x remapAddrOutX;
    e_address_y remapAddrOutY;
    e_data remapWriteDataOut;
    e_cmd remapCmdOut;

    logic bufferValid_last;
	always_ff @(posedge clock)
    begin
		if (reset)
		begin
            bufferValid_last = 0;
		end 
        else
        begin
            bufferValid_last = bufferValid;
        end
    end 

    logic remap_buffer_web, buffer_web;
    e_data buffer_output, bufferDataIn;
    e_address_y buffer_addr;
    assign buffer_addr = (mode==REMAPPING) ? remapAddrOutY : addrOutY;
    assign buffer_web = (mode==REMAPPING) ? remap_buffer_web : (cmdIn==CACHE_WRITE);

    RemapFsm remap(clock, reset,
                    readDataIn, mode,
                    memoryBusyIn,
                    remapWriteDataOut,
                    remapAddrOutX,
					remapAddrOutY,
                    remapCmdOut,
					shift, updateOffset,
                    useForcedPRNG, forcedPRNG_x, currentPRNG_x,
                    buffer_output,
                    remap_buffer_web);

	// Address translation 
	always_comb
	begin
		translate_addr (addrIn, randomOffset, addrTranslated, memorySize, memoryOffset, endAddrOutOfBounds);
	
		if (mode!=REMAPPING)
		begin
			addrOutX = addrTranslated[BITS_ADDR-1:BITS_ADDRY];
			addrOutY = addrTranslated[BITS_ADDRY-1:0];
		end
		else
		begin
			addrOutX = remapAddrOutX;
			addrOutY = remapAddrOutY;
		end
	end
	
	// Command control
        generate 
        if (NUM_BANKS!=1) for (genvar i=0; i<NUM_BANKS; i++) begin
            for (genvar j=0; j<PARTS; j++)  begin
                  // rram interleave
                 assign cmdOut[(PARTS*i+j)*4 + PARTS -1: (PARTS*i+j)*4] = (addrOutY[0]==i) ? ((mode==REMAPPING) ? remapCmdOut : (((cmdIn==READ && bufferValid) || (cmdIn==CACHE_WRITE)) ? IDLE : cmdIn)) : ((cmdIn==WRITE  || (mode==REMAPPING && remapCmdOut==WRITE)) ? WRITE : IDLE);
            end
        end // TODO update else if/ else like the if above
        else if (PARTS!=1) for (genvar j=0; j<4; j++)  begin
            assign cmdOut[(j)*PARTS + PARTS -1: (j)*PARTS] = (mode==REMAPPING) ? remapCmdOut : (((cmdIn==READ && bufferValid) || (cmdIn==CACHE_WRITE)) ? IDLE : cmdIn);
        end
        else assign cmdOut = (mode==REMAPPING) ? remapCmdOut : (((cmdIn==READ && bufferValid) || (cmdIn==CACHE_WRITE)) ? IDLE : cmdIn);
        endgenerate
	
	assign nextRandomOffset = (randomOffset + shift) % MR;
    assign writeDataOut = (mode==REMAPPING) ? remapWriteDataOut : writeDataIn;
    assign readDataOut = (bufferValid_last) ? buffer_output : readDataIn; 
    assign bufferDataIn = (mode==REMAPPING) ? readDataIn : writeDataIn;
    assign memoryBusyOut = (memoryBusyIn) || (mode==REMAPPING);

	always_ff @(posedge clock)
    begin
		if (reset)
		begin
		    randomOffset = 0;
            mode = DEFAULT;
		end 
        else if (mode==REMAPPING && updateOffset==1) // TODO check which is final state of remap fsm 
		begin
			randomOffset = nextRandomOffset;
            mode = DEFAULT;
            $display("Finished remamp! at time %d",$time);
		end
        else if (cmdIn==CMD_REMAP)
        begin
            mode = REMAPPING;
            $display("Started remap! at time %d",$time);
        end
        else if (useForcedRandomOffset) 
        begin
            randomOffset = {forcedRandomOffset_x, 6'b0};
            $display("Forced random offset to value %d",forcedRandomOffset_x<<6);
        end
	end
    assign randomOffset_x = randomOffset >> 6;
	
    generate
        if (WIDTH==128) 
        sram_2p_64_128 remap_buffer (
           .clk(clock),
           .addressA(buffer_addr), 
           .dataA(bufferDataIn),
           .webA(~buffer_web),
           .cebA(1'b0),
           .cebB(1'b1),
           .webB(1'b1),
           .addressB(6'b0),
           .qA(buffer_output),
           .power_down(1'b0),
           .maskA(128'b0)
        );
        else sram_2p_64_32 remap_buffer (
           .clk(clock),
           .addressA(buffer_addr), 
           .dataA(bufferDataIn),
           .webA(~buffer_web),
           .cebA(1'b0),
           .cebB(1'b1),
           .webB(1'b1),
           .addressB(6'b0),
           .qA(buffer_output),
           .power_down(1'b0),
           .maskA(32'b0)
        );
    endgenerate


    function void translate_addr (input e_address addrIn, 
                                    input e_address offset, 
                                    output e_address addrOut,
                                    input [31:0] memory_size, input [31:0] memory_start,
                                    output logic addr_out_of_bounds);
        if ((addrIn<memory_start) || (addrIn>=memory_size+memory_start)) 
        begin
            addrOut = addrIn;  // Send out memory directly
            addr_out_of_bounds = 1'b1;
        end
        else 
        begin
            addrOut = ((addrIn - offset) % memory_size) + memory_start;
            addr_out_of_bounds = 1'b0;
        end
	endfunction
endmodule

module RemapFsm(
    input clock,
    input reset,
    input e_data readDataIn,
    input mode,
    input memoryBusyIn,
    output e_data writeDataOut,
    output e_address_x addrOutX,
	output e_address_y addrOutY,
    output e_cmd cmdOut,
    output e_address shiftLast, 
	output logic updateOffset,
    input logic forcePRNG,         
    input e_address_x forcedPRNG_x,  
    output e_address_x currentPRNG_x,
    // outputs to sram
    input e_data buffer_output,
    output logic buffer_web
);
    /*
     * Performs a random-shift remapping.
     * Updates relative_shift so that virtual-physical translation can work.
     */
    
    e_address_x curr_addr, next_addr; 
    e_address prng, shift, shift_raw;
    e_address_x dist_x, next_dist_x, shift_x;
    e_address num_shift_loops, shifts_per_loop;
	
	logic[4-1:0] trailing_zeros;

    e_data buffer_output_slow;

    LFSR lfsr (
        .clock(clock), 
        .reset(reset),
        .updateOffset(updateOffset),
        .nextPrng(prng),
        .forcePRNG(forcePRNG),
        .forcedPRNG_x(forcedPRNG_x),
        .prng_x(currentPRNG_x));
 
    assign shift_x = shift[BITS_ADDR -1: BITS_ADDRY];
	
	always_comb 
	begin 
        // NOTE the count trailing zeros assumes 64 word endurer right now!! TODO
		count_trailing_zeros(shift_x, trailing_zeros);
        num_shift_loops = 1 << trailing_zeros;
        shifts_per_loop = MRX >> trailing_zeros;
	end

    // NOTE this is for the 64 word solution, need to support more bits if word-endurer
	// TODO fix hard coding?! find more efficient solution 
	function void count_trailing_zeros (e_address_x addrIn, output [4-1:0] count);
		if (addrIn[0]==1) count = 0;
		else if (addrIn[1:0]==2) count = 1;
		else if (addrIn[2:0]==4) count = 2;
		else if (addrIn[3:0]==8) count = 3;
		else if (addrIn[4:0]==16) count = 4;
		else if (addrIn[5:0]==32) count = 5;
		else if (addrIn[6:0]==64) count = 6;
		else if (addrIn[7:0]==128) count = 7;
		else if (addrIn[8:0]==256) count = 8;
		else if (addrIn[9:0]==512) count = 9;
		else if (addrIn[10:0]==1024) count = 10;
		//else if (addrIn[11:0]==2048) count = 11;
		//else if (addrIn[12:0]==4096) count = 12;
		//else if (addrIn[13:0]==8192) count = 13;
		// TODO check max?!
	endfunction

    assign shift_raw = (prng % (MR-1)) & (MR-LD); // Map at line level
    // >MR case is for when we remap smaller part of memory only. Simulation only
    // TODO switch to second line
    assign shift = (shift_raw>MR || (shift_raw==0)) ? (1<<BITS_ADDRY) : shift_raw;
 
    // Protect against the 0 shift case; always shift at least once
    //assign shift = (shift_raw == 0) ? (1<<BITS_ADDRY) : shift_raw;

    always_ff @(posedge clock)
    begin
        if (reset)
        begin
            shiftLast = 0;
            buffer_output_slow = 0;
        end else 
        begin
            shiftLast = shift;
            buffer_output_slow = buffer_output;
        end
    end

    e_address_y counter, next_counter;
    e_address_x middle_counter, next_middle_counter;
    e_address_x outer_counter, next_outer_counter;

  //  assign remap_buffer_addr = counter;

    e_outer_state state, next_state;
    e_inner_state inner_state, next_state3;
    always_ff @(posedge clock)
    begin
        if (reset)
        begin
            state = E_IDLE;
            inner_state = E_IDLE3;
            counter = 0;
            middle_counter = 0;
            outer_counter = 0;
            dist_x = 0;
        end else if (mode==REMAPPING && state==E_IDLE)
        begin
            state = next_state;
            inner_state = E_IDLE3;
            counter = 0;
            middle_counter = 0;
            outer_counter = 0;
            dist_x = 0;
        end else
        begin
            state = next_state;
            inner_state = next_state3;
            counter = next_counter;
            middle_counter = next_middle_counter;
            outer_counter = next_outer_counter;
            dist_x = next_dist_x;
        end
    end

    /* announce at transitions */ 
    always_ff @(negedge clock)
    begin
        if (mode==REMAPPING) begin
            if (state==INITIAL && inner_state==E_IDLE3) $display("Num shift loops %d, shifts per loop %d, prng %d", num_shift_loops, shifts_per_loop, prng);
        end
    end

    /* announce at transitions  
    always_ff @(negedge clock)
    begin
        if (mode==REMAPPING) begin
            if (inner_state==IN4 && next_state3==IN5) $display("Moving data %x to CurrAddr %x from New Addr %x", readDataIn, {curr_addr,counter},{next_addr,counter}); 
            else if (inner_state==IN2) $display("Read data %x for CurrAddr %x for load from New Addr %x", readDataIn, {curr_addr,counter},{next_addr,counter}); 
        end
    end*/ 

    // Outer State FSM
    always_comb
    begin
        next_state = E_IDLE;
        case (state)
            E_IDLE: 
                if (mode==REMAPPING) next_state = INITIAL;
                else next_state = E_IDLE;
            INITIAL:
                // Count cycles till loop parameters calculated
                if (inner_state==IN5) next_state = OUTER_LOOP;
                else next_state = INITIAL;
            OUTER_LOOP:
                if (middle_counter>=(shifts_per_loop-1) && inner_state==IN6) next_state = FINAL_SAVE;
                else next_state = OUTER_LOOP;
            FINAL_SAVE:
                if (inner_state==IN6) begin  // Wait for the buffer outputs to save
                    if (next_outer_counter<num_shift_loops) next_state = INITIAL;
                    else next_state = FINAL;
                end else next_state = FINAL_SAVE;
            FINAL:
                  next_state = E_IDLE;
        endcase
    end

    // Calculate outputs
    assign updateOffset = (state==FINAL);

    always_comb
    begin
        cmdOut = IDLE;
        case (state)
            INITIAL:
                case(inner_state)
                    IN1: cmdOut = IDLE;
                    IN1_STALL: cmdOut = READ; // hold stable?
                    IN2: cmdOut = IDLE; //LOAD;
                    IN2_STALL: cmdOut = IDLE;
                    IN_STALL: cmdOut = IDLE;
                    IN3: cmdOut = IDLE; //WRITE;
                    IN4: cmdOut = IDLE;
                    IN5: cmdOut = IDLE;
                endcase
            OUTER_LOOP:
                case(inner_state)
                    IN1: cmdOut = IDLE;
                    IN1_STALL: cmdOut = READ; // hold stable?
                    // IN1_STALL1, 2, 3
                    IN2: cmdOut = IDLE; //LOAD;
                    IN2_STALL: cmdOut = LOAD;
                    IN_STALL: cmdOut = IDLE; //LOAD;
                    IN3: cmdOut = IDLE; //WRITE;
                    IN4: cmdOut = WRITE;
                    IN5: cmdOut = IDLE;
                endcase
            FINAL_SAVE:
                case(inner_state)
                    IN1: cmdOut = IDLE;
                    IN1_STALL: cmdOut = IDLE;
                    IN2: cmdOut = IDLE; //LOAD;
                    IN2_STALL: cmdOut = LOAD;
                    IN_STALL: cmdOut = IDLE; //LOAD;
                    IN3: cmdOut = IDLE; //WRITE;
                    IN4: cmdOut = WRITE;
                    IN5: cmdOut = IDLE;
                    IN6: cmdOut = IDLE;
                endcase

        endcase
    end

    // For last part of loop, save data from buffer to rram
    assign writeDataOut = (state==FINAL_SAVE) ? buffer_output_slow : readDataIn;

    // count from 0 to LD (64)
    // Increment after load
    assign next_counter = (inner_state==IN2_STALL) ? counter + 1 : counter;

    // 0 to <num_shift_loops - 1
    // Reset at beginning of every loop
    // Increment when WRITE signal is first sent out
    assign next_middle_counter = (state==INITIAL) ? 0 : ((state==OUTER_LOOP && inner_state==IN3) ? middle_counter + 1 : middle_counter);


    // 0 to num_shift_loops
    // Increment after last value from buffer has been written in FINAL_SAVE state
    assign next_outer_counter = (state==FINAL_SAVE && inner_state==IN6) ? outer_counter + 1 : outer_counter;
    
    // dist += shift every time we increment middle_counter
    assign next_dist_x = (state==INITIAL) ? 0 : ((state==OUTER_LOOP && inner_state==IN4 && next_counter==0) ? (dist_x + shift_x) : dist_x);

    assign addrOutY = counter;
    assign addrOutX = (state==INITIAL) ? outer_counter : ((inner_state==IN1_STALL) ? next_addr : curr_addr);  
    // Read from next_addr for saving in curr_addr

    assign curr_addr = (outer_counter + dist_x) % MRX;
    assign next_addr = (curr_addr + shift_x) % MRX;
    
    assign buffer_web = (state==INITIAL && inner_state==IN2_STALL);

    always_comb 
    begin
        // DEFAULT: idle, no writes to SRAM
        next_state3 = E_IDLE3; 
        case(state) 
            INITIAL:
                case(inner_state)
                    E_IDLE3: 
                        if (memoryBusyIn==1'b0) next_state3 = IN1_PREQUEL;
                        else next_state3 = E_IDLE3;
                    IN1_PREQUEL: next_state3 = IN1;
                    IN1: next_state3 = IN1_STALL; 
                    IN1_STALL: next_state3 = IN1_STALL1;
                    IN1_STALL1: next_state3 = IN1_STALL2;
                    IN1_STALL2: next_state3 = IN1_STALL3;
                    IN1_STALL3: next_state3 = IN2;
                    IN2: next_state3 = IN2_PRESTALL;
                    IN2_PRESTALL: next_state3 = IN2_STALL;
                    IN2_STALL: 
                        if (next_counter>0) next_state3 = IN1;
                        else next_state3 = IN4;
                    IN4: next_state3 = IN5;
                    IN5: next_state3 = E_IDLE3;
                endcase
            OUTER_LOOP:
                case(inner_state)
                    E_IDLE3: if (memoryBusyIn==1'b0) next_state3 = IN1_PREQUEL;
                            else next_state3 = E_IDLE3;
                    IN1_PREQUEL: next_state3 = IN1;
                    IN1: next_state3 = IN1_STALL; 
                    IN1_STALL: next_state3 = IN1_STALL1;
                    IN1_STALL1: next_state3 = IN1_STALL2;
                    IN1_STALL2: next_state3 = IN1_STALL3;
                    IN1_STALL3: next_state3 = IN2;
                    IN2: next_state3 = IN2_STALL;
                    IN2_STALL: if (next_counter>0) next_state3 = IN1;
                            else next_state3 = IN_STALL;
                    IN_STALL:
                        if(memoryBusyIn==1'b1) next_state3 = IN_STALL;
                        else next_state3 = IN3;
                    IN3: next_state3 = IN4; 
                    IN4: next_state3 = IN4_STALL;
                    IN4_STALL: next_state3 = IN4_STALL2;
                    IN4_STALL2: next_state3 = IN5;
                    IN5: if (memoryBusyIn==1'b1) next_state3 = IN5;
                        else next_state3 = IN6;
                    IN6: next_state3 = E_IDLE3;
                endcase
            FINAL_SAVE:
                case(inner_state)
                    E_IDLE3: if (memoryBusyIn==1'b0) next_state3 = IN1_STALL;
                            else next_state3 = E_IDLE3;
                    IN1_STALL: next_state3 = IN2;
                    IN2: next_state3 = IN2_STALL;
                    IN2_STALL: if (next_counter>0) next_state3 = IN1_STALL;
                            else next_state3 = IN_STALL;
                    IN_STALL:
                        if(memoryBusyIn==1'b1) next_state3 = IN_STALL;
                        else next_state3 = IN3;
                    IN3: next_state3 = IN4; 
                    IN4: next_state3 = IN4_STALL;
                    IN4_STALL:  next_state3 = IN4_STALL2;
                    IN4_STALL2: next_state3 = IN5;
                    IN5: if (memoryBusyIn==1'b1) next_state3 = IN5;
                        else next_state3 = IN6;
                    IN6: next_state3 = E_IDLE3;
                endcase
       endcase
    end

endmodule

