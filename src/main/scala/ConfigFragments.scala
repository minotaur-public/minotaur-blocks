package minotaur.config

import chisel3._
import chisel3.util._

import org.chipsalliance.cde.config.{Field, Parameters, Config}
import freechips.rocketchip.subsystem._
import freechips.rocketchip.diplomacy.{LazyModule, ValName}
import freechips.rocketchip.devices.tilelink.{BootROMLocated}
import freechips.rocketchip.devices.debug.{Debug}
import freechips.rocketchip.tile._
import freechips.rocketchip.rocket.{RocketCoreParams, MulDivParams, DCacheParams, ICacheParams}

import minotaur._


// -----------------------
// Custom Minotaur Config Fragments
// -----------------------

// Name:     WithWideBus
//
// Description:    ConfigFragment for making front bus and system bus
//                 16 bytes (128 bits) wide.

class WithWideBus
extends Config((site, here, up) => {
  case SystemBusKey => up(SystemBusKey).copy(beatBytes = 16)
  case TilesLocated(InSubsystem) => up(TilesLocated(InSubsystem)) map {
    case tp: RocketTileAttachParams => tp.copy(tileParams = tp.tileParams.copy(
      dcache = tp.tileParams.dcache.map(_.copy(rowBits = 128)),
      icache = tp.tileParams.icache.map(_.copy(rowBits = 128))
    ))
  }
})

// by default, do not build the generic version
case object GenericKey extends Field[Boolean](false)
class WithGenericVersion extends Config((site, here, up) => { case GenericKey => true 
  case DNNKey => up(DNNKey) map{
   case a: DNNConfig => a.copy(technology = "generic") 
  }
})

