package minotaur

import minotaur.config._

import chisel3._
import chisel3.util._
import chisel3.experimental.{IntParam, BaseModule}
import freechips.rocketchip.amba.axi4._
import freechips.rocketchip.subsystem._
import org.chipsalliance.cde.config.{Parameters, Field, Config}
import freechips.rocketchip.diplomacy._
import freechips.rocketchip.regmapper.{HasRegMap, RegField}
import freechips.rocketchip.tilelink._
import freechips.rocketchip.util._
import freechips.rocketchip.interrupts.{IntSourceNode, IntSourcePortSimple}

import testchipip.serdes._

case class DNNConfig(
  mmioAddress: BigInt = A.MMIOMINOTAUR,
  sbusWidth: Int = 128,
  datatype: String = "E4M3",
  systolicArrayDimension: Int = 32,
  clockPeriod: Int = 5,
  technology: String = "generic"
)

case object DNNKey extends Field[Option[DNNConfig]](None)

class DNNIO(dataWidth: Int) extends Bundle {
  val clk = Input(Clock())
  val rstn = Input(Bool())

  val serialMatrixParamsIn_vld = Input(Bool())
  val serialMatrixParamsIn_rdy = Output(Bool())
  val serialMatrixParamsIn_dat = Input(UInt(32.W))

  val inputAddressRequest_vld = Output(Bool())
  val inputAddressRequest_rdy = Input(Bool())
  val inputAddressRequest_dat = Output(UInt(64.W))
  val inputDataResponse_vld = Input(Bool())
  val inputDataResponse_rdy = Output(Bool())
  val inputDataResponse_dat = Input(UInt(dataWidth.W))

  val weightAddressRequest_vld = Output(Bool())
  val weightAddressRequest_rdy = Input(Bool())
  val weightAddressRequest_dat = Output(UInt(64.W))
  val weightDataResponse_vld = Input(Bool())
  val weightDataResponse_rdy = Output(Bool())
  val weightDataResponse_dat = Input(UInt(dataWidth.W))

  val biasAddressRequest_vld = Output(Bool())
  val biasAddressRequest_rdy = Input(Bool())
  val biasAddressRequest_dat = Output(UInt(64.W))
  val biasDataResponse_vld = Input(Bool())
  val biasDataResponse_rdy = Output(Bool())
  val biasDataResponse_dat = Input(UInt(dataWidth.W))

  val matrixUnitStartSignal_vld = Output(Bool())
  val matrixUnitStartSignal_rdy = Input(Bool())
  val matrixUnitDoneSignal_vld = Output(Bool())
  val matrixUnitDoneSignal_rdy = Input(Bool())

  val serialVectorParamsIn_vld = Input(Bool());
  val serialVectorParamsIn_rdy = Output(Bool());
  val serialVectorParamsIn_dat = Input(UInt(32.W));

  val vectorFetch0AddressRequest_vld = Output(Bool())
  val vectorFetch0AddressRequest_rdy = Input(Bool())
  val vectorFetch0AddressRequest_dat = Output(UInt(64.W))
  val vectorFetch0DataResponse_vld = Input(Bool())
  val vectorFetch0DataResponse_rdy = Output(Bool())
  val vectorFetch0DataResponse_dat = Input(UInt(dataWidth.W))

  val vectorFetch1AddressRequest_vld = Output(Bool())
  val vectorFetch1AddressRequest_rdy = Input(Bool())
  val vectorFetch1AddressRequest_dat = Output(UInt(64.W))
  val vectorFetch1DataResponse_vld = Input(Bool())
  val vectorFetch1DataResponse_rdy = Output(Bool())
  val vectorFetch1DataResponse_dat = Input(UInt(dataWidth.W))

  val vectorFetch2AddressRequest_vld = Output(Bool())
  val vectorFetch2AddressRequest_rdy = Input(Bool())
  val vectorFetch2AddressRequest_dat = Output(UInt(64.W))
  val vectorFetch2DataResponse_vld = Input(Bool())
  val vectorFetch2DataResponse_rdy = Output(Bool())
  val vectorFetch2DataResponse_dat = Input(UInt(dataWidth.W))

  val vectorOutput_vld = Output(Bool())
  val vectorOutput_rdy = Input(Bool())
  val vectorOutput_dat = Output(UInt(dataWidth.W))
  val vectorOutputAddress_vld = Output(Bool())
  val vectorOutputAddress_rdy = Input(Bool())
  val vectorOutputAddress_dat = Output(UInt(32.W))

  val vectorUnitStartSignal_vld = Output(Bool())
  val vectorUnitStartSignal_rdy = Input(Bool())
  val vectorUnitDoneSignal_vld = Output(Bool())
  val vectorUnitDoneSignal_rdy = Input(Bool())
}

trait HasDNNIO extends BaseModule {
  val dataWidth: Int
  val io = IO(new DNNIO(dataWidth))
}

class Accelerator (
    datatype: String,
    systolicArrayDimension: Int,
    clockPeriod: Int,
    technology: String,
    val dataWidth: Int
) extends BlackBox
    with HasBlackBoxResource
    with HasDNNIO {
      addResource("/vsrc/ACCELERATOR/accelerator.v")
}

class TLDNN(address: BigInt, sbusWidth: Int, pbusBeatBytes: Int, sbusBeatBytes: Int)(implicit p: Parameters) extends LazyModule {
  val matrixUnitDevice = new SimpleDevice("dnn-MatrixUnit", Seq("stanford,dnn-MatrixUnit"))
  val vectorUnitDevice = new SimpleDevice("dnn-VectorUnit", Seq("stanford,dnn-VectorUnit"))
  val device = new SimpleDevice("dnn", Seq("stanford,dnn"))

  val config = p(DNNKey).get
  val datatypeWidth = config.datatype match {
    case "E4M3" => 8
    case "P8" => 8
    case "BF16" => 16
    case "FP32" => 32
    case _ => throw new Exception("Unsupported datatype")
  }
  val memoryInterfaceWidth = datatypeWidth * config.systolicArrayDimension

  println(s"Memory interface width: $memoryInterfaceWidth")
  println(s"Beat bytes: $sbusBeatBytes")

  val matrixUnitNode = TLRegisterNode(
    address = Seq(AddressSet(address, 0xff)),
    device = matrixUnitDevice,
    beatBytes = pbusBeatBytes,
    concurrency = 1)

  val vectorUnitNode = TLRegisterNode(
    address = Seq(AddressSet(address + 0x100, 0xff)),
    device = vectorUnitDevice,
    beatBytes = pbusBeatBytes,
    concurrency = 1)

  val node = TLRegisterNode(
    address = Seq(AddressSet(address + 0x200, 0xff)),
    device = device,
    beatBytes = pbusBeatBytes,
    concurrency = 1)

  val inputInterfaceNode = TLIdentityNode()
  val weightInterfaceNode = TLIdentityNode()
  val biasInterfaceNode = TLIdentityNode()

  val vectorFetch0InterfaceNode = TLIdentityNode()
  val vectorFetch1InterfaceNode = TLIdentityNode()
  val vectorFetch2InterfaceNode = TLIdentityNode()
  val vectorOutputInterfaceNode = TLIdentityNode()

  val inputsXbar = LazyModule(new TLXbar)
  inputsXbar.node := inputInterfaceNode 
  inputsXbar.node := vectorFetch0InterfaceNode 

  val weightsXbar = LazyModule(new TLXbar)
  weightsXbar.node := weightInterfaceNode 
  weightsXbar.node := vectorFetch1InterfaceNode 
  weightsXbar.node := biasInterfaceNode 

  // MatrixUnit
  val inputInterface = LazyModule(new TLBurstReadInterface("dnn-input", 32, memoryInterfaceWidth, memoryInterfaceWidth, 16)(p))
  val weightInterface = LazyModule(new TLBurstReadInterface("dnn-weight", 32, memoryInterfaceWidth, memoryInterfaceWidth, 16)(p))
  val biasInterface = LazyModule(new TLBurstReadInterface("dnn-bias", 32, memoryInterfaceWidth, memoryInterfaceWidth, 16)(p))

  // VectorUnit
  val vectorFetch0Interface = LazyModule(new TLBurstReadInterface("dnn-vectorFetch0", 32, memoryInterfaceWidth, memoryInterfaceWidth, 16)(p))
  val vectorFetch1Interface = LazyModule(new TLBurstReadInterface("dnn-vectorFetch1", 32, memoryInterfaceWidth, memoryInterfaceWidth, 16)(p))
  val vectorFetch2Interface = LazyModule(new TLBurstReadInterface("dnn-vectorFetch2", 32, memoryInterfaceWidth, memoryInterfaceWidth, 16)(p))
  val vectorOutputInterface = LazyModule(new TLFullWriteInterface("dnn-vectorOutput", 32, memoryInterfaceWidth, memoryInterfaceWidth, 16, 16)(p))

  if(memoryInterfaceWidth != sbusBeatBytes*8){
    // need to add a TLWidthWidget
    inputInterfaceNode := TLFIFOFixer(TLFIFOFixer.all) := TLWidthWidget(memoryInterfaceWidth/8) := inputInterface.node
    weightInterfaceNode := TLFIFOFixer(TLFIFOFixer.all) := TLWidthWidget(memoryInterfaceWidth/8) := weightInterface.node
    biasInterfaceNode := TLFIFOFixer(TLFIFOFixer.all) := TLWidthWidget(memoryInterfaceWidth/8) := biasInterface.node

    vectorFetch0InterfaceNode := TLFIFOFixer(TLFIFOFixer.all) := TLWidthWidget(memoryInterfaceWidth/8) := vectorFetch0Interface.node
    vectorFetch1InterfaceNode := TLFIFOFixer(TLFIFOFixer.all) := TLWidthWidget(memoryInterfaceWidth/8) := vectorFetch1Interface.node
    vectorFetch2InterfaceNode := TLFIFOFixer(TLFIFOFixer.all) := TLWidthWidget(memoryInterfaceWidth/8) := vectorFetch2Interface.node
    vectorOutputInterfaceNode := TLFIFOFixer(TLFIFOFixer.all) := TLWidthWidget(memoryInterfaceWidth/8) := vectorOutputInterface.node
  } else {
    inputInterfaceNode := inputInterface.node
    weightInterfaceNode := weightInterface.node
    biasInterfaceNode := biasInterface.node

    vectorFetch0InterfaceNode := vectorFetch0Interface.node
    vectorFetch1InterfaceNode := vectorFetch1Interface.node
    vectorFetch2InterfaceNode := vectorFetch2Interface.node
    vectorOutputInterfaceNode := vectorOutputInterface.node
  }

  val intNode = IntSourceNode(IntSourcePortSimple())

  lazy val module = new DNNModuleImp(this)
}

class DNNModuleImp(outer: TLDNN) extends LazyModuleImp(outer) {
  val config = p(DNNKey).get
  val bb = Module(new Accelerator(config.datatype, config.systolicArrayDimension, config.clockPeriod, config.technology, outer.memoryInterfaceWidth))

  bb.io.clk := clock
  bb.io.rstn := ~(reset.asBool)

  val (interrupts, _) = outer.intNode.out(0)
  interrupts(0) := bb.io.vectorUnitDoneSignal_vld

  // MatrixUnit
  val inputReqQueue = Module(new Queue(UInt(64.W), 8))
  val weightReqQueue = Module(new Queue(UInt(64.W), 8))
  val biasReqQueue = Module(new Queue(UInt(64.W), 8))

  val inputRespQueue = Module(new Queue(UInt(outer.memoryInterfaceWidth.W), 8))
  val weightRespQueue = Module(new Queue(UInt(outer.memoryInterfaceWidth.W), 8))
  val biasRespQueue = Module(new Queue(UInt(outer.memoryInterfaceWidth.W), 8))
  val serialMatrixParamsQueue = Module(new Queue(UInt(32.W), 1))

  val inputBaseAddress = Reg(UInt(32.W))
  val weightBaseAddress = Reg(UInt(32.W))
  val biasBaseAddress = Reg(UInt(32.W))

  // Params
  bb.io.serialMatrixParamsIn_vld := serialMatrixParamsQueue.io.deq.valid
  bb.io.serialMatrixParamsIn_dat := serialMatrixParamsQueue.io.deq.bits
  serialMatrixParamsQueue.io.deq.ready := bb.io.serialMatrixParamsIn_rdy

  // Inputs
  outer.inputInterface.module.io.baseAddress := inputBaseAddress
  // Request
  inputReqQueue.io.enq.valid := bb.io.inputAddressRequest_vld
  inputReqQueue.io.enq.bits := bb.io.inputAddressRequest_dat
  bb.io.inputAddressRequest_rdy := inputReqQueue.io.enq.ready
  outer.inputInterface.module.io.memRequest <> inputReqQueue.io.deq
  // Response
  inputRespQueue.io.enq <> outer.inputInterface.module.io.dataResponse
  bb.io.inputDataResponse_dat := inputRespQueue.io.deq.bits
  bb.io.inputDataResponse_vld := inputRespQueue.io.deq.valid
  inputRespQueue.io.deq.ready := bb.io.inputDataResponse_rdy

  // Weights
  outer.weightInterface.module.io.baseAddress := weightBaseAddress
  // Request
  weightReqQueue.io.enq.valid := bb.io.weightAddressRequest_vld
  weightReqQueue.io.enq.bits := bb.io.weightAddressRequest_dat
  bb.io.weightAddressRequest_rdy := weightReqQueue.io.enq.ready
  outer.weightInterface.module.io.memRequest <> weightReqQueue.io.deq
  // Response
  weightRespQueue.io.enq <> outer.weightInterface.module.io.dataResponse
  bb.io.weightDataResponse_dat := weightRespQueue.io.deq.bits
  bb.io.weightDataResponse_vld := weightRespQueue.io.deq.valid
  weightRespQueue.io.deq.ready := bb.io.weightDataResponse_rdy

  // Bias 
  outer.biasInterface.module.io.baseAddress := biasBaseAddress
  // Request
  biasReqQueue.io.enq.valid := bb.io.biasAddressRequest_vld
  biasReqQueue.io.enq.bits := bb.io.biasAddressRequest_dat
  bb.io.biasAddressRequest_rdy := biasReqQueue.io.enq.ready
  outer.biasInterface.module.io.memRequest <> biasReqQueue.io.deq
  // Response
  biasRespQueue.io.enq <> outer.biasInterface.module.io.dataResponse
  bb.io.biasDataResponse_dat := biasRespQueue.io.deq.bits
  bb.io.biasDataResponse_vld := biasRespQueue.io.deq.valid
  biasRespQueue.io.deq.ready := bb.io.biasDataResponse_rdy

  bb.io.matrixUnitStartSignal_rdy := 1.U
  bb.io.matrixUnitDoneSignal_rdy := 1.U

  // VectorUnit
  val vector0Queue = Module(new Queue(UInt(64.W), 8))
  val vector1Queue = Module(new Queue(UInt(64.W), 8))
  val vector2Queue = Module(new Queue(UInt(64.W), 8))
  val vectorOutputAddrQueue = Module(new Queue(UInt(64.W), 8))
  val vectorOutputDataQueue = Module(new Queue(UInt(outer.memoryInterfaceWidth.W), 8))

  val vector0RespQueue = Module(new Queue(UInt(outer.memoryInterfaceWidth.W), 8))
  val vector1RespQueue = Module(new Queue(UInt(outer.memoryInterfaceWidth.W), 8))
  val vector2RespQueue = Module(new Queue(UInt(outer.memoryInterfaceWidth.W), 8))
  val serialVectorParamsQueue = Module(new Queue(UInt(32.W), 1))

  val vectorOutputBaseAddress = Reg(UInt(32.W))
  val vector0BaseAddress = Reg(UInt(32.W))
  val vector1BaseAddress = Reg(UInt(32.W))
  val vector2BaseAddress = Reg(UInt(32.W))

  bb.io.serialVectorParamsIn_vld := serialVectorParamsQueue.io.deq.valid
  bb.io.serialVectorParamsIn_dat := serialVectorParamsQueue.io.deq.bits
  serialVectorParamsQueue.io.deq.ready := bb.io.serialVectorParamsIn_rdy

  // VectorFetch0
  outer.vectorFetch0Interface.module.io.baseAddress := vector0BaseAddress
  // Request
  vector0Queue.io.enq.valid := bb.io.vectorFetch0AddressRequest_vld
  vector0Queue.io.enq.bits := bb.io.vectorFetch0AddressRequest_dat
  bb.io.vectorFetch0AddressRequest_rdy := vector0Queue.io.enq.ready
  outer.vectorFetch0Interface.module.io.memRequest <> vector0Queue.io.deq
  // Response
  vector0RespQueue.io.enq <> outer.vectorFetch0Interface.module.io.dataResponse
  bb.io.vectorFetch0DataResponse_dat := vector0RespQueue.io.deq.bits
  bb.io.vectorFetch0DataResponse_vld := vector0RespQueue.io.deq.valid
  vector0RespQueue.io.deq.ready := bb.io.vectorFetch0DataResponse_rdy

  // VectorFetch1
  outer.vectorFetch1Interface.module.io.baseAddress := vector1BaseAddress
  // Request
  vector1Queue.io.enq.valid := bb.io.vectorFetch1AddressRequest_vld
  vector1Queue.io.enq.bits := bb.io.vectorFetch1AddressRequest_dat
  bb.io.vectorFetch1AddressRequest_rdy := vector1Queue.io.enq.ready
  outer.vectorFetch1Interface.module.io.memRequest <> vector1Queue.io.deq
  // Response
  vector1RespQueue.io.enq <> outer.vectorFetch1Interface.module.io.dataResponse
  bb.io.vectorFetch1DataResponse_dat := vector1RespQueue.io.deq.bits
  bb.io.vectorFetch1DataResponse_vld := vector1RespQueue.io.deq.valid
  vector1RespQueue.io.deq.ready := bb.io.vectorFetch1DataResponse_rdy

  // VectorFetch2
  outer.vectorFetch2Interface.module.io.baseAddress := vector2BaseAddress
  // Request
  vector2Queue.io.enq.valid := bb.io.vectorFetch2AddressRequest_vld
  vector2Queue.io.enq.bits := bb.io.vectorFetch2AddressRequest_dat
  bb.io.vectorFetch2AddressRequest_rdy := vector2Queue.io.enq.ready
  outer.vectorFetch2Interface.module.io.memRequest <> vector2Queue.io.deq
  // Response
  vector2RespQueue.io.enq <> outer.vectorFetch2Interface.module.io.dataResponse
  bb.io.vectorFetch2DataResponse_dat := vector2RespQueue.io.deq.bits
  bb.io.vectorFetch2DataResponse_vld := vector2RespQueue.io.deq.valid
  vector2RespQueue.io.deq.ready := bb.io.vectorFetch2DataResponse_rdy

  // VectorOutput
  outer.vectorOutputInterface.module.io.baseAddress := vectorOutputBaseAddress

  vectorOutputAddrQueue.io.enq.valid := bb.io.vectorOutputAddress_vld
  vectorOutputAddrQueue.io.enq.bits := bb.io.vectorOutputAddress_dat
  bb.io.vectorOutputAddress_rdy := vectorOutputAddrQueue.io.enq.ready
  outer.vectorOutputInterface.module.io.addrRequest <> vectorOutputAddrQueue.io.deq

  vectorOutputDataQueue.io.enq.valid := bb.io.vectorOutput_vld
  vectorOutputDataQueue.io.enq.bits := bb.io.vectorOutput_dat
  bb.io.vectorOutput_rdy := vectorOutputDataQueue.io.enq.ready
  outer.vectorOutputInterface.module.io.dataRequest <> vectorOutputDataQueue.io.deq

  bb.io.vectorUnitStartSignal_rdy := 1.U
  bb.io.vectorUnitDoneSignal_rdy := 1.U

  // Registers for cycle counting
  val matrixUnitRunning = RegInit(false.B)
  when(bb.io.matrixUnitStartSignal_vld) {
    matrixUnitRunning := true.B
  }.elsewhen(bb.io.matrixUnitDoneSignal_vld) {
    matrixUnitRunning := false.B
  }
  val matrixUnitCycleCount = WideCounter(64, matrixUnitRunning)

  val vectorUnitRunning = RegInit(false.B)
  when(bb.io.vectorUnitStartSignal_vld) {
    vectorUnitRunning := true.B
  }.elsewhen(bb.io.vectorUnitDoneSignal_vld) {
    vectorUnitRunning := false.B
  }
  val vectorUnitCycleCount = WideCounter(64, vectorUnitRunning)

  val dnnRunning = RegInit(false.B)
  when(bb.io.matrixUnitStartSignal_vld || bb.io.vectorUnitStartSignal_vld) {
    dnnRunning := true.B
  }.elsewhen(bb.io.vectorUnitDoneSignal_vld) {
    dnnRunning := false.B
  }
  val dnnCycleCount = WideCounter(64, dnnRunning)

  outer.matrixUnitNode.regmap(
    0x00 -> Seq(RegField.w(32, serialMatrixParamsQueue.io.enq)),
    0x04 -> Seq(RegField(32, inputBaseAddress)),
    0x08 -> Seq(RegField(32, weightBaseAddress)),
    0x0c -> Seq(RegField(32, biasBaseAddress))
  )

  outer.vectorUnitNode.regmap(
    0x00 -> Seq(RegField.w(32, serialVectorParamsQueue.io.enq)),
    0x04 -> Seq(RegField(32, vector0BaseAddress)),
    0x08 -> Seq(RegField(32, vector1BaseAddress)),
    0x0c -> Seq(RegField(32, vector2BaseAddress)),
    0x10 -> Seq(RegField(32, vectorOutputBaseAddress))
  )

  outer.node.regmap(
    0x00 -> Seq(RegField.r(64, matrixUnitCycleCount.value)),
    0x08 -> Seq(RegField.r(64, vectorUnitCycleCount.value)),
    0x10 -> Seq(RegField.r(64, dnnCycleCount.value))
  )
}

trait CanHavePeripheryDNN { this: BaseSubsystem =>
  implicit val p: Parameters
  private val sbus = locateTLBusWrapper(SBUS)
  private val pbus = locateTLBusWrapper(PBUS)

  p(DNNKey) .map { k =>
    val dnn = sbus { LazyModule(new TLDNN(A.MMIOMINOTAUR, k.sbusWidth, pbus.beatBytes, sbus.beatBytes)(p)) }
    sbus.coupleFrom("dnn-inputs") { _ := TLBuffer() := dnn.inputsXbar.node }
    sbus.coupleFrom("dnn-weights") { _ := TLBuffer() := dnn.weightsXbar.node }
    sbus.coupleFrom("dnn-vectorFetch2") { _ := TLBuffer() := dnn.vectorFetch2InterfaceNode }
    sbus.coupleFrom("dnn-vectorOutput") { _ := TLBuffer() := dnn.vectorOutputInterfaceNode }

    pbus.coupleTo("dnn-matrixUnit") { dnn.matrixUnitNode := TLFragmenter(pbus.beatBytes, pbus.blockBytes) := _ }
    pbus.coupleTo("dnn-vectorUnit") { dnn.vectorUnitNode := TLFragmenter(pbus.beatBytes, pbus.blockBytes) := _ }
    pbus.coupleTo("dnn") { dnn.node := TLFragmenter(pbus.beatBytes, pbus.blockBytes) := _ }

    // connect dnn interrupt node to ibus
    ibus.fromSync := dnn.intNode
  }
}

class WithDNN(sbusWidth: Int, datatype: String, systolicArrayDimension: Int, clockPeriod: Int, technology: String)
  extends Config((site, here, up) => { case DNNKey =>
    Some(DNNConfig(mmioAddress = A.MMIOMINOTAUR, sbusWidth = sbusWidth, datatype, systolicArrayDimension, clockPeriod, technology))
 }
)
