package minotaur

import chisel3._
import chisel3.util._
import chisel3.experimental.{IntParam, BaseModule}
import freechips.rocketchip.amba.axi4._
import freechips.rocketchip.subsystem.BaseSubsystem
import org.chipsalliance.cde.config.{Parameters, Field, Config}
import freechips.rocketchip.diplomacy._
import freechips.rocketchip.regmapper.{HasRegMap, RegField}
import freechips.rocketchip.tilelink._
import freechips.rocketchip.util._
import freechips.rocketchip.interrupts.{IntSourceNode, IntSourcePortSimple}
import testchipip.serdes._

import scala.math._

trait HasPortWidth {
  def addrBits: Int
  def dataBits: Int
}

trait HasReadInterface extends Module with HasPortWidth {
  val io = IO(new Bundle {
      val memRequest = Flipped(DecoupledIO(UInt((addrBits).W)))
      val dataResponse = DecoupledIO(UInt(dataBits.W))
  
      val baseAddress = Input(UInt(addrBits.W))
    })
}

class TLBurstReadInterface(name: String, addrWidth: Int, dataWidth: Int, busWidth: Int, fifoDepth: Int)(implicit p: Parameters) extends LazyModule {
  val node = TLClientNode(Seq(TLMasterPortParameters.v1(Seq(TLMasterParameters.v1(
    name = name,
    sourceId = IdRange(0, fifoDepth),
    requestFifo = true)))))

  lazy val module = new LazyModuleImp(this) with HasReadInterface with HasPortWidth {
    override def addrBits = addrWidth * 2
    override def dataBits = dataWidth

    val (tl, edge) = node.out(0)

    val sourceId = RegInit(0.U(fifoDepth.W))

    val credits = RegInit(fifoDepth.U)

    // val input_queue = Module(new Queue(UInt((addrWidth).W), 2))

    // val fragmenter = Module(new RequestFragmenter(addrWidth))
    // fragmenter.io.requestWithBurst <> io.memRequest
    // input_queue.io.enq <> fragmenter.io.fragmentedRequest

    val output_queue = Module(new Queue(UInt(dataWidth.W), fifoDepth + 4))

    val actualAddress = io.memRequest.bits(31, 0) + io.baseAddress
    val actualBurst = Wire(UInt(log2Ceil(32).W))

    when(io.memRequest.bits(63, 32) === 16.U){
      actualBurst := 4.U
    } .elsewhen(io.memRequest.bits(63, 32) === 32.U){
      actualBurst := 5.U
    } .elsewhen(io.memRequest.bits(63, 32) === 64.U){
      actualBurst := 6.U
    } .otherwise {
      actualBurst := 0.U
      assert(!io.memRequest.valid, "Unsupported burst size!")
    }
    tl.a.bits := edge.Get(sourceId, actualAddress, actualBurst)._2

    tl.a.valid := (credits > 1.U) && (io.memRequest.valid)
    io.memRequest.ready := tl.a.fire

    if(busWidth != dataWidth){
      val deserializer = Module(new GenericDeserializer(UInt(dataWidth.W), busWidth))
      deserializer.io.in.valid := tl.d.valid
      deserializer.io.in.bits := tl.d.bits.data
      tl.d.ready := deserializer.io.in.ready

      output_queue.io.enq <> deserializer.io.out
    } else {
      tl.d.ready := output_queue.io.enq.ready
      output_queue.io.enq.valid := tl.d.valid
      output_queue.io.enq.bits := tl.d.bits.data
    }

    io.dataResponse <> output_queue.io.deq

    val beat = log2Ceil(dataWidth/8).U

    when(tl.a.fire && !io.dataResponse.fire) {
      when(actualBurst === beat){
        credits := credits - 1.U
      }.elsewhen(actualBurst === beat + 1.U){
        credits := credits - 2.U
      }
    }.elsewhen(!tl.a.fire && io.dataResponse.fire) {
      credits := credits + 1.U
    }

    when(tl.a.fire) {
      sourceId := sourceId + 1.U
    }
  }
}

trait HasWriteInterface extends Module with HasPortWidth {
  val io = IO(new Bundle {
    val addrRequest = Flipped(DecoupledIO(UInt(addrBits.W)))
    val dataRequest = Flipped(DecoupledIO(UInt(dataBits.W)))
  
    val baseAddress = Input(UInt(addrBits.W))
  })
}

class TLFullWriteInterface(name: String, addrWidth: Int, dataWidth: Int, busWidth: Int, fifoDepth: Int, maxFlight: Int)(implicit p: Parameters) extends LazyModule {
  val node = TLClientNode(Seq(TLMasterPortParameters.v1(Seq(TLMasterParameters.v1(
    name = name,
    sourceId = IdRange(0, maxFlight),
    requestFifo = true)))))

  lazy val module = new LazyModuleImp(this) with HasWriteInterface with HasPortWidth {
    override def addrBits = addrWidth
    override def dataBits = dataWidth

    val (tl, edge) = node.out(0)

    val addrQueue = Module(new Queue(UInt(addrWidth.W), fifoDepth))
    val dataQueue = Module(new Queue(UInt(dataWidth.W), fifoDepth))

    val sourceId = RegInit(0.U(log2Ceil(maxFlight).W))

    val actualAddress = addrQueue.io.deq.bits + io.baseAddress
    val beat = log2Ceil(dataWidth/8).U
    tl.a.bits := edge.Put(sourceId, actualAddress, beat, (dataQueue.io.deq.bits))._2

    tl.a.valid := addrQueue.io.deq.valid && dataQueue.io.deq.valid

    addrQueue.io.deq.ready := tl.a.fire
    dataQueue.io.deq.ready := tl.a.fire

    addrQueue.io.enq <> io.addrRequest
    dataQueue.io.enq <> io.dataRequest

    // don't really care about the responses to the writes
    tl.d.ready := true.B

    when(tl.a.fire) {
      sourceId := sourceId + 1.U
    }
  }
}

class TLFullReadInterface(name: String, addrWidth: Int, dataWidth: Int, fifoDepth: Int)(implicit p: Parameters) extends LazyModule {
  val node = TLClientNode(Seq(TLMasterPortParameters.v1(Seq(TLMasterParameters.v1(
    name = name,
    sourceId = IdRange(0, fifoDepth),
    requestFifo = true)))))

  lazy val module = new LazyModuleImp(this) with HasReadInterface with HasPortWidth {
    override def addrBits = addrWidth
    override def dataBits = dataWidth

    val log2TL = (log(dataWidth/8)/log(2.0)).toInt

    val (tl, edge) = node.out(0)

    val sourceId = RegInit(0.U(fifoDepth.W))

    val credits = RegInit(fifoDepth.U)

    val input_queue = Module(new Queue(UInt(addrWidth.W), 2))

    input_queue.io.enq <> io.memRequest

    val output_queue = Module(new Queue(UInt(dataWidth.W), fifoDepth + 2))

    val actualAddress = input_queue.io.deq.bits + io.baseAddress
    tl.a.bits := edge.Get(sourceId, actualAddress, log2TL.U)._2

    tl.a.valid := (credits > 1.U) && (input_queue.io.deq.valid)
    input_queue.io.deq.ready := tl.a.fire

    tl.d.ready := output_queue.io.enq.ready
    output_queue.io.enq.valid := tl.d.valid
    output_queue.io.enq.bits := tl.d.bits.data

    io.dataResponse <> output_queue.io.deq

    when(tl.a.fire && !io.dataResponse.fire) {
      credits := credits - 1.U
    }.elsewhen(!tl.a.fire && io.dataResponse.fire) {
      credits := credits + 1.U
    }

    when(tl.a.fire) {
      sourceId := sourceId + 1.U
    }
  }
}
class RequestFragmenter(addrWidth: Int) extends Module {
  val io = IO(new Bundle {
    val requestWithBurst = Flipped(DecoupledIO(UInt((addrWidth * 2).W)))
    val fragmentedRequest = DecoupledIO(UInt(addrWidth.W))
  })

  // two cases:
  // 1. burst == 16, just pass-through
  // 2. burst == 32, fragment the request into two requests

  val fragmenting = RegInit(false.B)
  val oldAddr = RegEnable(io.fragmentedRequest.bits, io.fragmentedRequest.fire)
  val burstSize = Reg(UInt(addrWidth.W))

  io.requestWithBurst.ready := Mux(fragmenting, false.B, io.fragmentedRequest.ready)
  io.fragmentedRequest.bits := Mux(fragmenting, oldAddr + 16.U, io.requestWithBurst.bits(31, 0))
  io.fragmentedRequest.valid := Mux(fragmenting, true.B, io.requestWithBurst.valid)

  when(io.requestWithBurst.fire) {
    fragmenting := io.requestWithBurst.bits(63, 32) === 32.U
    burstSize := io.requestWithBurst.bits(63, 32) - 16.U
  }

  when(io.fragmentedRequest.fire && fragmenting) {
    fragmenting := false.B
  }
}

class TLGenericReadInterface(name: String, addrWidth: Int, dataWidth: Int, fifoDepth: Int, sbusWidth: Int)(implicit p: Parameters) extends LazyModule {
  val node = TLClientNode(Seq(TLMasterPortParameters.v1(Seq(TLMasterParameters.v1(
    name = name,
    sourceId = IdRange(0, fifoDepth),
    requestFifo = true)))))

  lazy val module = new LazyModuleImp(this) with HasReadInterface with HasPortWidth {
    val tlWidth = sbusWidth
    override def addrBits = addrWidth
    override def dataBits = dataWidth

    val log2DataWidth = (log(dataWidth/8)/log(2.0)).toInt
    val log2TL = (log(tlWidth/8)/log(2.0)).toInt

    val (tl, edge) = node.out(0)

    val sourceId = RegInit(0.U(fifoDepth.W))

    val credits = RegInit(fifoDepth.U)

    val input_queue = Module(new Queue(UInt(addrWidth.W), 2))
    // Save address so we know which part of data response to pass on
    val address_queue = Module(new Queue(UInt((log2TL - log2DataWidth).W), fifoDepth + 2))

    input_queue.io.enq <> io.memRequest

    address_queue.io.enq.valid :=  tl.a.fire
    address_queue.io.enq.bits := (input_queue.io.deq.bits + io.baseAddress)(log2TL - 1, log2DataWidth)

    val output_queue = Module(new Queue(UInt(dataWidth.W), fifoDepth + 2))

    val actualAddress = ((input_queue.io.deq.bits + io.baseAddress)(addrWidth - 1, log2TL) << log2TL.U)
    tl.a.bits := edge.Get(sourceId, actualAddress, log2TL.U)._2

    tl.a.valid := (credits > 1.U) && (input_queue.io.deq.valid)
    input_queue.io.deq.ready := tl.a.fire

    tl.d.ready := output_queue.io.enq.ready
    output_queue.io.enq.valid := tl.d.valid
    // Only save part of data that we need
    address_queue.io.deq.ready := output_queue.io.enq.ready && tl.d.valid
    val bitsSel = address_queue.io.deq.bits
    output_queue.io.enq.bits := (tl.d.bits.data >> (dataWidth.U * bitsSel))(dataWidth - 1, 0)

    io.dataResponse <> output_queue.io.deq

    when(tl.a.fire && !io.dataResponse.fire) {
      credits := credits - 1.U
    }.elsewhen(!tl.a.fire && io.dataResponse.fire) {
      credits := credits + 1.U
    }

    when(tl.a.fire) {
      sourceId := sourceId + 1.U
    }
  }
}

class TLGenericWriteInterface(name: String, addrWidth: Int, dataWidth: Int, fifoDepth: Int, maxFlight: Int, sbusWidth: Int)(implicit p: Parameters) extends LazyModule {
  val node = TLClientNode(Seq(TLMasterPortParameters.v1(Seq(TLMasterParameters.v1(
    name = name,
    sourceId = IdRange(0, maxFlight),
    requestFifo = true)))))

  lazy val module = new LazyModuleImp(this) with HasWriteInterface with HasPortWidth {
    val tlWidth = sbusWidth
    override def addrBits = addrWidth
    override def dataBits = dataWidth

    val (tl, edge) = node.out(0)

    val addrQueue = Module(new Queue(UInt(addrWidth.W), fifoDepth))
    val dataQueue = Module(new Queue(UInt(dataWidth.W), fifoDepth))

    val sourceId = RegInit(0.U(log2Ceil(maxFlight).W))

    val log2DataWidth = (log(dataWidth/8)/log(2.0)).toInt
    val log2TL = (log(tlWidth/8)/log(2.0)).toInt

    val actualAddress = ((addrQueue.io.deq.bits + io.baseAddress)(addrWidth - 1, log2TL) << log2TL.U)
    val bitsSel = (addrQueue.io.deq.bits + io.baseAddress)(log2TL - 1, log2DataWidth)
    val writeData = 0.U(tlWidth.W) + (dataQueue.io.deq.bits << (dataWidth.U * bitsSel))
    val writeMask = 0.U((tlWidth / 8).W) + (~(0.U((dataWidth / 8).W)) << (dataWidth.U / 8.U * bitsSel))
    tl.a.bits := edge.Put(sourceId, actualAddress, log2TL.U, writeData, writeMask)._2

    tl.a.valid := addrQueue.io.deq.valid && dataQueue.io.deq.valid

    addrQueue.io.deq.ready := tl.a.fire
    dataQueue.io.deq.ready := tl.a.fire

    addrQueue.io.enq <> io.addrRequest
    dataQueue.io.enq <> io.dataRequest

    // don't really care about the responses to the writes
    tl.d.ready := true.B

    when(tl.a.fire) {
      sourceId := sourceId + 1.U
    }
  }
}
