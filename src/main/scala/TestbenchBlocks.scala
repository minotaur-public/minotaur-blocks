package minotaur

import chisel3._
import chisel3.util._

class Verification extends BlackBox with HasBlackBoxResource {
  val io = IO(new Bundle {
    val clk = Input(Clock())
  })

  addResource("/vsrc/ACCELERATOR/Verification.v")
  addResource("/csrc/ACCELERATOR/DNNVerification.cc")
  addResource("/csrc/ACCELERATOR/InterfaceChecker.cc")

  addResource("/csrc/ACCELERATOR/soc/RRAMMemoryModel.cc")
  addResource("/csrc/ACCELERATOR/soc/RRAMMemoryModel.h")
  addResource("/csrc/ACCELERATOR/soc/SRAMMemoryModel.cc")
  addResource("/csrc/ACCELERATOR/soc/SRAMMemoryModel.h")
  addResource("/csrc/ACCELERATOR/soc/SoCSimulation.cc")
  addResource("/csrc/ACCELERATOR/soc/SoCSimulation.h")
  addResource("/csrc/ACCELERATOR/soc/RTLMemoryModel.h")
  addResource("/csrc/ACCELERATOR/soc/SoCMemoryModel.h")

  addResource("/csrc/ACCELERATOR/common/GoldModel.cc")
  addResource("/csrc/ACCELERATOR/common/MemoryModel.cc")
  addResource("/csrc/ACCELERATOR/common/SimpleMemoryModel.cc")
  addResource("/csrc/ACCELERATOR/common/Simulation.cc")
  addResource("/csrc/ACCELERATOR/common/Utils.cc")

  addResource("/csrc/ACCELERATOR/resnet/ResNet.cc")
  addResource("/csrc/ACCELERATOR/mobilebert/MobileBERT.cc")
}

