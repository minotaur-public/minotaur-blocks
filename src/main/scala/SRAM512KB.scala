package minotaur

import minotaur.config._

import chisel3._
import chisel3.util._
import chisel3.experimental.{IntParam, BaseModule}
import freechips.rocketchip.amba.axi4._
import freechips.rocketchip.subsystem._
import org.chipsalliance.cde.config.{Parameters, Field, Config}
import freechips.rocketchip.diplomacy._
import freechips.rocketchip.regmapper.{HasRegMap, RegField}
import freechips.rocketchip.tilelink._
import freechips.rocketchip.util._
import freechips.rocketchip.interrupts.{IntSourceNode, IntSourcePortSimple}

case class SRAM512KBConfig(
  address: BigInt,
  numBanks: Int
)

case object SRAM512KBKey extends Field[Option[SRAM512KBConfig]](None)

class SRAM512KBIO extends Bundle {
 val clk = Input(Clock())
 val rst = Input(Bool())
 val ren = Input(Bool())
 val wen = Input(Bool())
 val adr = Input(UInt(32.W))
 val din = Input(UInt(128.W))
 val wbeb = Input(UInt(128.W))
 val q = Output(UInt(128.W))
}

trait HasSRAM512KBIO extends BaseModule {
  val io = IO(new SRAM512KBIO)
}

class SRAM512KB(useGeneric: Boolean) extends BlackBox with HasBlackBoxResource with HasSRAM512KBIO {
  addResource("/vsrc/SRAM512KB.v")
  if (useGeneric) {
    addResource("/vsrc/GenericSRAM.v")
  }
}

class TLSRAM512KB(address: BigInt, pbusBeatBytes: Int)(implicit p: Parameters) extends LazyModule {
  val device = new SimpleDevice("SRAM512KB", Seq("intel,sram"))

  val node = TLManagerNode(
    Seq(
      TLSlavePortParameters.v1(
        Seq(
          TLSlaveParameters.v1(
            address = Seq(AddressSet(address, 0x7ffff)),
            resources = device.reg("mem"),
            regionType = RegionType.IDEMPOTENT,
            executable = true,
            supportsGet = TransferSizes(1, pbusBeatBytes),
            supportsPutFull = TransferSizes(1, pbusBeatBytes),
            supportsPutPartial = TransferSizes(1, pbusBeatBytes),
            supportsArithmetic = TransferSizes.none,
            supportsLogical = TransferSizes.none,
            fifoId = Some(0)
          )
        ),
        beatBytes = pbusBeatBytes,
        minLatency = 1
      )
    )
  )

  lazy val module = new SRAM512KBModuleImp(this, address)
}

class SRAM512KBModuleImp(outer: TLSRAM512KB, address: BigInt) extends LazyModuleImp(outer) {
  val config = p(SRAM512KBKey).get

  val sram = Module(new SRAM512KB(p(GenericKey)))

  val (tl, edge) = outer.node.in(0)

  sram.io.clk := clock
  sram.io.rst := reset.asBool

  val d_size = Reg(UInt(edge.bundle.sizeBits.W))
  val d_source = Reg(UInt(edge.bundle.sourceBits.W))
  val d_valid = RegInit(false.B) // async??
  val d_opcode = Reg(UInt(3.W))

  // ready if there's nothing waiting, or if the d channel will be ready
  tl.a.ready := (!d_valid || tl.d.ready)

  val isWrite = (tl.a.bits.opcode === TLMessages.PutFullData) || (tl.a.bits.opcode === TLMessages.PutPartialData)

  sram.io.adr := (tl.a.bits.address - address.U) >> 4
  sram.io.din := tl.a.bits.data
  sram.io.wen := isWrite && tl.a.fire
  sram.io.ren := tl.a.fire && !isWrite
  // TODO: clean this up
  sram.io.wbeb := ~Cat(
    Fill(8, tl.a.bits.mask(15)),
    Fill(8, tl.a.bits.mask(14)),
    Fill(8, tl.a.bits.mask(13)),
    Fill(8, tl.a.bits.mask(12)),
    Fill(8, tl.a.bits.mask(11)),
    Fill(8, tl.a.bits.mask(10)),
    Fill(8, tl.a.bits.mask(9)),
    Fill(8, tl.a.bits.mask(8)),
    Fill(8, tl.a.bits.mask(7)),
    Fill(8, tl.a.bits.mask(6)),
    Fill(8, tl.a.bits.mask(5)),
    Fill(8, tl.a.bits.mask(4)),
    Fill(8, tl.a.bits.mask(3)),
    Fill(8, tl.a.bits.mask(2)),
    Fill(8, tl.a.bits.mask(1)),
    Fill(8, tl.a.bits.mask(0))
  )

  when(tl.a.fire) {
    d_valid := true.B
    d_source := tl.a.bits.source
    d_size := tl.a.bits.size
    d_opcode := Mux(isWrite, TLMessages.AccessAck, TLMessages.AccessAckData)
  }
    .elsewhen(tl.d.fire) {
      d_valid := false.B
    }

  tl.d.valid := d_valid
  tl.d.bits.size := d_size
  tl.d.bits.source := d_source
  tl.d.bits.opcode := d_opcode
  tl.d.bits.data := sram.io.q
}

trait CanHavePeripherySRAM512KB { this: BaseSubsystem =>
  implicit val p: Parameters
  private val sbus = locateTLBusWrapper(SBUS)

  private val offset = 512 * 1024 // 512 KB banks

  p(SRAM512KBKey) .map { k =>
    for (i <- 0 to k.numBanks - 1) {
      val sram = sbus { LazyModule(new TLSRAM512KB(k.address + i * offset, sbus.beatBytes)(p)) }
      sbus.coupleTo(s"sram-{i}") { sram.node := TLBuffer() := TLFragmenter(sbus.beatBytes, sbus.blockBytes) := _ }
    }
  }
}


class WithNBanksSRAM512KB(address: BigInt, numBanks: Int) extends Config((site, here, up) => {
  case SRAM512KBKey => Some(SRAM512KBConfig(address = address, numBanks = numBanks))
})
