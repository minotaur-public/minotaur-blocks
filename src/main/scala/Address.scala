package minotaur

import chisel3._
import chisel3.util._

// Constant values and parameters to be used throughout this file

package object A {
  val TLSRAM512KB = 0x3000000
  val MMIOMINOTAUR = 0x29000
}


// package object A {
//   // SRAM ports
//   val TLSRAM512KB = 0x3000000
//   val TLSRAM_PORTA = 0x3000000
//   val TLSRAM_PORTB = 0x4000000
//   val TLSRAM_MMIO = 0x26000
//
//   // Accelerator addresses
//   val TLAXIACCEL = 0x23000
//   val MMIOAXIACCEL = 0x29000
//
// //
// //
// //   // C2C addresses
// //   val C2C_ADDRESS_1 = 0x9000000
// //   val C2C_ADDRESS_2 = 0x10000000
// //   val C2C_PIN_ADDRESS = 0x11000000
// //
// //   // RRAM MMIO Regs
// //   val NAP = 0x20 // NAP register address offset
// //   val BANK_SELECT = 0x30 // bank select register offset
// //   val SWITCH = 0x50 // Enable/disable endurer bypass
// //   val REMAP = 0x60 // Trigger Endurer remap
// //   val RANDOM_OFFSET = 0x70 // Current value of random offset
// //   val RANDOM_OFFSET_NEW = 0x80 // New forced value of random offset
// //   val PRNG = 0x90 // Current value of prng
// //   val PRNG_NEW = 0xa0 // New forced value of prng
// //   val ENDURER_OVERRIDE = 0xb0 // TO force, or not to force, Endurer registers
// //   val CMD = 0x100
// //   val READY = 0x140
// //   val ERROR = 0x150
// //   val ENDURER_MEM_SIZE = 0x160 // Size of memory we translate addresses around (MR)
// //   val ENDURER_START = 0x170 // Virtual offset for endurer memory
// //   val END_ADDR_OUT = 0x180
// //   val NUM_LOADS = 0x200 // number of loads between writes
// //   val STATE = 0x210
// //   val ENDURER_STATE_RD = 0x220
// //
// //   // RRAM address registers
// //   val ADDRY = 0x300 // adress y read register
// //   val ADDRX = 0x380 // address x read register
// //
// //   // RRAM data in/out registers
// //   val DATA_IN = 0x400 // data in read write
// //   val DATA_OUT = 0x460 // data out read only
// //
// //   // RRAM TRC in and out registers
// //   val TRC_DATA_OUT = 0x500 // TRC data out read only low
// //   val TRC_DATA_IN = 0x540 // TRC data in read low
// //
// //   // RRAM power control
// //   val TRC_POWER = 0x600
// //   val MACRO_POWER = 0x610
// //
// //   // Accelerator register offsets
// //   val SYNC_RSC_RDY = 0x84
// //   val SYNC_RSC_VLD = 0x88
// //   val INPUT_BURST_SIZE = 0x100
// //   val INPUT_BASE_ADDRESS = 0x104
// //   val OUTPUT_BURST_SIZE = 0x108
// //   val OUTPUT_BASE_ADDRESS = 0x10c
// //   val WEIGHTS_BURST_SIZE = 0x110
// //   val WEIGHTS_BASE_ADDRESS = 0x114
// //   val BIAS1_BURST_SIZE = 0x118
// //   val BIAS1_BASE_ADDRESS = 0x11c
// //   val BIAS2_BURST_SIZE = 0x120
// //   val BIAS2_BASE_ADDRESS = 0x124
// //   val IDENTITY_BURST_SIZE = 0x128
// //   val IDENTITY_BASE_ADDRESS = 0x12c
// //   val SERIAL_PARAMS = 0x130
// //
// //   // C2C mmio register offsets
// //   val C2C_MMIO_RRAM_ADDRESS = 0x00
// //   val C2C_MMIO_SRAM_ADDRESS = 0x04
// //   val C2C_MMIO_BOOT_MAP = 0x08
// //   val C2C_MMIO_SEND_DONE = 0x0c
// //   val C2C_MMIO_RECV_START = 0x10
// }
